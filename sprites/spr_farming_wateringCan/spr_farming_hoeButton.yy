{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 6,
  "bbox_right": 28,
  "bbox_top": 2,
  "bbox_bottom": 30,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"6bbe2488-e8a1-4ce7-8e3e-16f9bda694e5","path":"sprites/spr_farming_hoeButton/spr_farming_hoeButton.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6bbe2488-e8a1-4ce7-8e3e-16f9bda694e5","path":"sprites/spr_farming_hoeButton/spr_farming_hoeButton.yy",},"LayerId":{"name":"304510a4-4d00-4629-b214-4a5efcedbd1d","path":"sprites/spr_farming_hoeButton/spr_farming_hoeButton.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"6bbe2488-e8a1-4ce7-8e3e-16f9bda694e5","path":"sprites/spr_farming_hoeButton/spr_farming_hoeButton.yy",},"LayerId":{"name":"6c08e115-9f3c-4f37-9bbf-f15476b9719d","path":"sprites/spr_farming_hoeButton/spr_farming_hoeButton.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
        {"FrameId":{"name":"6bbe2488-e8a1-4ce7-8e3e-16f9bda694e5","path":"sprites/spr_farming_hoeButton/spr_farming_hoeButton.yy",},"LayerId":{"name":"192e88e8-33c1-48ff-8d29-fcd1b0c0d114","path":"sprites/spr_farming_hoeButton/spr_farming_hoeButton.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_farming_hoeButton","path":"sprites/spr_farming_hoeButton/spr_farming_hoeButton.yy",},"resourceVersion":"1.0","name":"6bbe2488-e8a1-4ce7-8e3e-16f9bda694e5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3ba40554-00cd-44a6-b8b7-135b9c1decfe","path":"sprites/spr_farming_hoeButton/spr_farming_hoeButton.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3ba40554-00cd-44a6-b8b7-135b9c1decfe","path":"sprites/spr_farming_hoeButton/spr_farming_hoeButton.yy",},"LayerId":{"name":"192e88e8-33c1-48ff-8d29-fcd1b0c0d114","path":"sprites/spr_farming_hoeButton/spr_farming_hoeButton.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_farming_hoeButton","path":"sprites/spr_farming_hoeButton/spr_farming_hoeButton.yy",},"resourceVersion":"1.0","name":"3ba40554-00cd-44a6-b8b7-135b9c1decfe","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_farming_hoeButton","path":"sprites/spr_farming_hoeButton/spr_farming_hoeButton.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"cb06c15a-d0ad-4405-b8e7-2b139364cd8c","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6bbe2488-e8a1-4ce7-8e3e-16f9bda694e5","path":"sprites/spr_farming_hoeButton/spr_farming_hoeButton.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"020f62bf-0b02-4c19-8d91-cd32a4d817fc","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3ba40554-00cd-44a6-b8b7-135b9c1decfe","path":"sprites/spr_farming_hoeButton/spr_farming_hoeButton.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_farming_hoeButton","path":"sprites/spr_farming_hoeButton/spr_farming_hoeButton.yy",},
    "resourceVersion": "1.3",
    "name": "spr_farming_hoeButton",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"Layer 1 (2)","resourceVersion":"1.0","name":"192e88e8-33c1-48ff-8d29-fcd1b0c0d114","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Buttons",
    "path": "folders/Sprites/Farming/Buttons.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_farming_hoeButton",
  "tags": [],
  "resourceType": "GMSprite",
}