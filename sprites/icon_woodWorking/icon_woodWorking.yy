{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 29,
  "bbox_top": 6,
  "bbox_bottom": 26,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"f9b15c81-dc50-497b-815d-c797dfebcc4e","path":"sprites/icon_woodWorking/icon_woodWorking.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f9b15c81-dc50-497b-815d-c797dfebcc4e","path":"sprites/icon_woodWorking/icon_woodWorking.yy",},"LayerId":{"name":"60deaf35-75f9-4fed-9614-007fedff45e7","path":"sprites/icon_woodWorking/icon_woodWorking.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"icon_woodWorking","path":"sprites/icon_woodWorking/icon_woodWorking.yy",},"resourceVersion":"1.0","name":"f9b15c81-dc50-497b-815d-c797dfebcc4e","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"icon_woodWorking","path":"sprites/icon_woodWorking/icon_woodWorking.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"01b49f7e-a721-41c7-a937-005c46bfa24a","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f9b15c81-dc50-497b-815d-c797dfebcc4e","path":"sprites/icon_woodWorking/icon_woodWorking.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"icon_woodWorking","path":"sprites/icon_woodWorking/icon_woodWorking.yy",},
    "resourceVersion": "1.3",
    "name": "icon_woodWorking",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"60deaf35-75f9-4fed-9614-007fedff45e7","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Skill Wing",
    "path": "folders/Sprites/Icons/Skill Wing.yy",
  },
  "resourceVersion": "1.0",
  "name": "icon_woodWorking",
  "tags": [],
  "resourceType": "GMSprite",
}