{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 4,
  "bbox_right": 28,
  "bbox_top": 5,
  "bbox_bottom": 22,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"405beb4e-2be3-4ad8-85d4-d4968f2987c2","path":"sprites/item_goldBar/item_goldBar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"405beb4e-2be3-4ad8-85d4-d4968f2987c2","path":"sprites/item_goldBar/item_goldBar.yy",},"LayerId":{"name":"814371a6-3a41-4bfa-b846-c17cc3381b97","path":"sprites/item_goldBar/item_goldBar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"item_goldBar","path":"sprites/item_goldBar/item_goldBar.yy",},"resourceVersion":"1.0","name":"405beb4e-2be3-4ad8-85d4-d4968f2987c2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5ef751c4-0535-4ee5-81ec-e0fa51072315","path":"sprites/item_goldBar/item_goldBar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5ef751c4-0535-4ee5-81ec-e0fa51072315","path":"sprites/item_goldBar/item_goldBar.yy",},"LayerId":{"name":"814371a6-3a41-4bfa-b846-c17cc3381b97","path":"sprites/item_goldBar/item_goldBar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"item_goldBar","path":"sprites/item_goldBar/item_goldBar.yy",},"resourceVersion":"1.0","name":"5ef751c4-0535-4ee5-81ec-e0fa51072315","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"item_goldBar","path":"sprites/item_goldBar/item_goldBar.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"6ebc5bd5-df62-471f-b87f-455092a9abbd","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"405beb4e-2be3-4ad8-85d4-d4968f2987c2","path":"sprites/item_goldBar/item_goldBar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bb670736-b397-4a2d-8197-e906ee79339a","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5ef751c4-0535-4ee5-81ec-e0fa51072315","path":"sprites/item_goldBar/item_goldBar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"item_goldBar","path":"sprites/item_goldBar/item_goldBar.yy",},
    "resourceVersion": "1.3",
    "name": "item_goldBar",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"814371a6-3a41-4bfa-b846-c17cc3381b97","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Bars",
    "path": "folders/Sprites/Items/Bars.yy",
  },
  "resourceVersion": "1.0",
  "name": "item_goldBar",
  "tags": [],
  "resourceType": "GMSprite",
}