{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 0,
  "bbox_top": 0,
  "bbox_bottom": 0,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 64,
  "height": 64,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"a99beb26-ad04-4b28-b61e-17fec07ec9f8","path":"sprites/item_tungstenArmPlates/item_tungstenArmPlates.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a99beb26-ad04-4b28-b61e-17fec07ec9f8","path":"sprites/item_tungstenArmPlates/item_tungstenArmPlates.yy",},"LayerId":{"name":"ece1a6ab-8ef5-4a5f-80d5-a84da9b289cf","path":"sprites/item_tungstenArmPlates/item_tungstenArmPlates.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"item_tungstenArmPlates","path":"sprites/item_tungstenArmPlates/item_tungstenArmPlates.yy",},"resourceVersion":"1.0","name":"a99beb26-ad04-4b28-b61e-17fec07ec9f8","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"item_tungstenArmPlates","path":"sprites/item_tungstenArmPlates/item_tungstenArmPlates.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"c5e2f390-6f17-4855-9285-1388c2e9e3a8","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a99beb26-ad04-4b28-b61e-17fec07ec9f8","path":"sprites/item_tungstenArmPlates/item_tungstenArmPlates.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"item_tungstenArmPlates","path":"sprites/item_tungstenArmPlates/item_tungstenArmPlates.yy",},
    "resourceVersion": "1.3",
    "name": "item_tungstenArmPlates",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"ece1a6ab-8ef5-4a5f-80d5-a84da9b289cf","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "a10_Arm Plates",
    "path": "folders/Sprites/Items/Armor Making/a10_Arm Plates.yy",
  },
  "resourceVersion": "1.0",
  "name": "item_tungstenArmPlates",
  "tags": [],
  "resourceType": "GMSprite",
}