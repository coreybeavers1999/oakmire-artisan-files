{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 9,
  "bbox_right": 24,
  "bbox_top": 2,
  "bbox_bottom": 26,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 32,
  "height": 32,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"6be7a9a4-5371-481f-87f6-fa5393bf775f","path":"sprites/icon_meatCarving/icon_meatCarving.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6be7a9a4-5371-481f-87f6-fa5393bf775f","path":"sprites/icon_meatCarving/icon_meatCarving.yy",},"LayerId":{"name":"c029c97f-c035-49c1-8b4a-229137fcebf2","path":"sprites/icon_meatCarving/icon_meatCarving.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"icon_meatCarving","path":"sprites/icon_meatCarving/icon_meatCarving.yy",},"resourceVersion":"1.0","name":"6be7a9a4-5371-481f-87f6-fa5393bf775f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"icon_meatCarving","path":"sprites/icon_meatCarving/icon_meatCarving.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"37182afd-4a33-4a33-a5a3-923229aa9699","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6be7a9a4-5371-481f-87f6-fa5393bf775f","path":"sprites/icon_meatCarving/icon_meatCarving.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"icon_meatCarving","path":"sprites/icon_meatCarving/icon_meatCarving.yy",},
    "resourceVersion": "1.3",
    "name": "icon_meatCarving",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"c029c97f-c035-49c1-8b4a-229137fcebf2","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Skill Wing",
    "path": "folders/Sprites/Icons/Skill Wing.yy",
  },
  "resourceVersion": "1.0",
  "name": "icon_meatCarving",
  "tags": [],
  "resourceType": "GMSprite",
}