{ //Drawing Foraging Spot Selection
	
	for(i = 0; i < spotBarNumber; i++) { //Draw Each Foraging Spot Option
		
		{ //Setting Background Vars
			var spotBackColor = c_black;
			
			var x1, x2, y1, y2;
			x1 = spotPosX;
			y1 = spotPosY + (i * spotBarHeight) + (i * spotYOffset);
			x2 = x1 + spotBarWidth;
			y2 = y1 + spotBarHeight;
		
		}
		
		{ //Mouse
			
			if(mouseOver(x1,y1,x2,y2)) {
				
				spotBackColor = c_white;
				
				{ //Clicking
					
					if(mouse_check_button_pressed(mb_left)) && (level[6] >= job[6][i][1]) {
						
						doingCollectingJob[0] = true;
						doingCollectingJob[1] = 6;
						doingCollectingJob[2] = i;
						
					}
					
				}
				
			}
			
		}
		
		{ //Background
			draw_set_alpha(.5);
			draw_set_color(spotBackColor);
			draw_rectangle(x1,y1,x2,y2,false);
		}
		
		{ //Drawing Text
			var textX, textY;
			textX = x1 + textOffsetX;
			textY = y1 + spotBarHeight / 2;
		
		
			draw_set_color(globals.cwClickableColor);
			draw_set_alpha(1);
			draw_set_valign(fa_middle);
			draw_set_halign(fa_left);
			if(level[6] >= job[6][i][1]) { draw_text(textX,textY,job[6][i][0]); } else { draw_text(textX,textY,"Unlock At Level " + string(job[6][i][1])); }
		}
		
	}
	
	{ //Draw Amount Completed
		
		if(doingCollectingJob[0] == true) && (doingCollectingJob[1] == 6) {
			var hx1, hx2, hy1, hy2;
			hx1 = healthbarX;
			hy1 = healthbarY;
			hx2 = spotPosX - 50;
			hy2 = hy1 + spotBarHeight;
			
			var currentJob = doingCollectingJob[2];
			
			draw_healthbar(hx1,hy1,hx2,hy2,(job[6][currentJob][3] / job[6][currentJob][4]) * 100,globals.jobIconCompBackgroundColor,globals.jobIconCompMinColor,globals.jobIconCompMaxColor,0,true,false);
		}
		
	}

}