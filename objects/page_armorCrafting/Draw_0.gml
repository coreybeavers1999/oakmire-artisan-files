var drawDescription = false;
var drawDescriptionItem = 0;
var drawDescriptionAmount = 0;


{ //Draw Input
	
	{ //Drawing Background
		
		if(armorCraftingInput[0] != 0) {
			
			draw_set_color(qualityColor[item[armorCraftingInput[0]][4]]);
			
		} else {
			draw_set_color(globals.inventorySlotBackgroundColor);
		}
		
		draw_set_alpha(globals.qualityBackgroundAlpha);
		draw_rectangle(inputSlotX1,inputSlotY1,inputSlotX2,inputSlotY2,false);
		draw_set_color(globals.defaultTextColor);
		draw_rectangle(inputSlotX1,inputSlotY1,inputSlotX2-1,inputSlotY2-1,true);
		
	}
	
	{ //Draw Item
		
		if(armorCraftingInput[1] > 0 && armorCraftingInput[0] != 0) {
			draw_sprite_ext(item[armorCraftingInput[0]][2],0,inputSlotX1,inputSlotY1,slotScale,slotScale,0,c_white,1);
			
			var tqOffset = 2; //text quantity offset
				
			draw_set_font(fontSmall);
			draw_set_valign(fa_bottom);
			draw_set_halign(fa_right);
			draw_set_color(globals.defaultTextColor);
			
			draw_text(inputSlotX2-tqOffset, inputSlotY2-tqOffset+5, string(armorCraftingInput[1]));
		
		} else {
			
			armorCraftingInput[0] = 0;
			armorCraftingInput[1] = 0;
			
		}
		
	}
	
	{ //Draw Amount Needed
		
		draw_set_halign(fa_center);
		draw_set_valign(fa_middle);
		draw_text(requiredBarX,requiredBarY,string(armorCraftingIndex[armorCraftingSelectedItem][2]) + " Bars Required");
		
	}
	
}

{ //Draw Output
	
	{ //Determine Output
		
		if(armorCraftingInput[0] != 0) { 
			
			armorCraftingOutput[0] = (armorCraftingInput[0] + 3732) + (67 * armorCraftingSelectedItem);
			
		} else if(armorCraftingOutput[1] == 0) { armorCraftingOutput[0] = 0; }
		
	}
	
	{ //Drawing Background
		
		if(armorCraftingOutput[0] != 0) {
			
			draw_set_color(qualityColor[item[armorCraftingOutput[0]][4]]);
			
		} else {
			draw_set_color(globals.inventorySlotBackgroundColor);
		}
		
		draw_set_alpha(globals.qualityBackgroundAlpha);
		draw_rectangle(outputSlotX1,outputSlotY1,outputSlotX2,outputSlotY2,false);
		draw_set_color(globals.defaultBoxOutlineColor);	
		draw_rectangle(outputSlotX1,outputSlotY1,outputSlotX2-1,outputSlotY2-1,true);
		
	}
	
	{ //Draw Item
		
		var tqOffset = 2; //text quantity offset
		var itemAlpha = 1;
		
		if(armorCraftingOutput[0] != 0 && armorCraftingOutput[1] != 0) {
			draw_set_color(globals.defaultTextColor);
			draw_set_font(fontSmall);
			draw_set_valign(fa_bottom);
			draw_set_halign(fa_right);
				
			draw_text(outputSlotX2-tqOffset, outputSlotY2-tqOffset+5, string(armorCraftingOutput[1]));
		}
		else
		{
			itemAlpha = .5;
		}
		
		draw_sprite_ext(item[armorCraftingOutput[0]][2],0,outputSlotX1,outputSlotY1,slotScale,slotScale,0,c_white,itemAlpha);
	}
	
	{ //Clicking
		
		if mouseOver(outputSlotX1,outputSlotY1,outputSlotX2,outputSlotY2) && mouse_check_button_pressed(mb_left) {
			
			inventoryAddItem(armorCraftingOutput[0],armorCraftingOutput[1]);
			armorCraftingOutput[0] = 0;
			armorCraftingOutput[1] = 0;
			
		}
		
	}
	
}

{ //Drawing Completion Bar
	
	draw_set_alpha(1);
	draw_healthbar(procX1,procY1,procX2,procY2,(armorCraftingTime[0] / armorCraftingTime[1]) * 100, globals.jobIconCompBackgroundColor,globals.jobIconCompMinColor,globals.jobIconCompMaxColor,0,true,false);
	
}

{ //Drawing Activate Button
	
	if(doingCraftingJob[0] == false or doingCraftingJob[1] != 15) { 
		draw_set_color(craftingListBackgroundColor);
	} else {
		draw_set_color(globals.defaultBoxActiveColor);
	}
	
	draw_set_alpha(craftingListBackgroundAlpha);
	draw_rectangle(activateX1,activateY1,activateX2,activateY2,false);
	draw_set_alpha(1);
	draw_set_color(globals.defaultTextColor);
	draw_set_halign(fa_middle);
	draw_set_valign(fa_center);
	draw_text(activeMidX,activeMidY,"Activate Skill");
	
	if mouseOver(activateX1,activateY1,activateX2,activateY2) && mouse_check_button_pressed(mb_left) {
	
		if(doingCraftingJob[1] == 15 && doingCraftingJob[0] == true) {
			
			doingCraftingJob[0] = false;
			
		} else {
			
			doingCraftingJob[0] = true;
			doingCraftingJob[1] = 15;
			
		}
		
	}
	
}

{ //Item Selection
	
	var selectedBar = drawItemSelection(269,335);
	
	if(mouse_check_button_pressed(mb_left) && selectedBar != 0) {
		
		armorCraftingTime[0] = 0;
		
		if(armorCraftingOutput[1] != 0) { //Deposit current output
			
			inventoryAddItem(armorCraftingOutput[0],armorCraftingOutput[1]);
			armorCraftingOutput[0] = 0;
			armorCraftingOutput[1] = 0;
			
		}
		
		
		if(selectedBar == armorCraftingInput[0]) {
			
			armorCraftingInput[0] = 0;
			
		} else {
			
			armorCraftingInput[0] = selectedBar;
			
		}
		
	}
	
}
	
{ //Draw Item Description
	
	if mouseOver(inputSlotX1,inputSlotY1,inputSlotX1 + slotSize,inputSlotY1 + slotSize) {
		
		drawDescription = true;
		drawDescriptionItem = armorCraftingInput[0];
		drawDescriptionAmount = armorCraftingInput[1];
		
	}
	
	if mouseOver(outputSlotX1,outputSlotY1,outputSlotX1 + slotSize,outputSlotY1 + slotSize) {
		
		drawDescription = true;
		drawDescriptionItem = armorCraftingOutput[0];
		drawDescriptionAmount = armorCraftingOutput[1];
		
	}
	
	if(drawDescription && drawDescriptionItem != 0) drawItemDescriptionID(drawDescriptionItem,drawDescriptionAmount);
	
}
	
{ //Draw Crafting List
	
	var numberOfCrafting = array_length(armorCraftingIndex);
	
	for(selectedSpot = 0; selectedSpot < numberOfCrafting; selectedSpot++) {
		
		var mouseIsOver = false;
		var craftingItemLocked = false;
		
		if(level[15] < armorCraftingIndex[selectedSpot][1]) { craftingItemLocked = true; }
		
		//Background
		var backgroundX1,backgroundX2,backgroundY1,backgroundY2;
		backgroundX1 = craftingListX1;
		backgroundY1 = craftingListY1 + (selectedSpot * (craftingListHeight + craftingListSpacing));
		backgroundX2 = backgroundX1 + craftingListWidth;
		backgroundY2 = backgroundY1 + craftingListHeight;
		
		draw_set_color(craftingListBackgroundColor);
		draw_set_alpha(craftingListBackgroundAlpha);
		
		//Selected so draw background selected color
		if(armorCraftingSelectedItem == selectedSpot) draw_set_color(craftingListColorActive);
		
		//Mouse Over
		if mouseOver(backgroundX1,backgroundY1,backgroundX2,backgroundY2) { 
			mouseIsOver = true;
			draw_set_color(craftingListBackgroundColorMouse);
			
			
			
			if mouse_check_button_pressed(mb_left) && craftingItemLocked == false {
				
				armorCraftingSelectedItem = selectedSpot;
				
				if(armorCraftingOutput[1] > 0) inventoryAddItem(armorCraftingOutput[0],armorCraftingOutput[1]);
				
				armorCraftingOutput[0] = 0;
				armorCraftingOutput[1] = 0;
				armorCraftingInput[0] = 0;
				armorCraftingInput[0] = 0;
				
				armorCraftingTime[0] = 0;
				
			}
		}

		draw_rectangle(backgroundX1,backgroundY1,backgroundX2,backgroundY2,false);
		
		
		
		draw_set_alpha(1);
		
		//Text
		draw_set_halign(fa_left);
		draw_set_valign(fa_middle);
		draw_set_color(craftingListTextColor);
		
		var textX, textY;
		textX = backgroundX1 + craftingListTextXBuffer;
		textY = backgroundY1 + craftingListTextYBuffer;
		
		if craftingItemLocked {
			draw_text(textX,textY,"Unlock At Level " + string(armorCraftingIndex[selectedSpot][1]));
		} else {
			draw_text(textX,textY,armorCraftingIndex[selectedSpot][0]);
		}
		
	}
	
}