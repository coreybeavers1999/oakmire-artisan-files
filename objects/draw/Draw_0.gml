var drawDescriptionForItem = false;
var descriptionItemSlot = 0;

{ //Draw Background
	
	draw_set_color(globals.backgroundColor);
	draw_set_alpha(0);
	draw_rectangle(0,0,screenW,screenH,false);
	
}

{ //Draw Pages
	
	if(main == 3 or main == 4) { //Treecutting And Mining Menus
		
		{ //Draw Jobs
			
			//Positioning
			var jobX, jobY, onRight;
			onRight = true;
			
			//Showing Correct Job List
			var amountOfJobs;
			
			amountOfJobs = array_length(job[main]);
			
				
			
			//Drawing Jobs
			for(i = 0; i < amountOfJobs; i++) {
				onRight = !onRight;
				jobY = jobIconY + ((jobIconBuffer + jobIconHeight) * (i - onRight - floor(i / 2))) - jobScrolled;
				jobX = jobIconX + ((jobIconBuffer + jobIconWidth) * onRight);
			
				drawJob(main,i,jobX,jobY);
			
			}
		}					
	}
	else
	{ //Inventory Menus+
		
		if(main == 100) { //Inventory
			
			//Draw Inventory Slots
			for(i = 0; i < inventorySlots; i++) {
				
				var mouseOn = false;
				
				{ //Setting Positions
					var x1, x2, y1, y2;
					x1 = inventorySlotX + (inventorySlotSize + inventoryBuffer) * (i - (floor(i / inventorySlotsWide) * inventorySlotsWide));
					x2 = x1 + inventorySlotSize;
					y1 = inventorySlotY + ((inventorySlotSize + inventoryBuffer) * floor(i / inventorySlotsWide));
					y2 = y1 + inventorySlotSize;
				}
				
				{ //Draw Background
					if(mouseOver(x1,y1,x2,y2)) { draw_set_color(globals.inventorySlotHighlightColor); mouseOn = true; } else { draw_set_color(globals.inventorySlotBackgroundColor); }
					draw_set_alpha(1);
					draw_rectangle(x1,y1,x2,y2,false);
					draw_set_color(c_white);
					draw_rectangle(x1,y1,x2+1,y2+1,true);					
					
				}
				
				{ //Draw Item In Slot
					if(inventory[i][0] != 0) {
						
						{ //Draw Quality Color
							
							draw_set_color(qualityColor[item[inventory[i][0]][4]]);
							draw_set_alpha(globals.qualityBackgroundAlpha);
							draw_rectangle(x1,y1,x2,y2,false);
							draw_set_alpha(1);
							
						}
						
						var scale = inventorySlotSize / 32;
						draw_sprite_ext(item[inventory[i][0]][2],0,x1,y1,scale,scale,0,c_white,1);
						
						{ //Draw Quantity
							var buffer = 2;
					
							draw_set_color(c_white);
							draw_set_font(fontTiny);
							draw_set_halign(fa_right);
							draw_set_valign(fa_bottom);
					
							draw_text(x2 - buffer, y2 - buffer, string(inventory[i][1]));
					
						}
						
					}
					
				}
				
				{ //Clicking Items
					
					if mouseOn == true && mouse_check_button_pressed(mb_left) {
						
						if(mouseItem[0] != inventory[i][0]) { //Don't Stack
							var prevItem, prevAmount;
							prevItem = mouseItem[0];
							prevAmount = mouseItem[1];
							
							mouseItem[0] = inventory[i][0];
							mouseItem[1] = inventory[i][1];
							
							inventory[i][0] = prevItem;
							inventory[i][1] = prevAmount;
						}
						else
						{ //Stack
							
							inventory[i][1] += mouseItem[1];
							mouseItem[0] = 0;
							mouseItem[1] = 0;
							
						}
						
					} else
					
					if mouseOn == true && mouse_check_button_pressed(mb_right)  { //Dropping/Picking up half
						
						if(mouseItem[0] == 0) { //Holding Nothing
						
							if inventory[i][0] != 0 && inventory[i][1] > 1 {
								
								mouseItem[0] = inventory[i][0];
								mouseItem[1] = ceil(inventory[i][1] / 2);
								inventory[i][1] = floor(inventory[i][1] / 2);
								
								show_debug_message("Testing");
								
							}
						
						} else
					
						if(mouseItem[0] != 0) { //Holding Something
							
							if(mouseItem[0] == inventory[i][0]) { //Dropping Onto Stack Of Same
							
								inventory[i][1] += 1;
								mouseItem[1] -= 1;
								
								show_debug_message(mouseItem[1]);
								
								if(mouseItem[1] <= 0) mouseItem[0] = 0;
							
							} else if(inventory[i][0] == 0) { //Dropping Into Empty Slot
								
								inventory[i][0] = mouseItem[0];
								inventory[i][1] = 1;
								mouseItem[1] -= 1;
								
								if(mouseItem[1] <= 0) mouseItem[0] = 0;
								
							}
							
						}
						
					}
				
				}
				
				{ //Tagging to draw description
					
					if(mouseOn == true && inventory[i][0] != 0) { drawDescriptionForItem = true; descriptionItemSlot = i; }
					
				}
			}
			
			//Draw Mouse Item
			if(mouseItem[0] != 0) {
				var scale = inventorySlotSize / 32;
				var offset = (inventorySlotSize / 32) * 16;
				var itemID = mouseItem[0];
				draw_sprite_ext(item[itemID][2],0,mouse_x - offset, mouse_y - offset,scale,scale,0,c_white,1);
				
				//Draw Amount
				if(mouseItem[1] > 1) {
					var buffer = 2;
					
					draw_set_color(c_white);
					draw_set_font(fontTiny);
					draw_set_halign(fa_right);
					draw_set_valign(fa_bottom);
					
					draw_text(mouse_x - buffer + offset, mouse_y - buffer + offset, string(mouseItem[1]));
				}
			}
			
		}
		
	}
	
}

{ //Drawing Experience Bar
	
	if(main >= 3 && main <= 50) { drawExperienceBar(); }
}

{ //Draw Crafting Wing Background
	
	draw_set_color(globals.craftingWingColor);
	draw_set_alpha(.5);
	draw_rectangle(0,0,craftingWingWidth,screenH,false);
	
}

{ //Draw Crafting Skill List
	
	draw_set_alpha(1);
	draw_set_color(globals.cwClickableColor);
	draw_set_font(fontSmall);
	draw_set_halign(fa_left);
	draw_set_valign(fa_top);
	
	var lineSpacing, X, Y, nonClickXOffset, iconXOffset; 
	lineSpacing = 32;
	X = 32;
	Y = 32;
	nonClickXOffset = 16;
	iconXOffset = 36;
	
	for(i = 0; i < cwCount; i ++;) { //Drawing Each Line
		
		if(cw[i][1] == false) { //Non-Clickable
			
			draw_set_color(globals.cwNonClickableColor);
			draw_text(X - nonClickXOffset,Y + (i * lineSpacing), cw[i][0]);
			
			draw_set_color(globals.cwClickableColor);
			
		} else { //Clickable
		
			//Doing Collecting Job Background
			if(doingCollectingJob[0] == true && doingCollectingJob[1] == i) {
				
				draw_set_alpha(1);
				draw_healthbar(0, Y + (i * lineSpacing), craftingWingWidth, Y + (i * lineSpacing) + lineSpacing, job[doingCollectingJob[1]][doingCollectingJob[2]][3] / job[doingCollectingJob[1]][doingCollectingJob[2]][4] * 100, globals.cwDoingJobBackground,globals.jobIconCompMinColor,globals.jobIconCompMinColor,0,true,false);
				
				draw_set_color(globals.cwClickableColor);

			}
			
			//Doing Processing Job Background
			if(doingProcessingJob[0] == true && doingProcessingJob[1] == i) {
				
				draw_set_alpha(1);
				
				if(i == 8) draw_healthbar(0, Y + (i * lineSpacing), craftingWingWidth, Y + (i * lineSpacing) + lineSpacing, smeltingTime[0] / smeltingTime[1] * 100, globals.cwDoingJobBackground,globals.jobIconCompMinColor,globals.jobIconCompMinColor,0,true,false);
				if(i == 9) draw_healthbar(0, Y + (i * lineSpacing), craftingWingWidth, Y + (i * lineSpacing) + lineSpacing, logPlaningTime[0] / logPlaningTime[1] * 100, globals.cwDoingJobBackground,globals.jobIconCompMinColor,globals.jobIconCompMinColor,0,true,false);
				if(i == 10) draw_healthbar(0, Y + (i * lineSpacing), craftingWingWidth, Y + (i * lineSpacing) + lineSpacing, meatCarvingTime[0] / meatCarvingTime[1] * 100, globals.cwDoingJobBackground,globals.jobIconCompMinColor,globals.jobIconCompMinColor,0,true,false);
				
				draw_set_color(globals.cwClickableColor);

			}
			
			//Doing Crafting Job Background
			if(doingCraftingJob[0] == true && doingCraftingJob[1] == i) {
				
				draw_set_alpha(1);
				
				if(i == 13) draw_healthbar(0, Y + (i * lineSpacing), craftingWingWidth, Y + (i * lineSpacing) + lineSpacing, bladeCraftingTime[0] / bladeCraftingTime[1] * 100, globals.cwDoingJobBackground,globals.jobIconCompMinColor,globals.jobIconCompMinColor,0,true,false);
				if(i == 14) draw_healthbar(0, Y + (i * lineSpacing), craftingWingWidth, Y + (i * lineSpacing) + lineSpacing, woodworkingTime[0] / woodworkingTime[1] * 100, globals.cwDoingJobBackground,globals.jobIconCompMinColor,globals.jobIconCompMinColor,0,true,false);
				if(i == 15) draw_healthbar(0, Y + (i * lineSpacing), craftingWingWidth, Y + (i * lineSpacing) + lineSpacing, armorCraftingTime[0] / armorCraftingTime[1] * 100, globals.cwDoingJobBackground,globals.jobIconCompMinColor,globals.jobIconCompMinColor,0,true,false);
				if(i == 16) draw_healthbar(0, Y + (i * lineSpacing), craftingWingWidth, Y + (i * lineSpacing) + lineSpacing, brewingTime[0] / brewingTime[1] * 100, globals.cwDoingJobBackground,globals.jobIconCompMinColor,globals.jobIconCompMinColor,0,true,false);
				
				draw_set_color(globals.cwClickableColor);

			}
			
			
			
			//Mouse Over
			if mouseOver(0, Y + (i * lineSpacing), craftingWingWidth, Y + (i * lineSpacing) + lineSpacing) { //Mouse Is Over
				
				draw_set_color(globals.cwMouseOverBackground);
				draw_set_alpha(1);
				draw_rectangle(0, Y + (i * lineSpacing), craftingWingWidth, Y + (i * lineSpacing) + lineSpacing, false);
				draw_set_color(globals.cwClickableColor);
				
				if(mouse_check_button_pressed(mb_left)) {
					main = cw[i][3];
					jobScrolled = 0;
					
					var nextRoomName = "";
					for(c = 1; c < string_length(cw[i][0]) + 1; c++) { //This loops until the name of the room has been copied until a space is entered
						
						if(string_char_at(cw[i][0],c) != " ") {
							nextRoomName += string_char_at(cw[i][0],c);
						}
						else
						{
							break;
						}
						
					}
					
					show_debug_message(nextRoomName);
					
					var roomID = asset_get_index(nextRoomName);
					room_goto(roomID);
				}
				
			}
			
			draw_sprite(cw[i][2],0,X,Y + (i * lineSpacing));
			draw_text(X + iconXOffset,Y + (i * lineSpacing), cw[i][0]);
		
		}
		
	}
	
}

{ //Doing Jobs
	
	if(doingCollectingJob[0] == true) {
		var SkillID = doingCollectingJob[1];
		var JobID = doingCollectingJob[2];
		
		job[SkillID][JobID][3] += 1 + timeSpeed;
		
		if(job[SkillID][JobID][3] >= job[SkillID][JobID][4]) {
			//*
			completeJob(SkillID,JobID);
		}
		
	}
	
	if(doingProcessingJob[0] == true) {
		
		if(doingProcessingJob[1] == 8) { //Smelting
			
			if(smeltingFuel > 0 && smeltingInput[0] > 0) { //If there's fuel and Input
				smeltingTime[0] += 1 + timeSpeed;
				
				if(smeltingTime[0] >= smeltingTime[1]) {
					
					smeltingTime[0] = 0;
					completeSmelting();
					
				}
			} else { //Not Enough Fuel/Input
				
				smeltingTime[0] = 0;
				
			}
			
		}
		
		if(doingProcessingJob[1] == 9) { //Log Planing
			
			if(logPlaningFuel > 0 && logPlaningInput[0] > 0) { //If there's fuel and Input
				logPlaningTime[0] += 1 + timeSpeed;
				
				if(logPlaningTime[0] >= logPlaningTime[1]) {
					
					logPlaningTime[0] = 0;
					completeLogPlaning();
					
				}
			} else { //Not Enough Fuel/Input
				
				logPlaningTime[0] = 0;
				
			}
			
		}
			
		if(doingProcessingJob[1] == 10) { //Meat Carving
			
			if(meatCarvingFuel > 0 && meatCarvingInput[0] > 0) { //If there's fuel and Input
				meatCarvingTime[0] += 1 + timeSpeed;
				
				if(meatCarvingTime[0] >= meatCarvingTime[1]) {
					
					meatCarvingTime[0] = 0;
					completeMeatCarving();
					
				}
			} else { //Not Enough Fuel/Input
				
				meatCarvingTime[0] = 0;
				
			}
			
		}
		
		
	}
	
	if(doingCraftingJob[0] == true) {
		
		
		if(doingCraftingJob[1] == 13) { //Bladecrafting
			
			if(bladeCraftingInput[1] >= bladeCraftingIndex[bladeCraftingSelectedItem][2]) && (doingCraftingJob[0] == true) && (doingCraftingJob[1] == 13) {
				
				bladeCraftingTime[0] += 1 + timeSpeed;
				if(bladeCraftingTime[0] >= bladeCraftingTime[1]) { completeBladeCrafting(); }
			
			}
			
		}
		
		if(doingCraftingJob[1] == 14) { //Woodworking
			
			if(woodworkingInput[1] >= woodworkingIndex[woodworkingSelectedItem][2]) && (doingCraftingJob[0] == true) && (doingCraftingJob[1] == 14) {
				woodworkingTime[0] += 1 + timeSpeed;
				if(woodworkingTime[0] >= woodworkingTime[1]) { completeWoodworking(); }
			
			}
			
		}
		
		if(doingCraftingJob[1] == 15) { //Armor Making
			
			if(armorCraftingInput[1] >= armorCraftingIndex[armorCraftingSelectedItem][2]) && (doingCraftingJob[0] == true) && (doingCraftingJob[1] == 15) {
				armorCraftingTime[0] += 1 + timeSpeed;
				if(armorCraftingTime[0] >= armorCraftingTime[1]) { completeArmorCrafting(); }
			
			}
			
		}
		
		if(doingCraftingJob[1] == 16) { //Brewing
						
			if(doingCraftingJob[0] == true) && (doingCraftingJob[1] == 16) && brewingEnough[1] && brewingEnough[2] && brewingEnough[3] {
				brewingTime[0] += 1 + timeSpeed;
				if(brewingTime[0] >= brewingTime[1]) { completeBrewing(); }
			
			}
			
		}
		
	}
	
}

{ //Job Scrolling
	if mouse_wheel_down() jobScrolled += jobScrollSpeed;
	if mouse_wheel_up() jobScrolled -= jobScrollSpeed;
	
	if(jobScrolled < 0) jobScrolled = 0;
}

{ //Drawing Item Description
	if(drawDescriptionForItem == true) drawItemDescription(descriptionItemSlot);
}

{ //Draw Time
	/*
	draw_set_color(c_white);
	draw_set_font(fontSmall);
	draw_set_halign(fa_right);
	draw_set_valign(fa_top);
	draw_set_alpha(1);
	draw_text(screenW, 0, string(month) + "/" + string(day) + "/" + string(year) + " || " + string(hour) + ":" + string(minute) + "." + string(second) + "\n" + 
						  "Last time played was: " + string(monthP) + "/" + string(dayP) + "/" + string(yearP) + "  " + string(hourP) + "," + string(minuteP) + "." + string(secondP) + "\n" +
						  "Seconds since last played = " + string(timeSinceLastPlayed));
	*/
}



{ //Smelting
	
	if(smeltingInput[0] != 0) { //Updating Input
		
		smeltingInput[1] = countItem(smeltingInput[0]);
		
		if(smeltingInput[1] <= 0) smeltingInput[0] = 0;
		
	}
	
	smeltingFuel = countItem(10000);
	
}

{ //Log Planing
	
	if(logPlaningInput[0] != 0) { //Updating Input
		
		logPlaningInput[1] = countItem(logPlaningInput[0]);
		
		if(logPlaningInput[1] <= 0) logPlaningInput[0] = 0;
		
	}
	
	logPlaningFuel = countItem(10001);
	
}

{ //Meat Carving
	
	if(meatCarvingInput[0] != 0) { //Updating Input
		
		meatCarvingInput[1] = countItem(meatCarvingInput[0]);
		
		if(meatCarvingInput[1] <= 0) meatCarvingInput[0] = 0;
		
	}
	
	meatCarvingFuel = countItem(10001);
	
}

{ //Bladecrafting
	
	if(bladeCraftingInput[0] != 0) { //Updating Input
		
		bladeCraftingInput[1] = countItem(bladeCraftingInput[0]);
		
		if(bladeCraftingInput[1] <= 0) bladeCraftingInput[0] = 0;
		
	}
	
	//Update Required Amount
	bladeCraftingRequiredAmount = bladeCraftingIndex[bladeCraftingSelectedItem][2];
	
	if(bladeCraftingInput[1] == 0 && bladeCraftingOutput[1] == 0) { bladeCraftingInput[1] = 0; bladeCraftingInput[0] = 0; }
	
}

{ //Woodworking
	
	if(woodworkingInput[0] != 0) { //Updating Input
		
		woodworkingInput[1] = countItem(woodworkingInput[0]);
		
		if(woodworkingInput[1] <= 0) woodworkingInput[0] = 0;
		
	}
	
	//Update Required Amount
	woodworkingRequiredAmount = woodworkingIndex[woodworkingSelectedItem][2];
	
	if(woodworkingInput[1] == 0 && woodworkingOutput[1] == 0) { woodworkingInput[1] = 0; woodworkingInput[0] = 0; }
	
}

{ //Armor Crafting
	
	if(armorCraftingInput[0] != 0) { //Updating Input
		
		armorCraftingInput[1] = countItem(armorCraftingInput[0]);
		
		if(armorCraftingInput[1] <= 0) armorCraftingInput[0] = 0;
		
	}
	
	//Update Required Amount
	armorCraftingRequiredAmount = armorCraftingIndex[armorCraftingSelectedItem][2];
	
	if(armorCraftingInput[1] == 0 && armorCraftingOutput[1] == 0) { armorCraftingInput[1] = 0; armorCraftingInput[0] = 0; }
	
}

{ //Brewing
	
	brewingEnough[1] = false;
	brewingEnough[2] = false;
	brewingEnough[3] = false;

	if(countItem(brewingInputOne[0]) >= brewingInputOne[1]		or brewingInputOne[0] == 0)		brewingEnough[1] = true;
	if(countItem(brewingInputTwo[0]) >= brewingInputTwo[1]		or brewingInputTwo[0] == 0)		brewingEnough[2] = true;
	if(countItem(brewingInputThree[0]) >= brewingInputThree[1]	or brewingInputThree[0] == 0)	brewingEnough[3] = true;	
	
}






	





function drawExperienceBar() {
	
	
		draw_set_alpha(1);
	
		{ //Vars
			var mainLevel, mainXP, mainRequiredXP, mainPrevRequired, showXPBar;
			showXPBar = true;
		
			mainLevel = level[main];
			mainXP = xp[main];
			mainRequiredXP = requiredXP[main];
			mainPrevRequired = prevRequired[main];
		
		}
	
		//Bar
		draw_healthbar(mainXPBarX1, mainXPBarY1, mainXPBarX2, mainXPBarY2, ((mainXP - mainPrevRequired) / (mainRequiredXP - mainPrevRequired)) * 100, globals.xpBarBackgroundColor, globals.xpBarMinColor, globals.xpBarMaxColor, 0, true, true);
			
		//Set Font
		draw_set_color(c_white);
		draw_set_font(fontSmallBold);
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		
		/*
		//Level
		draw_text(mainXPBarX1, mainXPBarY2 + mainXPBarTextBuffer, "Level " + string(mainLevel));
		*/
		
		//XP
		draw_set_halign(fa_right);
		draw_text(mainXPBarX2, mainXPBarY2 + mainXPBarTextBuffer, "XP " + string(mainXP) + "/" + string(mainRequiredXP) + " (" + string(mainRequiredXP - mainXP) + " remaining)");
			
		//Skill Title
		var skillTitle = "";
		
		switch (main)
		{
			case 3: skillTitle = cw[3][0]; break;
			case 4: skillTitle = cw[4][0]; break;
			case 5: skillTitle = cw[5][0]; break;
			case 6: skillTitle = cw[6][0]; break;
			
			case 8: skillTitle = cw[8][0]; break;
			case 9: skillTitle = cw[9][0]; break;
			case 10: skillTitle = cw[10][0]; break;
			case 11: skillTitle = cw[11][0]; break;
			
			case 13: skillTitle = cw[13][0]; break;
			case 14: skillTitle = cw[14][0]; break;
			case 15: skillTitle = cw[15][0]; break;
			case 16: skillTitle = cw[16][0]; break;
			case 17: skillTitle = cw[17][0]; break;
			case 18: skillTitle = cw[18][0]; break;
		}
			
		draw_set_halign(fa_left);
		draw_text(mainXPBarX1, mainXPBarY2 + mainXPBarTextBuffer, skillTitle);
		//draw_text((screenW + craftingWingWidth) / 2, mainXPBarY2 + mainXPBarTextBuffer, skillTitle);
		
}