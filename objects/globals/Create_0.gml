/// @description Establishing All Vars

{ //Debugging
	
	globalvar timeSpeed;
	timeSpeed = 0;
	
}

{ //Screen Sizing
	
	globalvar screenW, screenH;
	screenW = 1920;
	screenH = 1080;
	
}

{ //Crafting Wing
	
	globalvar craftingWingWidth;
	craftingWingWidth = 300;
	
}

{ //Crafting Wing Menu Array
	
	globalvar cw;
	
	/*
	[0] String Text
	[1] Clickable?
	[2] Icon
	[3] Main Page Index
	*/
	
		
	{ //Storage
		cw[0][0] = "Storage And Banking";
		cw[0][1] = false;
	
		cw[1][0] = "Inventory";
		cw[1][1] = true;
		cw[1][2] = icon_inventory;
		cw[1][3] = 100;
	}
	
	{ //Collecting Skills Text
		cw[2][0] = "Collecting Skills"
		cw[2][1] = false;
	
		cw[3][0] = "Treecutting";
		cw[3][1] = true;
		cw[3][2] = icon_treecutting;
		cw[3][3] = 3;
	
		cw[4][0] = "Mining";
		cw[4][1] = true;
		cw[4][2] = icon_mining;
		cw[4][3] = 4;
	
		cw[5][0] = "Fishing";
		cw[5][1] = true;
		cw[5][2] = icon_fishing;
		cw[5][3] = 5;
	
		cw[6][0] = "Foraging";
		cw[6][1] = true;
		cw[6][2] = icon_foraging;
		cw[6][3] = 6;
	}
	
	{ //Processing Skills
		cw[7][0] = "Processing Skills";
		cw[7][1] = false;
	
		cw[8][0] = "Smelting";
		cw[8][1] = true;
		cw[8][2] = icon_smelting;
		cw[8][3] = 8;
	
		cw[9][0] = "Log Planing";
		cw[9][1] = true;
		cw[9][2] = icon_logPlaning;
		cw[9][3] = 9;
	
		cw[10][0] = "Meat Carving";
		cw[10][1] = true;
		cw[10][2] = icon_meatCarving;
		cw[10][3] = 10;
	
		cw[11][0] = "Farming";
		cw[11][1] = true;
		cw[11][2] = icon_farming;
		cw[11][3] = 11;
	}
	
	{ //Crafting Skills
		cw[12][0] = "Crafting Skills";
		cw[12][1] = false;
	
		cw[13][0] = "Blade Crafting";
		cw[13][1] = true;
		cw[13][2] = icon_bladeCrafting;
		cw[13][3] = 13;
	
		cw[14][0] = "Woodworking";
		cw[14][1] = true;
		cw[14][2] = icon_woodWorking;
		cw[14][3] = 14;
	
		cw[15][0] = "Armor Crafting";
		cw[15][1] = true;
		cw[15][2] = icon_armorMaking;
		cw[15][3] = 15;
	
		cw[16][0] = "Brewing";
		cw[16][1] = true;
		cw[16][2] = icon_brewing;
		cw[16][3] = 16;
	
		cw[17][0] = "Cooking";
		cw[17][1] = true;
		cw[17][2] = icon_cooking;
		cw[17][3] = 17;
	
		cw[18][0] = "Jewelry Making";
		cw[18][1] = true;
		cw[18][2] = icon_jewelryMaking;
		cw[18][3] = 18;
	}

	{ //Other
		
		cw[19][0] = "/////////";
		cw[19][1] = false;
		
		cw[20][0] = "Market";
		cw[20][1] = true;
		cw[20][2] = icon_market;
		cw[20][3] = 0;
		
		cw[21][0] = "Combat";
		cw[21][1] = true;
		cw[21][2] = icon_combat;
		cw[21][3] = 0;
		
		cw[22][0] = "Exploration";
		cw[22][1] = true;
		cw[22][2] = icon_exploration;
		cw[22][3] = 0;
		
		
	}
	

	//Counting Up Number Of Sections
	globalvar cwCount;
	cwCount = array_length(cw);

}

{ //Main
	
	globalvar main;
	main = 100;
	
}

{ //Main Experience Bar
	
	globalvar mainXPBarX1, mainXPBarX2, mainXPBarY1, mainXPBarY2, mainXPBarXBuffer, mainXPBarHeight, mainXPBarTextBuffer;
	mainXPBarXBuffer = 64;
	mainXPBarX1 = craftingWingWidth + mainXPBarXBuffer;
	mainXPBarX2 = screenW - mainXPBarXBuffer;
	mainXPBarHeight = 16;
	mainXPBarY1 = screenH - 64;
	mainXPBarY2 = mainXPBarY1 + mainXPBarHeight;
	
	mainXPBarTextBuffer = 8;
	
	}

{ //Skill Levels
	
	globalvar level,xp,requiredXP,prevRequired;
	globalvar numberOfCollectingSkills, numberOfProcessingSkills, numberOfCraftingSkills;
	numberOfCollectingSkills = 4;
	numberOfProcessingSkills = 4;
	numberOfCraftingSkills = 6;
	
	//Create All Collecting Skills
	for(i = 3; i < 3 + numberOfCollectingSkills; i++) {
		level[i] = 1;
		xp[i] = 0;
		requiredXP[i] = 10;
		prevRequired[i] = 0;
		
	}
	
	//Create All Processing Skills
	for(i = 8; i < 8 + numberOfProcessingSkills; i++) {
		level[i] = 1;
		xp[i] = 0;
		requiredXP[i] = 10;
		prevRequired[i] = 0;
		
	}
	
	//Create All Crafting Skills
	for(i = 13; i < 13 + numberOfCraftingSkills; i++) {
		
		level[i] = 1;
		xp[i] = 0;
		requiredXP[i] = 10;
		prevRequired[i] = 0;
	
	}
	
	alarm[0] = 1;
}

{ //Jobs
	
	globalvar jobScrolled, jobScrollSpeed;
	jobScrolled = 0;
	jobScrollSpeed = 45;
	
	globalvar job, doingCollectingJob, doingProcessingJob, doingCraftingJob;
	
	doingCollectingJob[0] = false; //Is the player doing a job
	doingCollectingJob[1] = 0; //Skill Index for job
	doingCollectingJob[2] = 0; //Job Index
	
	doingProcessingJob[0] = false; //If You're Doing Processing Job
	doingProcessingJob[1] = 0; //What Skill You're Using
	
	doingCraftingJob[0] = true;
	doingCraftingJob[1] = 13;
	
	{ //Job List
		
		/*
		
		[3] Treecutting
		[4] Mining
		[5] Fishing
		[6] Foraging
		
		[0] Job Name
		[1] Icon
		[2] XP Given
		[3] Time Finished
		[4] Time Required
		[5] Level Unlocked At
		[6] Collected Item ID
		[7] Collected Amount
		[8] Chance Item ID
		[9] Percent Chance
		
		*/
		
		job[0][0][0] = "";
		job[0][0][1] = spr_empty;
		job[0][0][2] = 0;
		job[0][0][3] = 0;
		job[0][0][4] = 1;
		job[0][0][5] = 0;
		job[0][0][6] = 0;
		job[0][0][7] = 0;
		job[0][0][8] = 0;
		job[0][0][9] = 0;
		
		{ //UPDATED Woodcutting
			
			var woodJobs = 67;
			
			for(i = 0; i < woodJobs; i++) { //Autoset Woodcutting Jobs
				
				job[3][i][2] = ceil(1 + (power(i,2.25))); //XP
				job[3][i][3] = 0; //Setting Amount Done To 0
				job[3][i][4] = 60 + (30 * i); //Setting Time
				if(i == 0) { job[3][i][5] = 1; } else { job[3][i][5] = 15 * i; } //Set Required Level
				
			}
			
			/*
			[6] Collected Item
			[7] Amount Dropped
			[8] Chance Item
			[9] Chance Item Chance
			*/
			
			for(i = 0; i < 6; i++) { //Autoset Rest Of Info
				
				job[3][0 + (i*11)][0] = "Pine Tier " + string(i + 1);
				job[3][0 + (i*11)][1] = job_pineTree;
				job[3][0 + (i*11)][6] = 1+i; //Item Drop ID
				job[3][0 + (i*11)][7] = 1;
				job[3][0 + (i*11)][8] = 0; //Chance Item ID
				job[3][0 + (i*11)][9] = 0;
				
				job[3][1 + (i*11)][0] = "Black Birch Tier " + string(i + 1);
				job[3][1 + (i*11)][1] = job_blackBirchTree;
				job[3][1 + (i*11)][6] = 7+i; //Item Drop ID
				job[3][1 + (i*11)][7] = 1;
				job[3][1 + (i*11)][8] = 0; //Chance Item ID
				job[3][1 + (i*11)][9] = 0;
				
				job[3][2 + (i*11)][0] = "Oak Tier " + string(i + 1);
				job[3][2 + (i*11)][1] = job_oakTree;
				job[3][2 + (i*11)][6] = 13+i; //Item Drop ID
				job[3][2 + (i*11)][7] = 1;
				job[3][2 + (i*11)][8] = 0; //Chance Item ID
				job[3][2 + (i*11)][9] = 0;
				
				job[3][3 + (i*11)][0] = "Silver Maple Tier " + string(i + 1);
				job[3][3 + (i*11)][1] = job_silverMapleTree;
				job[3][3 + (i*11)][6] = 19+i; //Item Drop ID
				job[3][3 + (i*11)][7] = 1;
				job[3][3 + (i*11)][8] = 0; //Chance Item ID
				job[3][3 + (i*11)][9] = 0;
				
				job[3][4 + (i*11)][0] = "Dogwood Tier " + string(i + 1);
				job[3][4 + (i*11)][1] = job_dogwoodTree;
				job[3][4 + (i*11)][6] = 25+i; //Item Drop ID
				job[3][4 + (i*11)][7] = 1;
				job[3][4 + (i*11)][8] = 0; //Chance Item ID
				job[3][4 + (i*11)][9] = 0;
				
				job[3][5 + (i*11)][0] = "Maple Tier " + string(i + 1);
				job[3][5 + (i*11)][1] = job_mapleTree;
				job[3][5 + (i*11)][6] = 31+i; //Item Drop ID
				job[3][5 + (i*11)][7] = 1;
				job[3][5 + (i*11)][8] = 0; //Chance Item ID
				job[3][5 + (i*11)][9] = 0;
				
				job[3][6 + (i*11)][0] = "Ash Cedar Tier " + string(i + 1);
				job[3][6 + (i*11)][1] = job_ashCedarTree;
				job[3][6 + (i*11)][6] = 37+i; //Item Drop ID
				job[3][6 + (i*11)][7] = 1;
				job[3][6 + (i*11)][8] = 0; //Chance Item ID
				job[3][6 + (i*11)][9] = 0;
				
				job[3][7 + (i*11)][0] = "Chestnut Tier " + string(i + 1);
				job[3][7 + (i*11)][1] = job_chestnutTree;
				job[3][7 + (i*11)][6] = 43+i; //Item Drop ID
				job[3][7 + (i*11)][7] = 1;
				job[3][7 + (i*11)][8] = 0; //Chance Item ID
				job[3][7 + (i*11)][9] = 0;
				
				job[3][8 + (i*11)][0] = "Willow Tier " + string(i + 1);
				job[3][8 + (i*11)][1] = job_willowTree;
				job[3][8 + (i*11)][6] = 49+i; //Item Drop ID
				job[3][8 + (i*11)][7] = 1;
				job[3][8 + (i*11)][8] = 0; //Chance Item ID
				job[3][8 + (i*11)][9] = 0;
				
				job[3][9 + (i*11)][0] = "Palm Tier " + string(i + 1);
				job[3][9 + (i*11)][1] = job_palmTree;
				job[3][9 + (i*11)][6] = 55+i; //Item Drop ID
				job[3][9 + (i*11)][7] = 1;
				job[3][9 + (i*11)][8] = 0; //Chance Item ID
				job[3][9 + (i*11)][9] = 0;
				
				job[3][10 + (i*11)][0] = "Spruce Tier " + string(i + 1);
				job[3][10 + (i*11)][1] = job_spruceTree;
				job[3][10 + (i*11)][6] = 61+i; //Item Drop ID
				job[3][10 + (i*11)][7] = 1;
				job[3][10 + (i*11)][8] = 0; //Chance Item ID
				job[3][10 + (i*11)][9] = 0;
				
			}
			
			//Final Job
			job[3][66][0] = "Magic Burger Tree";
			job[3][66][1] = job_magicBurgerTree;
			job[3][66][6] = 67;
			job[3][66][7] = 1;
			job[3][66][8] = 0;
			job[3][66][9] = 0;
			
		}
		
		{ //UPDATED Mining
			
			var miningJobs = 67;
			
			for(i = 0; i < miningJobs; i++) { //Autoset Woodcutting Jobs
				
				job[4][i][2] = ceil(1 + (power(i,2.25))); //XP
				job[4][i][3] = 0; //Setting Amount Done To 0
				job[4][i][4] = 120 + (60 * i); //Setting Time
				if(i == 0) { job[4][i][5] = 1; } else { job[4][i][5] = 15 * i; } //Set Required Level
				
			}
			
			/*
			[6] Collected Item
			[7] Amount Dropped
			[8] Chance Item
			[9] Chance Item Chance
			*/
			
			for(i = 0; i < 6; i++) { //Autoset Rest Of Info
				
				job[4][0 + (i*11)][0] = "Mine Lead Tier " + string(i + 1);
				job[4][0 + (i*11)][1] = item_leadOre;
				job[4][0 + (i*11)][6] = 68+i; //Item Drop ID
				job[4][0 + (i*11)][7] = 1;
				job[4][0 + (i*11)][8] = 0; //Chance Item ID
				job[4][0 + (i*11)][9] = 0;
				
				job[4][1 + (i*11)][0] = "Mine Tin Tier " + string(i + 1);
				job[4][1 + (i*11)][1] = item_tinOre;
				job[4][1 + (i*11)][6] = 74+i; //Item Drop ID
				job[4][1 + (i*11)][7] = 1;
				job[4][1 + (i*11)][8] = 0; //Chance Item ID
				job[4][1 + (i*11)][9] = 0;
				
				job[4][2 + (i*11)][0] = "Mine Gold Tier " + string(i + 1);
				job[4][2 + (i*11)][1] = item_goldOre;
				job[4][2 + (i*11)][6] = 80+i; //Item Drop ID
				job[4][2 + (i*11)][7] = 1;
				job[4][2 + (i*11)][8] = 0; //Chance Item ID
				job[4][2 + (i*11)][9] = 0;
				
				job[4][3 + (i*11)][0] = "Mine Silver Tier " + string(i + 1);
				job[4][3 + (i*11)][1] = item_silverOre;
				job[4][3 + (i*11)][6] = 86+i; //Item Drop ID
				job[4][3 + (i*11)][7] = 1;
				job[4][3 + (i*11)][8] = 0; //Chance Item ID
				job[4][3 + (i*11)][9] = 0;
				
				job[4][4 + (i*11)][0] = "Mine Copper Tier " + string(i + 1);
				job[4][4 + (i*11)][1] = item_copperOre;
				job[4][4 + (i*11)][6] = 92+i; //Item Drop ID
				job[4][4 + (i*11)][7] = 1;
				job[4][4 + (i*11)][8] = 0; //Chance Item ID
				job[4][4 + (i*11)][9] = 0;
				
				job[4][5 + (i*11)][0] = "Mine Brass Tier " + string(i + 1);
				job[4][5 + (i*11)][1] = item_brassOre;
				job[4][5 + (i*11)][6] = 98+i; //Item Drop ID
				job[4][5 + (i*11)][7] = 1;
				job[4][5 + (i*11)][8] = 0; //Chance Item ID
				job[4][5 + (i*11)][9] = 0;
				
				job[4][6 + (i*11)][0] = "Mine Aluminum Tier " + string(i + 1);
				job[4][6 + (i*11)][1] = item_aluminumOre;
				job[4][6 + (i*11)][6] = 104+i; //Item Drop ID
				job[4][6 + (i*11)][7] = 1;
				job[4][6 + (i*11)][8] = 0; //Chance Item ID
				job[4][6 + (i*11)][9] = 0;
				
				job[4][7 + (i*11)][0] = "Mine Iron Tier " + string(i + 1);
				job[4][7 + (i*11)][1] = item_ironOre;
				job[4][7 + (i*11)][6] = 110+i; //Item Drop ID
				job[4][7 + (i*11)][7] = 1;
				job[4][7 + (i*11)][8] = 0; //Chance Item ID
				job[4][7 + (i*11)][9] = 0;
				
				job[4][8 + (i*11)][0] = "Mine Titanium Tier " + string(i + 1);
				job[4][8 + (i*11)][1] = item_titaniumOre;
				job[4][8 + (i*11)][6] = 116+i; //Item Drop ID
				job[4][8 + (i*11)][7] = 1;
				job[4][8 + (i*11)][8] = 0; //Chance Item ID
				job[4][8 + (i*11)][9] = 0;
				
				job[4][9 + (i*11)][0] = "Mine Platinum Tier " + string(i + 1);
				job[4][9 + (i*11)][1] = item_platinumOre;
				job[4][9 + (i*11)][6] = 122+i; //Item Drop ID
				job[4][9 + (i*11)][7] = 1;
				job[4][9 + (i*11)][8] = 0; //Chance Item ID
				job[4][9 + (i*11)][9] = 0;
				
				job[4][10 + (i*11)][0] = "Mine Tungsten Tier " + string(i + 1);
				job[4][10 + (i*11)][1] = item_tungstenOre;
				job[4][10 + (i*11)][6] = 128+i; //Item Drop ID
				job[4][10 + (i*11)][7] = 1;
				job[4][10 + (i*11)][8] = 0; //Chance Item ID
				job[4][10 + (i*11)][9] = 0;
				
			}
			
			//Final Job
			job[4][66][0] = "Mine Soulscape Iron";
			job[4][66][1] = item_soulscapeOre;
			job[4][66][6] = 134;
			job[4][66][7] = 1;
			job[4][66][8] = 0;
			job[4][66][9] = 0;
			
		}
		
		{ //UPDATED Fishing
			
			/*
			[0] Fishing Spot Name
			[1] Level Unlocked
			[2] Drop Info [ids/chance/xp]
			[3] Time Done
			[4] Time
			*/
			
			job[5][0][0] = "Forested Fishing Hole";
			job[5][0][1] = 1;
			job[5][0][2] = "135,136,137,138,139/100,50,25,10,2/1,2,3,4,5/";
			job[5][0][3] = 0;
			job[5][0][4] = 120;
			
			job[5][1][0] = "Forested Fishing Hole";
			job[5][1][1] = 100;
			job[5][1][2] = "1,2,3/50,25,25/1,2,3/";
			job[5][1][3] = 0;
			job[5][1][4] = 120;
			
		}
		
		{ //Foraging
			
			/*
			[0] Searching Spot Name
			[1] Level Unlocked
			[2] Drop Info [ids/chance/xp]
			[5] Time Done
			[6] Time
			*/
			
			job[6][0][0] = "Search Your Backyard";
			job[6][0][1] = 1;
			job[6][0][2] = "202,203,204,205,206,207,208,209,537/100,80,70,60,50,40,30,20,10/1,1,2,2,3,3,5,5,1/";
			job[6][0][3] = 0;
			job[6][0][4] = 120;
			
			job[6][1][0] = "Search Your Backyard";
			job[6][1][1] = 10;
			job[6][1][2] = "202,203,204,205,206,207,208,209/100,80,60,50,40,30,20,10/1,1,2,2,3,3,5,5/";
			job[6][1][3] = 0;
			job[6][1][4] = 120;
			
			job[6][2][0] = "Search Your Backyard";
			job[6][2][1] = 20;
			job[6][2][2] = "202,203,204,205,206,207,208,209/100,80,60,50,40,30,20,10/1,1,2,2,3,3,5,5/";
			job[6][2][3] = 0;
			job[6][2][4] = 120;
			
		}
		
	}
	
	{ //Job Icon Vars
		
		globalvar jobIconWidth, jobIconHeight, jobIconX, jobIconY, jobIconBuffer;
		jobIconWidth = 640;
		jobIconHeight = 128;
		
		jobIconX = mainXPBarX1;
		jobIconY = 32;
		jobIconBuffer = 16;
		
		globalvar jobIconCompX, jobIconCompY, jobIconCompWidth, jobIconCompHeight, jobIconCompBuffer;
		jobIconCompBuffer = 4;
		jobIconCompWidth = jobIconWidth / 3;
		jobIconCompHeight = 16;
		
		jobIconCompX = (jobIconWidth - (jobIconWidth / 3)) - jobIconCompBuffer;
		jobIconCompY = jobIconHeight - jobIconCompHeight - jobIconCompBuffer;
		
	}
	
}

{ //Items
	
	/*
	[0] name
	[1] description
	[2] sprite
	[3] base value
	*/
	
	//EDIT
	/*
	[0] name
	[1] description
	[2] sprite
	[3] base value
	[4] quality level
	*/
	
	globalvar item;
	item[0][0] = "";
	item[0][1] = "";
	item[0][2] = spr_empty;
	item[0][3] = 0;
	item[0][4] = 0;
	
	{ //Quality Levels
		
		globalvar qualityLevel;
		qualityLevel[1] = "Rookiemade";
		qualityLevel[2] = "Student Crafted";
		qualityLevel[3] = "Proficiently Crafted";
		qualityLevel[4] = "Skillfully Crafted";
		qualityLevel[5] = "Expertly Crafted";
		qualityLevel[6] = "Mastercrafted";
		
		globalvar qualityColor;
		qualityColor[1] = rookieColor;
		qualityColor[2] = studentColor;
		qualityColor[3] = proficientColor;
		qualityColor[4] = skillfulColor;
		qualityColor[5] = expertColor;
		qualityColor[6] = masterColor;
		
	}
	
	{ //Logs
		
		var qualityLevels = 6;
		
		for(i = 0; i < qualityLevels; i++) {
			
			item[1+i][0] = "Pine Log";
			item[1+i][1] = "";
			item[1+i][2] = item_pineLog;
			item[1+i][4] = i + 1;
			item[1+i][3] = 1 + power(i,10);
			
			item[7+i][0] = "Black Birch";
			item[7+i][1] = "";
			item[7+i][2] = item_blackBirchLog;
			item[7+i][4] = i + 1;
			item[7+i][3] = 1 + power(i,10);
			
			item[13+i][0] = "Oak Log";
			item[13+i][1] = "";
			item[13+i][2] = item_oakLog;
			item[13+i][4] = 1 + i;
			item[13+i][3] = 1 + power(i,10);
			
			item[19+i][0] = "Silver Maple Log";
			item[19+i][1] = "";
			item[19+i][2] = item_silverMapleLog;
			item[19+i][4] = 1 + i;
			item[19+i][3] = 1 + power(i,10);
			
			item[25+i][0] = "Dogwood Log";
			item[25+i][1] = "";
			item[25+i][2] = item_dogwoodLog;
			item[25+i][4] = 1 + i;
			item[25+i][3] = 1 + power(i,10);
			
			item[31+i][0] = "Maple Log";
			item[31+i][1] = "";
			item[31+i][2] = item_mapleLog;
			item[31+i][4] = 1 + i;
			item[31+i][3] = 1 + power(i,10);
			
			item[37+i][0] = "Ash Cedar Log";
			item[37+i][1] = "";
			item[37+i][2] = item_ashCedarLog;
			item[37+i][4] = 1 + i;
			item[37+i][3] = 1 + power(i,10);
			
			item[43+i][0] = "Chestnut Log";
			item[43+i][1] = "";
			item[43+i][2] = item_chestnutLog;
			item[43+i][4] = 1 + i;
			item[43+i][3] = 1 + power(i,10);
			
			item[49+i][0] = "Willow Log";
			item[49+i][1] = "";
			item[49+i][2] = item_willowLog;
			item[49+i][4] = 1 + i;
			item[49+i][3] = 1 + power(i,10);
			
			item[55+i][0] = "Palm Log";
			item[55+i][1] = "";
			item[55+i][2] = item_pineLog;
			item[55+i][4] = 1 + i;
			item[55+i][3] = 1 + power(i,10);
			
			item[61+i][0] = "Spruce Log";
			item[61+i][1] = "";
			item[61+i][2] = item_spruceLog;
			item[61+i][4] = 1 + i;
			item[61+i][3] = 1 + power(i,10);
			
		}
		
		//Final Log
		item[67][0] = "Magic Burger Tree Log";
		item[67][1] = "";
		item[67][2] = item_magicBurgerLog;
		item[67][4] = 6;
		item[67][3] = 1;
		
		
		
		{ //Setting Values
			
			for(Colum = 1; Colum < 7; Colum++) {
				
				for(Layer = 0; Layer < 11; Layer ++) {
					
					var ItemID = Colum + (Layer * 6);
					var ActualID = ((Colum - 1) * 11) + Layer
					
					item[ItemID][3] = 1 + round(power(ActualID,1.5));
					
				}
				
			}
			
			item[67][3] = item[66][3] + 100;
			
		}
		
	}
		
	{ //Fish
		
		item[135][0] = "Cod";
		item[135][1] = "";
		item[135][2] = item_cod;
		item[135][4] = 1;
		item[135][3] = 1;
		
		item[136][0] = "Trout";
		item[136][1] = "";
		item[136][2] = item_trout;
		item[136][4] = 1;
		item[136][3] = 1;
		
		item[137][0] = "Carp";
		item[137][1] = "";
		item[137][2] = item_carp;
		item[137][4] = 1;
		item[137][3] = 1;
		
		item[138][0] = "Crayfish";
		item[138][1] = "";
		item[138][2] = item_crayfish;
		item[138][4] = 1;
		item[138][3] = 1;
		
		item[139][0] = "Spotted Bass";
		item[139][1] = "";
		item[139][2] = item_spottedBass;
		item[139][4] = 1;
		item[139][3] = 1;
		
	}
		
	{ //Ores
		
		var qualityLevels = 6;
		
		for(i = 0; i < qualityLevels; i++) {
			
			item[68+i][0] = "Lead Ore";
			item[68+i][1] = "";
			item[68+i][2] = item_leadOre;
			item[68+i][4] = i + 1;
			
			item[74+i][0] = "Tin Ore";
			item[74+i][1] = "";
			item[74+i][2] = item_tinOre;
			item[74+i][4] = i + 1;
			
			item[80+i][0] = "Gold Ore";
			item[80+i][1] = "";
			item[80+i][2] = item_goldOre;
			item[80+i][4] = 1 + i;
			
			item[86+i][0] = "Silver Ore";
			item[86+i][1] = "";
			item[86+i][2] = item_silverOre;
			item[86+i][4] = 1 + i;
			
			item[92+i][0] = "Copper Ore";
			item[92+i][1] = "";
			item[92+i][2] = item_copperOre;
			item[92+i][4] = 1 + i;
			
			item[98+i][0] = "Brass Ore";
			item[98+i][1] = "";
			item[98+i][2] = item_brassOre;
			item[98+i][4] = 1 + i;
			
			item[104+i][0] = "Aluminum Ore";
			item[104+i][1] = "";
			item[104+i][2] = item_aluminumOre;
			item[104+i][4] = 1 + i;
			
			item[110+i][0] = "Iron Ore";
			item[110+i][1] = "";
			item[110+i][2] = item_ironOre;
			item[110+i][4] = 1 + i;
			
			item[116+i][0] = "Titanium Ore";
			item[116+i][1] = "";
			item[116+i][2] = item_titaniumOre;
			item[116+i][4] = 1 + i;
			
			item[122+i][0] = "Platinum Ore";
			item[122+i][1] = "";
			item[122+i][2] = item_platinumOre;
			item[122+i][4] = 1 + i;
			
			item[128+i][0] = "Tungsten Ore";
			item[128+i][1] = "";
			item[128+i][2] = item_tungstenOre;
			item[128+i][4] = 1 + i;
			
		}
		
		//Final Log
		item[134][0] = "Soulscape Ore";
		item[134][1] = "";
		item[134][2] = item_soulscapeOre;
		item[134][4] = 6;
		item[134][3] = 1;
		
		
		
		{ //Setting Values
			
			for(Colum = 1; Colum < 7; Colum++) {
				
				for(Layer = 0; Layer < 11; Layer ++) {
					
					var ItemID = Colum + (Layer * 6) + 67;
					var ActualID = ((Colum - 1) * 11) + Layer + 67;
					
					item[ItemID][3] = 2 + round(power(ActualID - 67,1.75));
					
				}
				
			}
			
			item[134][3] = item[133][3] + 100;
			
		}
		
	}
	
	{ //Foraging Stuff
		
		item[202][0] = "Leaf";
		item[202][1] = "";
		item[202][2] = item_leaf;
		item[202][4] = 1;
		item[202][3] = 1;
		
		item[203][0] = "Stick";
		item[203][1] = "";
		item[203][2] = item_stick;
		item[203][4] = 1;
		item[203][3] = 1;
		
		item[204][0] = "Rock";
		item[204][1] = "";
		item[204][2] = item_rock;
		item[204][4] = 1;
		item[204][3] = 1;
		
		item[205][0] = "Pinecone";
		item[205][1] = "";
		item[205][2] = item_pinecone;
		item[205][4] = 1;
		item[205][3] = 1;
		
		item[206][0] = "Worm";
		item[206][1] = "";
		item[206][2] = item_worm;
		item[206][4] = 1;
		item[206][3] = 1;
		
		item[207][0] = "Acorn";
		item[207][1] = "";
		item[207][2] = item_acorn;
		item[207][4] = 1;
		item[207][3] = 1;
		
		item[208][0] = "Buttercup";
		item[108][1] = "";
		item[208][2] = item_buttercupFlower;
		item[208][4] = 1;
		item[208][3] = 1;
		
		item[209][0] = "Wild Onion";
		item[209][1] = "";
		item[209][2] = item_wildOnion;
		item[209][4] = 1;
		item[209][3] = 1;
		
	}
	
	{ //Bars
		
		var qualityLevels = 6;
		
		for(i = 0; i < qualityLevels; i++) {
			
			item[269+i][0] = "Lead Bar";
			item[269+i][1] = "";
			item[269+i][2] = item_leadBar;
			item[269+i][4] = i + 1;
			
			item[275+i][0] = "Tin Bar";
			item[275+i][1] = "";
			item[275+i][2] = item_tinBar;
			item[275+i][4] = i + 1;
			
			item[281+i][0] = "Gold Bar";
			item[281+i][1] = "";
			item[281+i][2] = item_goldBar;
			item[281+i][4] = 1 + i;
			
			item[287+i][0] = "Silver Bar";
			item[287+i][1] = "";
			item[287+i][2] = item_silverBar;
			item[287+i][4] = 1 + i;
			
			item[293+i][0] = "Copper Bar";
			item[293+i][1] = "";
			item[293+i][2] = item_copperBar;
			item[293+i][4] = 1 + i;
			
			item[299+i][0] = "Brass Bar";
			item[299+i][1] = "";
			item[299+i][2] = item_brassBar;
			item[299+i][4] = 1 + i;
			
			item[305+i][0] = "Aluminum Bar";
			item[305+i][1] = "";
			item[305+i][2] = item_aluminumBar;
			item[305+i][4] = 1 + i;
			
			item[311+i][0] = "Iron Bar";
			item[311+i][1] = "";
			item[311+i][2] = item_ironBar;
			item[311+i][4] = 1 + i;
			
			item[317+i][0] = "Titanium Bar";
			item[317+i][1] = "";
			item[317+i][2] = item_titaniumBar;
			item[317+i][4] = 1 + i;
			
			item[323+i][0] = "Platinum Bar";
			item[323+i][1] = "";
			item[323+i][2] = item_platinumBar;
			item[323+i][4] = 1 + i;
			
			item[329+i][0] = "Tungsten Bar";
			item[329+i][1] = "";
			item[329+i][2] = item_tungstenBar;
			item[329+i][4] = 1 + i;
			
		}
		
		//Final Bar
		item[335][0] = "Soulscape Bar";
		item[335][1] = "";
		item[335][2] = item_soulscapeBar;
		item[335][4] = 6;
		item[335][3] = 1;
		
		
		
		{ //Setting Values
			
			for(Colum = 1; Colum < 7; Colum++) {
				
				for(Layer = 0; Layer < 11; Layer ++) {
					
					var ItemID = Colum + (Layer * 6) + 67;
					var ActualID = ((Colum - 1) * 11) + Layer + 67;
					
					item[ItemID][3] = 2 + round(power(ActualID - 67,1.75));
					
				}
				
			}
			
			item[134][3] = item[133][3] + 100;
			
		}
		
	}
		
	{ //Planks
		
		var qualityLevels = 6;
		
		for(i = 0; i < qualityLevels; i++) {
			
			item[336+i][0] = "Pine Plank";
			item[336+i][1] = "";
			item[336+i][2] = item_pinePlank;
			item[336+i][4] = i + 1;
			item[336+i][3] = 1 + power(i,10);
			
			item[342+i][0] = "Black Birch";
			item[342+i][1] = "";
			item[342+i][2] = item_blackBirchPlank;
			item[342+i][4] = i + 1;
			item[342+i][3] = 1 + power(i,10);
			
			item[348+i][0] = "Oak Plank";
			item[348+i][1] = "";
			item[348+i][2] = item_oakPlank;
			item[348+i][4] = 1 + i;
			item[348+i][3] = 1 + power(i,10);
			
			item[354+i][0] = "Silver Maple Plank";
			item[354+i][1] = "";
			item[354+i][2] = item_silverMaplePlank;
			item[354+i][4] = 1 + i;
			item[354+i][3] = 1 + power(i,10);
			
			item[360+i][0] = "Dogwood Plank";
			item[360+i][1] = "";
			item[360+i][2] = item_dogwoodPlank;
			item[360+i][4] = 1 + i;
			item[360+i][3] = 1 + power(i,10);
			
			item[366+i][0] = "Maple Plank";
			item[366+i][1] = "";
			item[366+i][2] = item_maplePlank;
			item[366+i][4] = 1 + i;
			item[366+i][3] = 1 + power(i,10);
			
			item[372+i][0] = "Ash Cedar Plank";
			item[372+i][1] = "";
			item[372+i][2] = item_ashCedarPlank;
			item[372+i][4] = 1 + i;
			item[372+i][3] = 1 + power(i,10);
			
			item[378+i][0] = "Chestnut Plank";
			item[378+i][1] = "";
			item[378+i][2] = item_chestnutPlank;
			item[378+i][4] = 1 + i;
			item[378+i][3] = 1 + power(i,10);
			
			item[384+i][0] = "Willow Plank";
			item[384+i][1] = "";
			item[384+i][2] = item_willowPlank;
			item[384+i][4] = 1 + i;
			item[384+i][3] = 1 + power(i,10);
			
			item[390+i][0] = "Palm Plank";
			item[390+i][1] = "";
			item[390+i][2] = item_palmPlank;
			item[390+i][4] = 1 + i;
			item[390+i][3] = 1 + power(i,10);
			
			item[396+i][0] = "Spruce Plank";
			item[396+i][1] = "";
			item[396+i][2] = item_sprucePlank;
			item[396+i][4] = 1 + i;
			item[396+i][3] = 1 + power(i,10);
			
		}
		
		//Final Plank
		item[402][0] = "Magic Burger Tree Plank";
		item[402][1] = "";
		item[402][2] = item_magicBurgerPlank;
		item[402][4] = 6;
		item[402][3] = 1;
		
		
		
		{ //Setting Values
			
			for(Colum = 1; Colum < 7; Colum++) {
				
				for(Layer = 0; Layer < 11; Layer ++) {
					
					var ItemID = Colum + (Layer * 6);
					var ActualID = ((Colum - 1) * 11) + Layer
					
					item[ItemID][3] = 1 + round(power(ActualID,1.5));
					
				}
				
			}
			
			item[402][3] = item[66][3] + 100;
			
		}
		
	}	
	
	{ //Fish Meat
		
		item[403][0] = "Cod Meat";
		item[403][1] = "";
		item[403][2] = item_codMeat;
		item[403][4] = 1;
		item[403][3] = 1;
		
		item[404][0] = "Trout Meat";
		item[404][1] = "";
		item[404][2] = item_troutMeat;
		item[404][4] = 1;
		item[404][3] = 1;
		
		item[405][0] = "Carp Meat";
		item[405][1] = "";
		item[405][2] = item_carpMeat;
		item[405][4] = 1;
		item[405][3] = 1;
		
		item[406][0] = "Crayfish Meat";
		item[406][1] = "";
		item[406][2] = item_crayfishMeat;
		item[406][4] = 1;
		item[406][3] = 1;
		
		item[407][0] = "Spotted Bass Meat";
		item[407][1] = "";
		item[407][2] = item_spottedBassMeat;
		item[407][4] = 1;
		item[407][3] = 1;
		
	}
	
	{ //Cooked Cod
		
		item[470][0] = "Cooked Cod";
		item[470][1] = "";
		item[470][2] = item_cookedCod;
		item[470][4] = 1;
		
	}
	
	{ //Seeds
		
		item[537][0] = "Grass Seed";
		item[537][1] = "";
		item[537][2] = item_grassSeed;
		item[537][4] = 1;
		item[537][3] = 1;
		
		item[538][0] = "Carrot Seed";
		item[538][1] = "";
		item[538][2] = item_carrotSeed;
		item[538][4] = 1;
		item[538][3] = 1;
		
		item[539][0] = "Celery Seed";
		item[539][1] = "";
		item[539][2] = item_celerySeed;
		item[539][4] = 1;
		item[539][3] = 1;
		
		item[540][0] = "Corn Seed";
		item[540][1] = "";
		item[540][2] = item_cornSeed;
		item[540][4] = 1;
		item[540][3] = 1;
		
		item[541][0] = "Cucumber Seed";
		item[541][1] = "";
		item[541][2] = item_cucumberSeed;
		item[541][4] = 1;
		item[541][3] = 1;
		
		item[542][0] = "Lettuce Seed";
		item[542][1] = "";
		item[542][2] = item_lettuceSeed;
		item[542][4] = 1;
		item[542][3] = 1;
		
		item[543][0] = "Radish Seed";
		item[543][1] = "";
		item[543][2] = item_radishSeed;
		item[543][4] = 1;
		item[543][3] = 1;
		
		item[544][0] = "Spinach Seed";
		item[544][1] = "";
		item[544][2] = item_spinachSeed;
		item[544][4] = 1;
		item[544][3] = 1;
		
		item[545][0] = "Strawberry Seed";
		item[545][1] = "";
		item[545][2] = item_strawberrySeed;
		item[545][4] = 1;
		item[545][3] = 1;
		
		item[546][0] = "Squash Seed";
		item[546][1] = "";
		item[546][2] = item_squashSeed;
		item[546][4] = 1;
		item[546][3] = 1;
		
	}
	
	{ //Harvested Produce
		
		item[604][0] = "Hay";
		item[604][1] = "";
		item[604][2] = item_hay;
		item[604][4] = 1;
		item[604][3] = 1;
		
		item[605][0] = "Carrot";
		item[605][1] = "";
		item[605][2] = item_carrot;
		item[605][4] = 1;
		item[605][3] = 1;
		
		item[606][0] = "Celery";
		item[606][1] = "";
		item[606][2] = item_celery;
		item[606][4] = 1;
		item[606][3] = 1;
		
		item[607][0] = "Corn";
		item[607][1] = "";
		item[607][2] = item_corn;
		item[607][4] = 1;
		item[607][3] = 1;
		
		item[608][0] = "Cucumber";
		item[608][1] = "";
		item[608][2] = item_cucumber;
		item[608][4] = 1;
		item[608][3] = 1;
		
		item[609][0] = "Lettuce";
		item[609][1] = "";
		item[609][2] = item_lettuce;
		item[609][4] = 1;
		item[609][3] = 1;
		
		item[610][0] = "Radish";
		item[610][1] = "";
		item[610][2] = item_radish;
		item[610][4] = 1;
		item[610][3] = 1;
		
		item[611][0] = "Spinach";
		item[611][1] = "";
		item[611][2] = item_spinach;
		item[611][4] = 1;
		item[611][3] = 1;
		
		item[612][0] = "Strawberry";
		item[612][1] = "";
		item[612][2] = item_strawberry;
		item[612][4] = 1;
		item[612][3] = 1;
		
		item[613][0] = "Squash";
		item[613][1] = "";
		item[613][2] = item_squash;
		item[613][4] = 1;
		item[613][3] = 1;
		
	}
	
	{ //Misc
		
		item[10000][0] = "Coal Chunk";
		item[10000][1] = "";
		item[10000][2] = item_coalChunk;
		item[10000][3] = 0;
		item[10000][4] = 1;
		
		item[10001][0] = "Sharpening Stone";
		item[10001][1] = "";
		item[10001][2] = item_sharpeningStone;
		item[10001][3] = 0;
		item[10001][4] = 1;
		
	}
		
	{ //Blade Crafting
		
		{ //Carving Knives
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[2001+i][0] = "Lead Carving Knife";
				item[2001+i][1] = "";
				item[2001+i][2] = item_leadCarvingKnife;
				item[2001+i][4] = i + 1;
			
				item[2007+i][0] = "Tin Carving Knife";
				item[2007+i][1] = "";
				item[2007+i][2] = item_tinCarvingKnife;
				item[2007+i][4] = i + 1;
			
				item[2013+i][0] = "Gold Carving Knife";
				item[2013+i][1] = "";
				item[2013+i][2] = item_goldCarvingKnife;
				item[2013+i][4] = 1 + i;
			
				item[2019+i][0] = "Silver Carving Knife";
				item[2019+i][1] = "";
				item[2019+i][2] = item_silverCarvingKnife;
				item[2019+i][4] = 1 + i;
			
				item[2025+i][0] = "Copper Carving Knife";
				item[2025+i][1] = "";
				item[2025+i][2] = item_copperCarvingKnife;
				item[2025+i][4] = 1 + i;
			
				item[2031+i][0] = "Brass Carving Knife";
				item[2031+i][1] = "";
				item[2031+i][2] = item_brassCarvingKnife;
				item[2031+i][4] = 1 + i;
			
				item[2037+i][0] = "Aluminum Carving Knife";
				item[2037+i][1] = "";
				item[2037+i][2] = item_aluminumCarvingKnife;
				item[2037+i][4] = 1 + i;
			
				item[2043+i][0] = "Iron Carving Knife";
				item[2043+i][1] = "";
				item[2043+i][2] = item_ironCarvingKnife;
				item[2043+i][4] = 1 + i;
			
				item[2049+i][0] = "Titanium Carving Knife";
				item[2049+i][1] = "";
				item[2049+i][2] = item_titaniumCarvingKnife;
				item[2049+i][4] = 1 + i;
			
				item[2055+i][0] = "Platinum Carving Knife";
				item[2055+i][1] = "";
				item[2055+i][2] = item_platinumCarvingKnife;
				item[2055+i][4] = 1 + i;
			
				item[2061+i][0] = "Tungsten Carving Knife";
				item[2061+i][1] = "";
				item[2061+i][2] = item_tungstenCarvingKnife;
				item[2061+i][4] = 1 + i;
			
			}
			
			item[2067][0] = "Soulscape Carving Knife";
			item[2067][1] = "";
			item[2067][2] = item_soulscapeCarvingKnife;
			item[2067][4] = 6;
			
		}
		
		{ //Practice Sword
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[2068+i][0] = "Lead Practice Sword";
				item[2068+i][1] = "";
				item[2068+i][2] = item_leadPracticeSword;
				item[2068+i][4] = i + 1;
			
				item[2074+i][0] = "Tin Practice Sword";
				item[2074+i][1] = "";
				item[2074+i][2] = item_tinPracticeSword;
				item[2074+i][4] = i + 1;
			
				item[2080+i][0] = "Gold Practice Sword";
				item[2080+i][1] = "";
				item[2080+i][2] = item_goldPracticeSword;
				item[2080+i][4] = 1 + i;
			
				item[2086+i][0] = "Silver Practice Sword";
				item[2086+i][1] = "";
				item[2086+i][2] = item_silverPracticeSword;
				item[2086+i][4] = 1 + i;
			
				item[2092+i][0] = "Copper Practice Sword";
				item[2092+i][1] = "";
				item[2092+i][2] = item_copperPracticeSword;
				item[2092+i][4] = 1 + i;
			
				item[2098+i][0] = "Brass Practice Sword";
				item[2098+i][1] = "";
				item[2098+i][2] = item_brassPracticeSword;
				item[2098+i][4] = 1 + i;
			
				item[2104+i][0] = "Aluminum Practice Sword";
				item[2104+i][1] = "";
				item[2104+i][2] = item_aluminumPracticeSword;
				item[2104+i][4] = 1 + i;
			
				item[2110+i][0] = "Iron Practice Sword";
				item[2110+i][1] = "";
				item[2110+i][2] = item_ironPracticeSword;
				item[2110+i][4] = 1 + i;
			
				item[2116+i][0] = "Titanium Practice Sword";
				item[2116+i][1] = "";
				item[2116+i][2] = item_titaniumPracticeSword;
				item[2116+i][4] = 1 + i;
			
				item[2122+i][0] = "Platinum Practice Sword";
				item[2122+i][1] = "";
				item[2122+i][2] = item_platinumPracticeSword;
				item[2122+i][4] = 1 + i;
			
				item[2128+i][0] = "Tungsten Practice Sword";
				item[2128+i][1] = "";
				item[2128+i][2] = item_tungstenPracticeSword;
				item[2128+i][4] = 1 + i;
			
			}
			
			item[2134][0] = "Soulscape Practice Sword";
			item[2134][1] = "";
			item[2134][2] = item_soulscapePracticeSword;
			item[2134][4] = 6;
			
		}
		
		{ //Dagger
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[2135+i][0] = "Lead Dagger";
				item[2135+i][1] = "";
				item[2135+i][2] = item_leadDagger;
				item[2135+i][4] = i + 1;
			
				item[2141+i][0] = "Tin Dagger";
				item[2141+i][1] = "";
				item[2141+i][2] = item_tinDagger;
				item[2141+i][4] = i + 1;
			
				item[2147+i][0] = "Gold Dagger";
				item[2147+i][1] = "";
				item[2147+i][2] = item_goldDagger;
				item[2147+i][4] = 1 + i;
			
				item[2153+i][0] = "Silver Dagger";
				item[2153+i][1] = "";
				item[2153+i][2] = item_silverDagger;
				item[2153+i][4] = 1 + i;
			
				item[2159+i][0] = "Copper Dagger";
				item[2159+i][1] = "";
				item[2159+i][2] = item_copperDagger;
				item[2159+i][4] = 1 + i;
			
				item[2165+i][0] = "Brass Dagger";
				item[2165+i][1] = "";
				item[2165+i][2] = item_brassDagger;
				item[2165+i][4] = 1 + i;
			
				item[2171+i][0] = "Aluminum Dagger";
				item[2171+i][1] = "";
				item[2171+i][2] = item_aluminumDagger;
				item[2171+i][4] = 1 + i;
			
				item[2177+i][0] = "Iron Dagger";
				item[2177+i][1] = "";
				item[2177+i][2] = item_ironDagger;
				item[2177+i][4] = 1 + i;
			
				item[2183+i][0] = "Titanium Dagger";
				item[2183+i][1] = "";
				item[2183+i][2] = item_titaniumDagger;
				item[2183+i][4] = 1 + i;
			
				item[2189+i][0] = "Platinum Dagger";
				item[2189+i][1] = "";
				item[2189+i][2] = item_platinumDagger;
				item[2189+i][4] = 1 + i;
			
				item[2195+i][0] = "Tungsten Dagger";
				item[2195+i][1] = "";
				item[2195+i][2] = item_tungstenDagger;
				item[2195+i][4] = 1 + i;
			
			}
			
			item[2201][0] = "Soulscape Dagger";
			item[2201][1] = "";
			item[2201][2] = item_soulscapeDagger;
			item[2201][4] = 6;
			
		}
		
		{ //Shortsword
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[2202+i][0] = "Lead Shortsword";
				item[2202+i][1] = "";
				item[2202+i][2] = item_leadShortsword;
				item[2202+i][4] = i + 1;
			
				item[2208+i][0] = "Tin Shortsword";
				item[2208+i][1] = "";
				item[2208+i][2] = item_tinShortsword;
				item[2208+i][4] = i + 1;
			
				item[2214+i][0] = "Gold Shortsword";
				item[2214+i][1] = "";
				item[2214+i][2] = item_goldShortsword;
				item[2214+i][4] = 1 + i;
			
				item[2220+i][0] = "Silver Shortsword";
				item[2220+i][1] = "";
				item[2220+i][2] = item_silverShortsword;
				item[2220+i][4] = 1 + i;
			
				item[2226+i][0] = "Copper Shortsword";
				item[2226+i][1] = "";
				item[2226+i][2] = item_copperShortsword;
				item[2226+i][4] = 1 + i;
			
				item[2232+i][0] = "Brass Shortsword";
				item[2232+i][1] = "";
				item[2232+i][2] = item_brassShortsword;
				item[2232+i][4] = 1 + i;
			
				item[2238+i][0] = "Aluminum Shortsword";
				item[2238+i][1] = "";
				item[2238+i][2] = item_aluminumShortsword;
				item[2238+i][4] = 1 + i;
			
				item[2244+i][0] = "Iron Shortsword";
				item[2244+i][1] = "";
				item[2244+i][2] = item_ironShortsword;
				item[2244+i][4] = 1 + i;
			
				item[2250+i][0] = "Titanium Shortsword";
				item[2250+i][1] = "";
				item[2250+i][2] = item_titaniumShortsword;
				item[2250+i][4] = 1 + i;
			
				item[2256+i][0] = "Platinum Shortsword";
				item[2256+i][1] = "";
				item[2256+i][2] = item_platinumShortsword;
				item[2256+i][4] = 1 + i;
			
				item[2262+i][0] = "Tungsten Shortsword";
				item[2262+i][1] = "";
				item[2262+i][2] = item_tungstenShortsword;
				item[2262+i][4] = 1 + i;
			
			}
			
			item[2268][0] = "Soulscape Shortsword";
			item[2268][1] = "";
			item[2268][2] = item_soulscapeShortsword;
			item[2268][4] = 6;
			
		}
		
		{ //Cutlass
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[2269+i][0] = "Lead Cutlass";
				item[2269+i][1] = "";
				item[2269+i][2] = item_leadCutlass;
				item[2269+i][4] = i + 1;
			
				item[2275+i][0] = "Tin Cutlass";
				item[2275+i][1] = "";
				item[2275+i][2] = item_tinCutlass;
				item[2275+i][4] = i + 1;
			
				item[2281+i][0] = "Gold Cutlass";
				item[2281+i][1] = "";
				item[2281+i][2] = item_goldCutlass;
				item[2281+i][4] = 1 + i;
			
				item[2287+i][0] = "Silver Cutlass";
				item[2287+i][1] = "";
				item[2287+i][2] = item_silverCutlass;
				item[2287+i][4] = 1 + i;
			
				item[2293+i][0] = "Copper Cutlass";
				item[2293+i][1] = "";
				item[2293+i][2] = item_copperCutlass;
				item[2293+i][4] = 1 + i;
			
				item[2299+i][0] = "Brass Cutlass";
				item[2299+i][1] = "";
				item[2299+i][2] = item_brassCutlass;
				item[2299+i][4] = 1 + i;
			
				item[2305+i][0] = "Aluminum Cutlass";
				item[2305+i][1] = "";
				item[2305+i][2] = item_aluminumCutlass;
				item[2305+i][4] = 1 + i;
			
				item[2311+i][0] = "Iron Cutlass";
				item[2311+i][1] = "";
				item[2311+i][2] = item_ironCutlass;
				item[2311+i][4] = 1 + i;
			
				item[2317+i][0] = "Titanium Cutlass";
				item[2317+i][1] = "";
				item[2317+i][2] = item_titaniumCutlass;
				item[2317+i][4] = 1 + i;
			
				item[2323+i][0] = "Platinum Cutlass";
				item[2323+i][1] = "";
				item[2323+i][2] = item_platinumCutlass;
				item[2323+i][4] = 1 + i;
			
				item[2329+i][0] = "Tungsten Cutlass";
				item[2329+i][1] = "";
				item[2329+i][2] = item_tungstenCutlass;
				item[2329+i][4] = 1 + i;
			
			}
			
			item[2335][0] = "Soulscape Cutlass";
			item[2335][1] = "";
			item[2335][2] = item_soulscapeCutlass;
			item[2335][4] = 6;
			
		}
	
		{ //Saber
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[2336+i][0] = "Lead Saber";
				item[2336+i][1] = "";
				item[2336+i][2] = item_leadSaber;
				item[2336+i][4] = i + 1;
			
				item[2342+i][0] = "Tin Saber";
				item[2342+i][1] = "";
				item[2342+i][2] = item_tinSaber;
				item[2342+i][4] = i + 1;
			
				item[2348+i][0] = "Gold Saber";
				item[2348+i][1] = "";
				item[2348+i][2] = item_goldSaber;
				item[2348+i][4] = 1 + i;
			
				item[2354+i][0] = "Silver Saber";
				item[2354+i][1] = "";
				item[2354+i][2] = item_silverSaber;
				item[2354+i][4] = 1 + i;
			
				item[2360+i][0] = "Copper Saber";
				item[2360+i][1] = "";
				item[2360+i][2] = item_copperSaber;
				item[2360+i][4] = 1 + i;
			
				item[2366+i][0] = "Brass Saber";
				item[2366+i][1] = "";
				item[2366+i][2] = item_brassSaber;
				item[2366+i][4] = 1 + i;
			
				item[2372+i][0] = "Aluminum Saber";
				item[2372+i][1] = "";
				item[2372+i][2] = item_aluminumSaber;
				item[2372+i][4] = 1 + i;
			
				item[2378+i][0] = "Iron Saber";
				item[2378+i][1] = "";
				item[2378+i][2] = item_ironSaber;
				item[2378+i][4] = 1 + i;
			
				item[2384+i][0] = "Titanium Saber";
				item[2384+i][1] = "";
				item[2384+i][2] = item_titaniumSaber;
				item[2384+i][4] = 1 + i;
			
				item[2390+i][0] = "Platinum Saber";
				item[2390+i][1] = "";
				item[2390+i][2] = item_platinumSaber;
				item[2390+i][4] = 1 + i;
			
				item[2396+i][0] = "Tungsten Saber";
				item[2396+i][1] = "";
				item[2396+i][2] = item_tungstenSaber;
				item[2396+i][4] = 1 + i;
			
			}
			
			item[2402][0] = "Soulscape Saber";
			item[2402][1] = "";
			item[2402][2] = item_soulscapeSaber;
			item[2402][4] = 6;
			
		}
	
		{ //Longsword
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[2403+i][0] = "Lead Longsword";
				item[2403+i][1] = "";
				item[2403+i][2] = item_leadLongsword;
				item[2403+i][4] = i + 1;
			
				item[2409+i][0] = "Tin Longsword";
				item[2409+i][1] = "";
				item[2409+i][2] = item_tinLongsword;
				item[2409+i][4] = i + 1;
			
				item[2415+i][0] = "Gold Longsword";
				item[2415+i][1] = "";
				item[2415+i][2] = item_goldLongsword;
				item[2415+i][4] = 1 + i;
			
				item[2421+i][0] = "Silver Longsword";
				item[2421+i][1] = "";
				item[2421+i][2] = item_silverLongsword;
				item[2421+i][4] = 1 + i;
			
				item[2427+i][0] = "Copper Longsword";
				item[2427+i][1] = "";
				item[2427+i][2] = item_copperLongsword;
				item[2427+i][4] = 1 + i;
			
				item[2433+i][0] = "Brass Longsword";
				item[2433+i][1] = "";
				item[2433+i][2] = item_brassLongsword;
				item[2433+i][4] = 1 + i;
			
				item[2439+i][0] = "Aluminum Longsword";
				item[2439+i][1] = "";
				item[2439+i][2] = item_aluminumLongsword;
				item[2439+i][4] = 1 + i;
			
				item[2445+i][0] = "Iron Longsword";
				item[2445+i][1] = "";
				item[2445+i][2] = item_ironLongsword;
				item[2445+i][4] = 1 + i;
			
				item[2451+i][0] = "Titanium Longsword";
				item[2451+i][1] = "";
				item[2451+i][2] = item_titaniumLongsword;
				item[2451+i][4] = 1 + i;
			
				item[2457+i][0] = "Platinum Longsword";
				item[2457+i][1] = "";
				item[2457+i][2] = item_platinumLongsword;
				item[2457+i][4] = 1 + i;
			
				item[2463+i][0] = "Tungsten Longsword";
				item[2463+i][1] = "";
				item[2463+i][2] = item_tungstenLongsword;
				item[2463+i][4] = 1 + i;
			
			}
			
			item[2469][0] = "Soulscape Longsword";
			item[2469][1] = "";
			item[2469][2] = item_soulscapeLongsword;
			item[2469][4] = 6;
			
		}
		
		{ //Rapier
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[2470+i][0] = "Lead Rapier";
				item[2470+i][1] = "";
				item[2470+i][2] = item_leadRapier;
				item[2470+i][4] = i + 1;
			
				item[2476+i][0] = "Tin Rapier";
				item[2476+i][1] = "";
				item[2476+i][2] = item_tinRapier;
				item[2476+i][4] = i + 1;
			
				item[2482+i][0] = "Gold Rapier";
				item[2482+i][1] = "";
				item[2482+i][2] = item_goldRapier;
				item[2482+i][4] = 1 + i;
			
				item[2488+i][0] = "Silver Rapier";
				item[2488+i][1] = "";
				item[2488+i][2] = item_silverRapier;
				item[2488+i][4] = 1 + i;
			
				item[2494+i][0] = "Copper Rapier";
				item[2494+i][1] = "";
				item[2494+i][2] = item_copperRapier;
				item[2494+i][4] = 1 + i;
			
				item[2500+i][0] = "Brass Rapier";
				item[2500+i][1] = "";
				item[2500+i][2] = item_brassRapier;
				item[2500+i][4] = 1 + i;
			
				item[2506+i][0] = "Aluminum Rapier";
				item[2506+i][1] = "";
				item[2506+i][2] = item_aluminumRapier;
				item[2506+i][4] = 1 + i;
			
				item[2512+i][0] = "Iron Rapier";
				item[2512+i][1] = "";
				item[2512+i][2] = item_ironRapier;
				item[2512+i][4] = 1 + i;
			
				item[2518+i][0] = "Titanium Rapier";
				item[2518+i][1] = "";
				item[2518+i][2] = item_titaniumRapier;
				item[2518+i][4] = 1 + i;
			
				item[2524+i][0] = "Platinum Rapier";
				item[2524+i][1] = "";
				item[2524+i][2] = item_platinumRapier;
				item[2524+i][4] = 1 + i;
			
				item[2530+i][0] = "Tungsten Rapier";
				item[2530+i][1] = "";
				item[2530+i][2] = item_tungstenRapier;
				item[2530+i][4] = 1 + i;
			
			}
			
			item[2536][0] = "Soulscape Rapier";
			item[2536][1] = "";
			item[2536][2] = item_soulscapeRapier;
			item[2536][4] = 6;
			
		}
		
		{ //Backsword
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[2537+i][0] = "Lead Backsword";
				item[2537+i][1] = "";
				item[2537+i][2] = item_leadBacksword;
				item[2537+i][4] = i + 1;
			
				item[2543+i][0] = "Tin Backsword";
				item[2543+i][1] = "";
				item[2543+i][2] = item_tinBacksword;
				item[2543+i][4] = i + 1;
			
				item[2549+i][0] = "Gold Backsword";
				item[2549+i][1] = "";
				item[2549+i][2] = item_goldBacksword;
				item[2549+i][4] = 1 + i;
			
				item[2555+i][0] = "Silver Backsword";
				item[2555+i][1] = "";
				item[2555+i][2] = item_silverBacksword;
				item[2555+i][4] = 1 + i;
			
				item[2561+i][0] = "Copper Backsword";
				item[2561+i][1] = "";
				item[2561+i][2] = item_copperBacksword;
				item[2561+i][4] = 1 + i;
			
				item[2567+i][0] = "Brass Backsword";
				item[2567+i][1] = "";
				item[2567+i][2] = item_brassBacksword;
				item[2567+i][4] = 1 + i;
			
				item[2573+i][0] = "Aluminum Backsword";
				item[2573+i][1] = "";
				item[2573+i][2] = item_aluminumBacksword;
				item[2573+i][4] = 1 + i;
			
				item[2579+i][0] = "Iron Backsword";
				item[2579+i][1] = "";
				item[2579+i][2] = item_ironBacksword;
				item[2579+i][4] = 1 + i;
			
				item[2585+i][0] = "Titanium Backsword";
				item[2585+i][1] = "";
				item[2585+i][2] = item_titaniumBacksword;
				item[2585+i][4] = 1 + i;
			
				item[2591+i][0] = "Platinum Backsword";
				item[2591+i][1] = "";
				item[2591+i][2] = item_platinumBacksword;
				item[2591+i][4] = 1 + i;
			
				item[2597+i][0] = "Tungsten Backsword";
				item[2597+i][1] = "";
				item[2597+i][2] = item_tungstenBacksword;
				item[2597+i][4] = 1 + i;
			
			}
			
			item[2603][0] = "Soulscape Backsword";
			item[2603][1] = "";
			item[2603][2] = item_soulscapeBacksword;
			item[2603][4] = 6;
			
		}
		
		{ //Gladius
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[2604+i][0] = "Lead Gladius";
				item[2604+i][1] = "";
				item[2604+i][2] = item_leadGladius;
				item[2604+i][4] = i + 1;
			
				item[2610+i][0] = "Tin Gladius";
				item[2610+i][1] = "";
				item[2610+i][2] = item_tinGladius;
				item[2610+i][4] = i + 1;
			
				item[2616+i][0] = "Gold Gladius";
				item[2616+i][1] = "";
				item[2616+i][2] = item_goldGladius;
				item[2616+i][4] = 1 + i;
			
				item[2622+i][0] = "Silver Gladius";
				item[2622+i][1] = "";
				item[2622+i][2] = item_silverGladius;
				item[2622+i][4] = 1 + i;
			
				item[2628+i][0] = "Copper Gladius";
				item[2628+i][1] = "";
				item[2628+i][2] = item_copperGladius;
				item[2628+i][4] = 1 + i;
			
				item[2634+i][0] = "Brass Gladius";
				item[2634+i][1] = "";
				item[2634+i][2] = item_brassGladius;
				item[2634+i][4] = 1 + i;
			
				item[2640+i][0] = "Aluminum Gladius";
				item[2640+i][1] = "";
				item[2640+i][2] = item_aluminumGladius;
				item[2640+i][4] = 1 + i;
			
				item[2646+i][0] = "Iron Gladius";
				item[2646+i][1] = "";
				item[2646+i][2] = item_ironGladius;
				item[2646+i][4] = 1 + i;
			
				item[2652+i][0] = "Titanium Gladius";
				item[2652+i][1] = "";
				item[2652+i][2] = item_titaniumGladius;
				item[2652+i][4] = 1 + i;
			
				item[2658+i][0] = "Platinum Gladius";
				item[2658+i][1] = "";
				item[2658+i][2] = item_platinumGladius;
				item[2658+i][4] = 1 + i;
			
				item[2664+i][0] = "Tungsten Gladius";
				item[2664+i][1] = "";
				item[2664+i][2] = item_tungstenGladius;
				item[2664+i][4] = 1 + i;
			
			}
			
			item[2670][0] = "Soulscape Gladius";
			item[2670][1] = "";
			item[2670][2] = item_soulscapeGladius;
			item[2670][4] = 6;
			
		}
		
		{ //Katana
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[2671+i][0] = "Lead Katana";
				item[2671+i][1] = "";
				item[2671+i][2] = item_leadKatana;
				item[2671+i][4] = i + 1;
			
				item[2677+i][0] = "Tin Katana";
				item[2677+i][1] = "";
				item[2677+i][2] = item_tinKatana;
				item[2677+i][4] = i + 1;
			
				item[2683+i][0] = "Gold Katana";
				item[2683+i][1] = "";
				item[2683+i][2] = item_goldKatana;
				item[2683+i][4] = 1 + i;
			
				item[2689+i][0] = "Silver Katana";
				item[2689+i][1] = "";
				item[2689+i][2] = item_silverKatana;
				item[2689+i][4] = 1 + i;
			
				item[2695+i][0] = "Copper Katana";
				item[2695+i][1] = "";
				item[2695+i][2] = item_copperKatana;
				item[2695+i][4] = 1 + i;
			
				item[2701+i][0] = "Brass Katana";
				item[2701+i][1] = "";
				item[2701+i][2] = item_brassKatana;
				item[2701+i][4] = 1 + i;
			
				item[2707+i][0] = "Aluminum Katana";
				item[2707+i][1] = "";
				item[2707+i][2] = item_aluminumKatana;
				item[2707+i][4] = 1 + i;
			
				item[2713+i][0] = "Iron Katana";
				item[2713+i][1] = "";
				item[2713+i][2] = item_ironKatana;
				item[2713+i][4] = 1 + i;
			
				item[2719+i][0] = "Titanium Katana";
				item[2719+i][1] = "";
				item[2719+i][2] = item_titaniumKatana;
				item[2719+i][4] = 1 + i;
			
				item[2725+i][0] = "Platinum Katana";
				item[2725+i][1] = "";
				item[2725+i][2] = item_platinumKatana;
				item[2725+i][4] = 1 + i;
			
				item[2731+i][0] = "Tungsten Katana";
				item[2731+i][1] = "";
				item[2731+i][2] = item_tungstenKatana;
				item[2731+i][4] = 1 + i;
			
			}
			
			item[2737][0] = "Soulscape Katana";
			item[2737][1] = "";
			item[2737][2] = item_soulscapeKatana;
			item[2737][4] = 6;
			
		}
		
		{ //Dirk
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[2738+i][0] = "Lead Dirk";
				item[2738+i][1] = "";
				item[2738+i][2] = item_leadDirk;
				item[2738+i][4] = i + 1;
			
				item[2744+i][0] = "Tin Dirk";
				item[2744+i][1] = "";
				item[2744+i][2] = item_tinDirk;
				item[2744+i][4] = i + 1;
			
				item[2750+i][0] = "Gold Dirk";
				item[2750+i][1] = "";
				item[2750+i][2] = item_goldDirk;
				item[2750+i][4] = 1 + i;
			
				item[2756+i][0] = "Silver Dirk";
				item[2756+i][1] = "";
				item[2756+i][2] = item_silverDirk;
				item[2756+i][4] = 1 + i;
			
				item[2762+i][0] = "Copper Dirk";
				item[2762+i][1] = "";
				item[2762+i][2] = item_copperDirk;
				item[2762+i][4] = 1 + i;
			
				item[2768+i][0] = "Brass Dirk";
				item[2768+i][1] = "";
				item[2768+i][2] = item_brassDirk;
				item[2768+i][4] = 1 + i;
			
				item[2774+i][0] = "Aluminum Dirk";
				item[2774+i][1] = "";
				item[2774+i][2] = item_aluminumDirk;
				item[2774+i][4] = 1 + i;
			
				item[2780+i][0] = "Iron Dirk";
				item[2780+i][1] = "";
				item[2780+i][2] = item_ironDirk;
				item[2780+i][4] = 1 + i;
			
				item[2786+i][0] = "Titanium Dirk";
				item[2786+i][1] = "";
				item[2786+i][2] = item_titaniumDirk;
				item[2786+i][4] = 1 + i;
			
				item[2792+i][0] = "Platinum Dirk";
				item[2792+i][1] = "";
				item[2792+i][2] = item_platinumDirk;
				item[2792+i][4] = 1 + i;
			
				item[2798+i][0] = "Tungsten Dirk";
				item[2798+i][1] = "";
				item[2798+i][2] = item_tungstenDirk;
				item[2798+i][4] = 1 + i;
			
			}
			
			item[2804][0] = "Soulscape Dirk";
			item[2804][1] = "";
			item[2804][2] = item_soulscapeDirk;
			item[2804][4] = 6;
			
		}
		
		{ //Broadsword
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[2805+i][0] = "Lead Broadsword";
				item[2805+i][1] = "";
				item[2805+i][2] = item_leadBroadsword;
				item[2805+i][4] = i + 1;
			
				item[2811+i][0] = "Tin Broadsword";
				item[2811+i][1] = "";
				item[2811+i][2] = item_tinBroadsword;
				item[2811+i][4] = i + 1;
			
				item[2817+i][0] = "Gold Broadsword";
				item[2817+i][1] = "";
				item[2817+i][2] = item_goldBroadsword;
				item[2817+i][4] = 1 + i;
			
				item[2823+i][0] = "Silver Broadsword";
				item[2823+i][1] = "";
				item[2823+i][2] = item_silverBroadsword;
				item[2823+i][4] = 1 + i;
			
				item[2829+i][0] = "Copper Broadsword";
				item[2829+i][1] = "";
				item[2829+i][2] = item_copperBroadsword;
				item[2829+i][4] = 1 + i;
			
				item[2835+i][0] = "Brass Broadsword";
				item[2835+i][1] = "";
				item[2835+i][2] = item_brassBroadsword;
				item[2835+i][4] = 1 + i;
			
				item[2841+i][0] = "Aluminum Broadsword";
				item[2841+i][1] = "";
				item[2841+i][2] = item_aluminumBroadsword;
				item[2841+i][4] = 1 + i;
			
				item[2847+i][0] = "Iron Broadsword";
				item[2847+i][1] = "";
				item[2847+i][2] = item_ironBroadsword;
				item[2847+i][4] = 1 + i;
			
				item[2853+i][0] = "Titanium Broadsword";
				item[2853+i][1] = "";
				item[2853+i][2] = item_titaniumBroadsword;
				item[2853+i][4] = 1 + i;
			
				item[2859+i][0] = "Platinum Broadsword";
				item[2859+i][1] = "";
				item[2859+i][2] = item_platinumBroadsword;
				item[2859+i][4] = 1 + i;
			
				item[2865+i][0] = "Tungsten Broadsword";
				item[2865+i][1] = "";
				item[2865+i][2] = item_tungstenBroadsword;
				item[2865+i][4] = 1 + i;
			
			}
			
			item[2871][0] = "Soulscape Broadsword";
			item[2871][1] = "";
			item[2871][2] = item_soulscapeBroadsword;
			item[2871][4] = 6;
			
		}
		
		{ //Claymore
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[2872+i][0] = "Lead Claymore";
				item[2872+i][1] = "";
				item[2872+i][2] = item_leadClaymore;
				item[2872+i][4] = i + 1;
			
				item[2878+i][0] = "Tin Claymore";
				item[2878+i][1] = "";
				item[2878+i][2] = item_tinClaymore;
				item[2878+i][4] = i + 1;
			
				item[2884+i][0] = "Gold Claymore";
				item[2884+i][1] = "";
				item[2884+i][2] = item_goldClaymore;
				item[2884+i][4] = 1 + i;
			
				item[2890+i][0] = "Silver Claymore";
				item[2890+i][1] = "";
				item[2890+i][2] = item_silverClaymore;
				item[2890+i][4] = 1 + i;
			
				item[2896+i][0] = "Copper Claymore";
				item[2896+i][1] = "";
				item[2896+i][2] = item_copperClaymore;
				item[2896+i][4] = 1 + i;
			
				item[2902+i][0] = "Brass Claymore";
				item[2902+i][1] = "";
				item[2902+i][2] = item_brassClaymore;
				item[2902+i][4] = 1 + i;
			
				item[2908+i][0] = "Aluminum Claymore";
				item[2908+i][1] = "";
				item[2908+i][2] = item_aluminumClaymore;
				item[2908+i][4] = 1 + i;
			
				item[2914+i][0] = "Iron Claymore";
				item[2914+i][1] = "";
				item[2914+i][2] = item_ironClaymore;
				item[2914+i][4] = 1 + i;
			
				item[2920+i][0] = "Titanium Claymore";
				item[2920+i][1] = "";
				item[2920+i][2] = item_titaniumClaymore;
				item[2920+i][4] = 1 + i;
			
				item[2926+i][0] = "Platinum Claymore";
				item[2926+i][1] = "";
				item[2926+i][2] = item_platinumClaymore;
				item[2926+i][4] = 1 + i;
			
				item[2932+i][0] = "Tungsten Claymore";
				item[2932+i][1] = "";
				item[2932+i][2] = item_tungstenClaymore;
				item[2932+i][4] = 1 + i;
			
			}
			
			item[2938][0] = "Soulscape Claymore";
			item[2938][1] = "";
			item[2938][2] = item_soulscapeClaymore;
			item[2938][4] = 6;
			
		}
		
	}
	
	{ //Woodworking
		
		{ //Arrow
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[3001+i][0] = "Pine Arrow";
				item[3001+i][1] = "";
				item[3001+i][2] = item_pineArrow;
				item[3001+i][4] = i + 1;
			
				item[3007+i][0] = "Black Birch Arrow";
				item[3007+i][1] = "";
				item[3007+i][2] = item_blackBirchArrow;
				item[3007+i][4] = i + 1;
			
				item[3013+i][0] = "Oak Arrow";
				item[3013+i][1] = "";
				item[3013+i][2] = item_oakArrow;
				item[3013+i][4] = 1 + i;
			
				item[3019+i][0] = "Silver Maple Arrow";
				item[3019+i][1] = "";
				item[3019+i][2] = item_silverMapleArrow;
				item[3019+i][4] = 1 + i;
			
				item[3025+i][0] = "Dogwood Arrow";
				item[3025+i][1] = "";
				item[3025+i][2] = item_dogwoodArrow;
				item[3025+i][4] = 1 + i;
			
				item[3031+i][0] = "Maple Arrow";
				item[3031+i][1] = "";
				item[3031+i][2] = item_mapleArrow;
				item[3031+i][4] = 1 + i;
			
				item[3037+i][0] = "Ash Cedar Arrow";
				item[3037+i][1] = "";
				item[3037+i][2] = item_ashCedarArrow;
				item[3037+i][4] = 1 + i;
			
				item[3043+i][0] = "Chestnut Arrow";
				item[3043+i][1] = "";
				item[3043+i][2] = item_chestnutArrow;
				item[3043+i][4] = 1 + i;
			
				item[3049+i][0] = "Willow Arrow";
				item[3049+i][1] = "";
				item[3049+i][2] = item_willowArrow;
				item[3049+i][4] = 1 + i;
			
				item[3055+i][0] = "Palm Arrow";
				item[3055+i][1] = "";
				item[3055+i][2] = item_palmArrow;
				item[3055+i][4] = 1 + i;
			
				item[3061+i][0] = "Spruce Arrow";
				item[3061+i][1] = "";
				item[3061+i][2] = item_spruceArrow;
				item[3061+i][4] = 1 + i;
			
			}
			
			item[3067][0] = "Magic Burger Arrow";
			item[3067][1] = "";
			item[3067][2] = item_magicBurgerArrow;
			item[3067][4] = 6;
			
		}
		
		{ //Chair
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[3068+i][0] = "Pine Chair";
				item[3068+i][1] = "";
				item[3068+i][2] = item_pineChair;
				item[3068+i][4] = i + 1;
			
				item[3074+i][0] = "Black Birch Chair";
				item[3074+i][1] = "";
				item[3074+i][2] = item_blackBirchChair;
				item[3074+i][4] = i + 1;
			
				item[3080+i][0] = "Oak Chair";
				item[3080+i][1] = "";
				item[3080+i][2] = item_oakChair;
				item[3080+i][4] = 1 + i;
			
				item[3086+i][0] = "Silver Maple Chair";
				item[3086+i][1] = "";
				item[3086+i][2] = item_silverMapleChair;
				item[3086+i][4] = 1 + i;
			
				item[3092+i][0] = "Dogwood Chair";
				item[3092+i][1] = "";
				item[3092+i][2] = item_dogwoodChair;
				item[3092+i][4] = 1 + i;
			
				item[3098+i][0] = "Maple Chair";
				item[3098+i][1] = "";
				item[3098+i][2] = item_mapleChair;
				item[3098+i][4] = 1 + i;
			
				item[3104+i][0] = "Ash Cedar Chair";
				item[3104+i][1] = "";
				item[3104+i][2] = item_ashCedarChair;
				item[3104+i][4] = 1 + i;
			
				item[3110+i][0] = "Chestnut Chair";
				item[3110+i][1] = "";
				item[3110+i][2] = item_chestnutChair;
				item[3110+i][4] = 1 + i;
			
				item[3116+i][0] = "Willow Chair";
				item[3116+i][1] = "";
				item[3116+i][2] = item_willowChair;
				item[3116+i][4] = 1 + i;
			
				item[3122+i][0] = "Palm Chair";
				item[3122+i][1] = "";
				item[3122+i][2] = item_palmChair;
				item[3122+i][4] = 1 + i;
			
				item[3128+i][0] = "Spruce Chair";
				item[3128+i][1] = "";
				item[3128+i][2] = item_spruceChair;
				item[3128+i][4] = 1 + i;
			
			}
			
			item[3134][0] = "Magic Burger Chair";
			item[3134][1] = "";
			item[3134][2] = item_magicBurgerChair;
			item[3134][4] = 6;
			
		}
		
		{ //Short Bow
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[3135+i][0] = "Pine Short Bow";
				item[3135+i][1] = "";
				item[3135+i][2] = item_pineShortBow;
				item[3135+i][4] = i + 1;
			
				item[3141+i][0] = "Black Birch Short Bow";
				item[3141+i][1] = "";
				item[3141+i][2] = item_blackBirchShortBow;
				item[3141+i][4] = i + 1;
			
				item[3147+i][0] = "Oak Short Bow";
				item[3147+i][1] = "";
				item[3147+i][2] = item_oakShortBow;
				item[3147+i][4] = 1 + i;
			
				item[3153+i][0] = "Silver Maple Short Bow";
				item[3153+i][1] = "";
				item[3153+i][2] = item_silverMapleShortBow;
				item[3153+i][4] = 1 + i;
			
				item[3159+i][0] = "Dogwood Short Bow";
				item[3159+i][1] = "";
				item[3159+i][2] = item_dogwoodShortBow;
				item[3159+i][4] = 1 + i;
			
				item[3165+i][0] = "Maple Short Bow";
				item[3165+i][1] = "";
				item[3165+i][2] = item_mapleShortBow;
				item[3165+i][4] = 1 + i;
			
				item[3171+i][0] = "Ash Cedar Short Bow";
				item[3171+i][1] = "";
				item[3171+i][2] = item_ashCedarShortBow;
				item[3171+i][4] = 1 + i;
			
				item[3177+i][0] = "Chestnut Short Bow";
				item[3177+i][1] = "";
				item[3177+i][2] = item_chestnutShortBow;
				item[3177+i][4] = 1 + i;
			
				item[3183+i][0] = "Willow Short Bow";
				item[3183+i][1] = "";
				item[3183+i][2] = item_willowShortBow;
				item[3183+i][4] = 1 + i;
			
				item[3189+i][0] = "Palm Short Bow";
				item[3189+i][1] = "";
				item[3189+i][2] = item_palmShortBow;
				item[3189+i][4] = 1 + i;
			
				item[3195+i][0] = "Spruce Short Bow";
				item[3195+i][1] = "";
				item[3195+i][2] = item_spruceShortBow;
				item[3195+i][4] = 1 + i;
			
			}
			
			item[3201][0] = "Magic Burger Short Bow";
			item[3201][1] = "";
			item[3201][2] = item_magicBurgerShortBow;
			item[3201][4] = 6;
			
		}
		
		{ //Table
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[3202+i][0] = "Pine Table";
				item[3202+i][1] = "";
				item[3202+i][2] = item_pineTable;
				item[3202+i][4] = i + 1;
			
				item[3208+i][0] = "Black Birch Table";
				item[3208+i][1] = "";
				item[3208+i][2] = item_blackBirchTable;
				item[3208+i][4] = i + 1;
			
				item[3214+i][0] = "Oak Table";
				item[3214+i][1] = "";
				item[3214+i][2] = item_oakTable;
				item[3214+i][4] = 1 + i;
			
				item[3220+i][0] = "Silver Maple Table";
				item[3220+i][1] = "";
				item[3220+i][2] = item_silverMapleTable;
				item[3220+i][4] = 1 + i;
			
				item[3226+i][0] = "Dogwood Table";
				item[3226+i][1] = "";
				item[3226+i][2] = item_dogwoodTable;
				item[3226+i][4] = 1 + i;
			
				item[3232+i][0] = "Maple Table";
				item[3232+i][1] = "";
				item[3232+i][2] = item_mapleTable;
				item[3232+i][4] = 1 + i;
			
				item[3238+i][0] = "Ash Cedar Table";
				item[3238+i][1] = "";
				item[3238+i][2] = item_ashCedarTable;
				item[3238+i][4] = 1 + i;
			
				item[3244+i][0] = "Chestnut Table";
				item[3244+i][1] = "";
				item[3244+i][2] = item_chestnutTable;
				item[3244+i][4] = 1 + i;
			
				item[3250+i][0] = "Willow Table";
				item[3250+i][1] = "";
				item[3250+i][2] = item_willowTable;
				item[3250+i][4] = 1 + i;
			
				item[3256+i][0] = "Palm Table";
				item[3256+i][1] = "";
				item[3256+i][2] = item_palmTable;
				item[3256+i][4] = 1 + i;
			
				item[3262+i][0] = "Spruce Table";
				item[3262+i][1] = "";
				item[3262+i][2] = item_spruceTable;
				item[3262+i][4] = 1 + i;
			
			}
			
			item[3268][0] = "Magic Burger Table";
			item[3268][1] = "";
			item[3268][2] = item_magicBurgerTable;
			item[3268][4] = 6;
			
		}
		
		{ //Long Bow
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[3269+i][0] = "Pine Long Bow";
				item[3269+i][1] = "";
				item[3269+i][2] = item_pineLongBow;
				item[3269+i][4] = i + 1;
			
				item[3275+i][0] = "Black Birch Long Bow";
				item[3275+i][1] = "";
				item[3275+i][2] = item_blackBirchLongBow;
				item[3275+i][4] = i + 1;
			
				item[3281+i][0] = "Oak Long Bow";
				item[3281+i][1] = "";
				item[3281+i][2] = item_oakLongBow;
				item[3281+i][4] = 1 + i;
			
				item[3287+i][0] = "Silver Maple Long Bow";
				item[3287+i][1] = "";
				item[3287+i][2] = item_silverMapleLongBow;
				item[3287+i][4] = 1 + i;
			
				item[3293+i][0] = "Dogwood Long Bow";
				item[3293+i][1] = "";
				item[3293+i][2] = item_dogwoodLongBow;
				item[3293+i][4] = 1 + i;
			
				item[3299+i][0] = "Maple Long Bow";
				item[3299+i][1] = "";
				item[3299+i][2] = item_mapleLongBow;
				item[3299+i][4] = 1 + i;
			
				item[3305+i][0] = "Ash Cedar Long Bow";
				item[3305+i][1] = "";
				item[3305+i][2] = item_ashCedarLongBow;
				item[3305+i][4] = 1 + i;
			
				item[3311+i][0] = "Chestnut Long Bow";
				item[3311+i][1] = "";
				item[3311+i][2] = item_chestnutLongBow;
				item[3311+i][4] = 1 + i;
			
				item[3317+i][0] = "Willow Long Bow";
				item[3317+i][1] = "";
				item[3317+i][2] = item_willowLongBow;
				item[3317+i][4] = 1 + i;
			
				item[3323+i][0] = "Palm Long Bow";
				item[3323+i][1] = "";
				item[3323+i][2] = item_palmLongBow;
				item[3323+i][4] = 1 + i;
			
				item[3329+i][0] = "Spruce Long Bow";
				item[3329+i][1] = "";
				item[3329+i][2] = item_spruceLongBow;
				item[3329+i][4] = 1 + i;
			
			}
			
			item[3335][0] = "Magic Burger Long Bow";
			item[3335][1] = "";
			item[3335][2] = item_magicBurgerLongBow;
			item[3335][4] = 6;
			
		}
		
		{ //Bowl
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[3336+i][0] = "Pine Bowl";
				item[3336+i][1] = "";
				item[3336+i][2] = item_pineBowl;
				item[3336+i][4] = i + 1;
			
				item[3342+i][0] = "Black Birch Bowl";
				item[3342+i][1] = "";
				item[3342+i][2] = item_blackBirchBowl;
				item[3342+i][4] = i + 1;
			
				item[3348+i][0] = "Oak Bowl";
				item[3348+i][1] = "";
				item[3348+i][2] = item_oakBowl;
				item[3348+i][4] = 1 + i;
			
				item[3354+i][0] = "Silver Maple Bowl";
				item[3354+i][1] = "";
				item[3354+i][2] = item_silverMapleBowl;
				item[3354+i][4] = 1 + i;
			
				item[3360+i][0] = "Dogwood Bowl";
				item[3360+i][1] = "";
				item[3360+i][2] = item_dogwoodBowl;
				item[3360+i][4] = 1 + i;
			
				item[3366+i][0] = "Maple Bowl";
				item[3366+i][1] = "";
				item[3366+i][2] = item_mapleBowl;
				item[3366+i][4] = 1 + i;
			
				item[3372+i][0] = "Ash Cedar Bowl";
				item[3372+i][1] = "";
				item[3372+i][2] = item_ashCedarBowl;
				item[3372+i][4] = 1 + i;
			
				item[3378+i][0] = "Chestnut Bowl";
				item[3378+i][1] = "";
				item[3378+i][2] = item_chestnutBowl;
				item[3378+i][4] = 1 + i;
			
				item[3384+i][0] = "Willow Bowl";
				item[3384+i][1] = "";
				item[3384+i][2] = item_willowBowl;
				item[3384+i][4] = 1 + i;
			
				item[3390+i][0] = "Palm Bowl";
				item[3390+i][1] = "";
				item[3390+i][2] = item_palmBowl;
				item[3390+i][4] = 1 + i;
			
				item[3396+i][0] = "Spruce Bowl";
				item[3396+i][1] = "";
				item[3396+i][2] = item_spruceBowl;
				item[3396+i][4] = 1 + i;
			
			}
			
			item[3402][0] = "Magic Burger Bowl";
			item[3402][1] = "";
			item[3402][2] = item_magicBurgerBowl;
			item[3402][4] = 6;
			
		}
		
		{ //Chest
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[3403+i][0] = "Pine Chest";
				item[3403+i][1] = "";
				item[3403+i][2] = item_pineChest;
				item[3403+i][4] = i + 1;
			
				item[3409+i][0] = "Black Birch Chest";
				item[3409+i][1] = "";
				item[3409+i][2] = item_blackBirchChest;
				item[3409+i][4] = i + 1;
			
				item[3415+i][0] = "Oak Chest";
				item[3415+i][1] = "";
				item[3415+i][2] = item_oakChest;
				item[3415+i][4] = 1 + i;
			
				item[3421+i][0] = "Silver Maple Chest";
				item[3421+i][1] = "";
				item[3421+i][2] = item_silverMapleChest;
				item[3421+i][4] = 1 + i;
			
				item[3427+i][0] = "Dogwood Chest";
				item[3427+i][1] = "";
				item[3427+i][2] = item_dogwoodChest;
				item[3427+i][4] = 1 + i;
			
				item[3433+i][0] = "Maple Chest";
				item[3433+i][1] = "";
				item[3433+i][2] = item_mapleChest;
				item[3433+i][4] = 1 + i;
			
				item[3439+i][0] = "Ash Cedar Chest";
				item[3439+i][1] = "";
				item[3439+i][2] = item_ashCedarChest;
				item[3439+i][4] = 1 + i;
			
				item[3445+i][0] = "Chestnut Chest";
				item[3445+i][1] = "";
				item[3445+i][2] = item_chestnutChest;
				item[3445+i][4] = 1 + i;
			
				item[3451+i][0] = "Willow Chest";
				item[3451+i][1] = "";
				item[3451+i][2] = item_willowChest;
				item[3451+i][4] = 1 + i;
			
				item[3457+i][0] = "Palm Chest";
				item[3457+i][1] = "";
				item[3457+i][2] = item_palmChest;
				item[3457+i][4] = 1 + i;
			
				item[3463+i][0] = "Spruce Chest";
				item[3463+i][1] = "";
				item[3463+i][2] = item_spruceChest;
				item[3463+i][4] = 1 + i;
			
			}
			
			item[3469][0] = "Magic Burger Chest";
			item[3469][1] = "";
			item[3469][2] = item_magicBurgerChest;
			item[3469][4] = 6;
			
		}
		
		{ //Lute
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[3470+i][0] = "Pine Lute";
				item[3470+i][1] = "";
				item[3470+i][2] = item_pineLute;
				item[3470+i][4] = i + 1;
			
				item[3476+i][0] = "Black Birch Lute";
				item[3476+i][1] = "";
				item[3476+i][2] = item_blackBirchLute;
				item[3476+i][4] = i + 1;
			
				item[3482+i][0] = "Oak Lute";
				item[3482+i][1] = "";
				item[3482+i][2] = item_oakLute;
				item[3482+i][4] = 1 + i;
			
				item[3488+i][0] = "Silver Maple Lute";
				item[3488+i][1] = "";
				item[3488+i][2] = item_silverMapleLute;
				item[3488+i][4] = 1 + i;
			
				item[3494+i][0] = "Dogwood Lute";
				item[3494+i][1] = "";
				item[3494+i][2] = item_dogwoodLute;
				item[3494+i][4] = 1 + i;
			
				item[3500+i][0] = "Maple Lute";
				item[3500+i][1] = "";
				item[3500+i][2] = item_mapleLute;
				item[3500+i][4] = 1 + i;
			
				item[3506+i][0] = "Ash Cedar Lute";
				item[3506+i][1] = "";
				item[3506+i][2] = item_ashCedarLute;
				item[3506+i][4] = 1 + i;
			
				item[3512+i][0] = "Lutenut Lute";
				item[3512+i][1] = "";
				item[3512+i][2] = item_chestnutLute;
				item[3512+i][4] = 1 + i;
			
				item[3518+i][0] = "Willow Lute";
				item[3518+i][1] = "";
				item[3518+i][2] = item_willowLute;
				item[3518+i][4] = 1 + i;
			
				item[3524+i][0] = "Palm Lute";
				item[3524+i][1] = "";
				item[3524+i][2] = item_palmLute;
				item[3524+i][4] = 1 + i;
			
				item[3530+i][0] = "Spruce Lute";
				item[3530+i][1] = "";
				item[3530+i][2] = item_spruceLute;
				item[3530+i][4] = 1 + i;
			
			}
			
			item[3536][0] = "Magic Burger Lute";
			item[3536][1] = "";
			item[3536][2] = item_magicBurgerLute;
			item[3536][4] = 6;
			
		}
		
		{ //Bed
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[3537+i][0] = "Pine Bed";
				item[3537+i][1] = "";
				item[3537+i][2] = item_pineBed;
				item[3537+i][4] = i + 1;
			
				item[3543+i][0] = "Black Birch Bed";
				item[3543+i][1] = "";
				item[3543+i][2] = item_blackBirchBed;
				item[3543+i][4] = i + 1;
			
				item[3549+i][0] = "Oak Bed";
				item[3549+i][1] = "";
				item[3549+i][2] = item_oakBed;
				item[3549+i][4] = 1 + i;
			
				item[3555+i][0] = "Silver Maple Bed";
				item[3555+i][1] = "";
				item[3555+i][2] = item_silverMapleBed;
				item[3555+i][4] = 1 + i;
			
				item[3561+i][0] = "Dogwood Bed";
				item[3561+i][1] = "";
				item[3561+i][2] = item_dogwoodBed;
				item[3561+i][4] = 1 + i;
			
				item[3567+i][0] = "Maple Bed";
				item[3567+i][1] = "";
				item[3567+i][2] = item_mapleBed;
				item[3567+i][4] = 1 + i;
			
				item[3573+i][0] = "Ash Cedar Bed";
				item[3573+i][1] = "";
				item[3573+i][2] = item_ashCedarBed;
				item[3573+i][4] = 1 + i;
			
				item[3579+i][0] = "Chestnut Bed";
				item[3579+i][1] = "";
				item[3579+i][2] = item_chestnutBed;
				item[3579+i][4] = 1 + i;
			
				item[3585+i][0] = "Willow Bed";
				item[3585+i][1] = "";
				item[3585+i][2] = item_willowBed;
				item[3585+i][4] = 1 + i;
			
				item[3591+i][0] = "Palm Bed";
				item[3591+i][1] = "";
				item[3591+i][2] = item_palmBed;
				item[3591+i][4] = 1 + i;
			
				item[3597+i][0] = "Spruce Bed";
				item[3597+i][1] = "";
				item[3597+i][2] = item_spruceBed;
				item[3597+i][4] = 1 + i;
			
			}
			
			item[3603][0] = "Magic Burger Bed";
			item[3603][1] = "";
			item[3603][2] = item_magicBurgerBed;
			item[3603][4] = 6;
			
		}
		
		{ //Crossbow
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[3604+i][0] = "Pine Crossbow";
				item[3604+i][1] = "";
				item[3604+i][2] = item_pineCrossbow;
				item[3604+i][4] = i + 1;
			
				item[3610+i][0] = "Black Birch Crossbow";
				item[3610+i][1] = "";
				item[3610+i][2] = item_blackBirchCrossbow;
				item[3610+i][4] = i + 1;
			
				item[3616+i][0] = "Oak Crossbow";
				item[3616+i][1] = "";
				item[3616+i][2] = item_oakCrossbow;
				item[3616+i][4] = 1 + i;
			
				item[3622+i][0] = "Silver Maple Crossbow";
				item[3622+i][1] = "";
				item[3622+i][2] = item_silverMapleCrossbow;
				item[3622+i][4] = 1 + i;
			
				item[3628+i][0] = "Dogwood Crossbow";
				item[3628+i][1] = "";
				item[3628+i][2] = item_dogwoodCrossbow;
				item[3628+i][4] = 1 + i;
			
				item[3634+i][0] = "Maple Crossbow";
				item[3634+i][1] = "";
				item[3634+i][2] = item_mapleCrossbow;
				item[3634+i][4] = 1 + i;
			
				item[3640+i][0] = "Ash Cedar Crossbow";
				item[3640+i][1] = "";
				item[3640+i][2] = item_ashCedarCrossbow;
				item[3640+i][4] = 1 + i;
			
				item[3646+i][0] = "Chestnut Crossbow";
				item[3646+i][1] = "";
				item[3646+i][2] = item_chestnutCrossbow;
				item[3646+i][4] = 1 + i;
			
				item[3652+i][0] = "Willow Crossbow";
				item[3652+i][1] = "";
				item[3652+i][2] = item_willowCrossbow;
				item[3652+i][4] = 1 + i;
			
				item[3658+i][0] = "Palm Crossbow";
				item[3658+i][1] = "";
				item[3658+i][2] = item_palmCrossbow;
				item[3658+i][4] = 1 + i;
			
				item[3664+i][0] = "Spruce Crossbow";
				item[3664+i][1] = "";
				item[3664+i][2] = item_spruceCrossbow;
				item[3664+i][4] = 1 + i;
			
			}
			
			item[3670][0] = "Magic Burger Crossbow";
			item[3670][1] = "";
			item[3670][2] = item_magicBurgerCrossbow;
			item[3670][4] = 6;
			
		}
		
		{ //Barrel
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[3671+i][0] = "Pine Barrel";
				item[3671+i][1] = "";
				item[3671+i][2] = item_pineBarrel;
				item[3671+i][4] = i + 1;
			
				item[3677+i][0] = "Black Birch Barrel";
				item[3677+i][1] = "";
				item[3677+i][2] = item_blackBirchBarrel;
				item[3677+i][4] = i + 1;
			
				item[3683+i][0] = "Oak Barrel";
				item[3683+i][1] = "";
				item[3683+i][2] = item_oakBarrel;
				item[3683+i][4] = 1 + i;
			
				item[3689+i][0] = "Silver Maple Barrel";
				item[3689+i][1] = "";
				item[3689+i][2] = item_silverMapleBarrel;
				item[3689+i][4] = 1 + i;
			
				item[3695+i][0] = "Dogwood Barrel";
				item[3695+i][1] = "";
				item[3695+i][2] = item_dogwoodBarrel;
				item[3695+i][4] = 1 + i;
			
				item[3701+i][0] = "Maple Barrel";
				item[3701+i][1] = "";
				item[3701+i][2] = item_mapleBarrel;
				item[3701+i][4] = 1 + i;
			
				item[3707+i][0] = "Ash Cedar Barrel";
				item[3707+i][1] = "";
				item[3707+i][2] = item_ashCedarBarrel;
				item[3707+i][4] = 1 + i;
			
				item[3713+i][0] = "Chestnut Barrel";
				item[3713+i][1] = "";
				item[3713+i][2] = item_chestnutBarrel;
				item[3713+i][4] = 1 + i;
			
				item[3719+i][0] = "Willow Barrel";
				item[3719+i][1] = "";
				item[3719+i][2] = item_willowBarrel;
				item[3719+i][4] = 1 + i;
			
				item[3725+i][0] = "Palm Barrel";
				item[3725+i][1] = "";
				item[3725+i][2] = item_palmBarrel;
				item[3725+i][4] = 1 + i;
			
				item[3731+i][0] = "Spruce Barrel";
				item[3731+i][1] = "";
				item[3731+i][2] = item_spruceBarrel;
				item[3731+i][4] = 1 + i;
			
			}
			
			item[3737][0] = "Magic Burger Barrel";
			item[3737][1] = "";
			item[3737][2] = item_magicBurgerBarrel;
			item[3737][4] = 6;
			
		}
		
		{ //Staff
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[3738+i][0] = "Pine Staff";
				item[3738+i][1] = "";
				item[3738+i][2] = item_pineStaff;
				item[3738+i][4] = i + 1;
			
				item[3744+i][0] = "Black Birch Staff";
				item[3744+i][1] = "";
				item[3744+i][2] = item_blackBirchStaff;
				item[3744+i][4] = i + 1;
			
				item[3750+i][0] = "Oak Staff";
				item[3750+i][1] = "";
				item[3750+i][2] = item_oakStaff;
				item[3750+i][4] = 1 + i;
			
				item[3756+i][0] = "Silver Maple Staff";
				item[3756+i][1] = "";
				item[3756+i][2] = item_silverMapleStaff;
				item[3756+i][4] = 1 + i;
			
				item[3762+i][0] = "Dogwood Staff";
				item[3762+i][1] = "";
				item[3762+i][2] = item_dogwoodStaff;
				item[3762+i][4] = 1 + i;
			
				item[3768+i][0] = "Maple Staff";
				item[3768+i][1] = "";
				item[3768+i][2] = item_mapleStaff;
				item[3768+i][4] = 1 + i;
			
				item[3774+i][0] = "Ash Cedar Staff";
				item[3774+i][1] = "";
				item[3774+i][2] = item_ashCedarStaff;
				item[3774+i][4] = 1 + i;
			
				item[3780+i][0] = "Chestnut Staff";
				item[3780+i][1] = "";
				item[3780+i][2] = item_chestnutStaff;
				item[3780+i][4] = 1 + i;
			
				item[3786+i][0] = "Willow Staff";
				item[3786+i][1] = "";
				item[3786+i][2] = item_willowStaff;
				item[3786+i][4] = 1 + i;
			
				item[3792+i][0] = "Palm Staff";
				item[3792+i][1] = "";
				item[3792+i][2] = item_palmStaff;
				item[3792+i][4] = 1 + i;
			
				item[3798+i][0] = "Spruce Staff";
				item[3798+i][1] = "";
				item[3798+i][2] = item_spruceStaff;
				item[3798+i][4] = 1 + i;
			
			}
			
			item[3804][0] = "Magic Burger Staff";
			item[3804][1] = "";
			item[3804][2] = item_magicBurgerStaff;
			item[3804][4] = 6;
			
		}
		
		{ //Caravan Cart
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[3805+i][0] = "Pine Caravan Cart";
				item[3805+i][1] = "";
				item[3805+i][2] = item_pineCaravanCart;
				item[3805+i][4] = i + 1;
			
				item[3811+i][0] = "Black Birch Caravan Cart";
				item[3811+i][1] = "";
				item[3811+i][2] = item_blackBirchCaravanCart;
				item[3811+i][4] = i + 1;
			
				item[3817+i][0] = "Oak Caravan Cart";
				item[3817+i][1] = "";
				item[3817+i][2] = item_oakCaravanCart;
				item[3817+i][4] = 1 + i;
			
				item[3823+i][0] = "Silver Maple Caravan Cart";
				item[3823+i][1] = "";
				item[3823+i][2] = item_silverMapleCaravanCart;
				item[3823+i][4] = 1 + i;
			
				item[3829+i][0] = "Dogwood Caravan Cart";
				item[3829+i][1] = "";
				item[3829+i][2] = item_dogwoodCaravanCart;
				item[3829+i][4] = 1 + i;
			
				item[3835+i][0] = "Maple Caravan Cart";
				item[3835+i][1] = "";
				item[3835+i][2] = item_mapleCaravanCart;
				item[3835+i][4] = 1 + i;
			
				item[3841+i][0] = "Ash Cedar Caravan Cart";
				item[3841+i][1] = "";
				item[3841+i][2] = item_ashCedarCaravanCart;
				item[3841+i][4] = 1 + i;
			
				item[3847+i][0] = "Chestnut Caravan Cart";
				item[3847+i][1] = "";
				item[3847+i][2] = item_chestnutCaravanCart;
				item[3847+i][4] = 1 + i;
			
				item[3853+i][0] = "Willow Caravan Cart";
				item[3853+i][1] = "";
				item[3853+i][2] = item_willowCaravanCart;
				item[3853+i][4] = 1 + i;
			
				item[3859+i][0] = "Palm Caravan Cart";
				item[3859+i][1] = "";
				item[3859+i][2] = item_palmCaravanCart;
				item[3859+i][4] = 1 + i;
			
				item[3865+i][0] = "Spruce Caravan Cart";
				item[3865+i][1] = "";
				item[3865+i][2] = item_spruceCaravanCart;
				item[3865+i][4] = 1 + i;
			
			}
			
			item[3871][0] = "Magic Burger Caravan Cart";
			item[3871][1] = "";
			item[3871][2] = item_magicBurgerCaravanCart;
			item[3871][4] = 6;
			
		}
		
		{ //Catapult
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[3872+i][0] = "Pine Catapult";
				item[3872+i][1] = "";
				item[3872+i][2] = item_pineCatapult;
				item[3872+i][4] = i + 1;
			
				item[3878+i][0] = "Black Birch Catapult";
				item[3878+i][1] = "";
				item[3878+i][2] = item_blackBirchCatapult;
				item[3878+i][4] = i + 1;
			
				item[3884+i][0] = "Oak Catapult";
				item[3884+i][1] = "";
				item[3884+i][2] = item_oakCatapult;
				item[3884+i][4] = 1 + i;
			
				item[3890+i][0] = "Silver Maple Catapult";
				item[3890+i][1] = "";
				item[3890+i][2] = item_silverMapleCatapult;
				item[3890+i][4] = 1 + i;
			
				item[3896+i][0] = "Dogwood Catapult";
				item[3896+i][1] = "";
				item[3896+i][2] = item_dogwoodCatapult;
				item[3896+i][4] = 1 + i;
			
				item[3902+i][0] = "Maple Catapult";
				item[3902+i][1] = "";
				item[3902+i][2] = item_mapleCatapult;
				item[3902+i][4] = 1 + i;
			
				item[3908+i][0] = "Ash Cedar Catapult";
				item[3908+i][1] = "";
				item[3908+i][2] = item_ashCedarCatapult;
				item[3908+i][4] = 1 + i;
			
				item[3914+i][0] = "Chestnut Catapult";
				item[3914+i][1] = "";
				item[3914+i][2] = item_chestnutCatapult;
				item[3914+i][4] = 1 + i;
			
				item[3920+i][0] = "Willow Catapult";
				item[3920+i][1] = "";
				item[3920+i][2] = item_willowCatapult;
				item[3920+i][4] = 1 + i;
			
				item[3926+i][0] = "Palm Catapult";
				item[3926+i][1] = "";
				item[3926+i][2] = item_palmCatapult;
				item[3926+i][4] = 1 + i;
			
				item[3932+i][0] = "Spruce Catapult";
				item[3932+i][1] = "";
				item[3932+i][2] = item_spruceCatapult;
				item[3932+i][4] = 1 + i;
			
			}
			
			item[3938][0] = "Magic Burger Catapult";
			item[3938][1] = "";
			item[3938][2] = item_magicBurgerCatapult;
			item[3938][4] = 6;
			
		}
	}

	{ //Armor Crafting
		
		{ //Chain Mail Shirt
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[4001+i][0] = "Lead Chain Mail Shirt";
				item[4001+i][1] = "";
				item[4001+i][2] = item_leadChainMailShirt;
				item[4001+i][4] = i + 1;
			
				item[4007+i][0] = "Tin Chain Mail Shirt";
				item[4007+i][1] = "";
				item[4007+i][2] = item_tinChainMailShirt;
				item[4007+i][4] = i + 1;
			
				item[4013+i][0] = "Gold Chain Mail Shirt";
				item[4013+i][1] = "";
				item[4013+i][2] = item_goldChainMailShirt;
				item[4013+i][4] = 1 + i;
			
				item[4019+i][0] = "Silver Chain Mail Shirt";
				item[4019+i][1] = "";
				item[4019+i][2] = item_silverChainMailShirt;
				item[4019+i][4] = 1 + i;
			
				item[4025+i][0] = "Copper Chain Mail Shirt";
				item[4025+i][1] = "";
				item[4025+i][2] = item_copperChainMailShirt;
				item[4025+i][4] = 1 + i;
			
				item[4031+i][0] = "Brass Chain Mail Shirt";
				item[4031+i][1] = "";
				item[4031+i][2] = item_brassChainMailShirt;
				item[4031+i][4] = 1 + i;
			
				item[4037+i][0] = "Aluminum Chain Mail Shirt";
				item[4037+i][1] = "";
				item[4037+i][2] = item_aluminumChainMailShirt;
				item[4037+i][4] = 1 + i;
			
				item[4043+i][0] = "Iron Chain Mail Shirt";
				item[4043+i][1] = "";
				item[4043+i][2] = item_ironChainMailShirt;
				item[4043+i][4] = 1 + i;
			
				item[4049+i][0] = "Titanium Chain Mail Shirt";
				item[4049+i][1] = "";
				item[4049+i][2] = item_titaniumChainMailShirt;
				item[4049+i][4] = 1 + i;
		
				item[4055+i][0] = "Platinum Chain Mail Shirt";
				item[4055+i][1] = "";
				item[4055+i][2] = item_platinumChainMailShirt;
				item[4055+i][4] = 1 + i;
			
				item[4061+i][0] = "Tungsten Chain Mail Shirt";
				item[4061+i][1] = "";
				item[4061+i][2] = item_tungstenChainMailShirt;
				item[4061+i][4] = 1 + i;
			
			}
			
			item[4067][0] = "Soulscape Chain Mail Shirt";
			item[4067][1] = "";
			item[4067][2] = item_soulscapeChainMailShirt;
			item[4067][4] = 6;
			
		}
		
		{ //Chain Mail Pants
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[4068+i][0] = "Lead Chain Mail Pants";
				item[4068+i][1] = "";
				item[4068+i][2] = item_leadChainMailPants;
				item[4068+i][4] = i + 1;
			
				item[4074+i][0] = "Tin Chain Mail Pants";
				item[4074+i][1] = "";
				item[4074+i][2] = item_tinChainMailPants;
				item[4074+i][4] = i + 1;
			
				item[4080+i][0] = "Gold Chain Mail Pants";
				item[4080+i][1] = "";
				item[4080+i][2] = item_goldChainMailPants;
				item[4080+i][4] = 1 + i;
			
				item[4086+i][0] = "Silver Chain Mail Pants";
				item[4086+i][1] = "";
				item[4086+i][2] = item_silverChainMailPants;
				item[4086+i][4] = 1 + i;
			
				item[4092+i][0] = "Copper Chain Mail Pants";
				item[4092+i][1] = "";
				item[4092+i][2] = item_copperChainMailPants;
				item[4092+i][4] = 1 + i;
			
				item[4098+i][0] = "Brass Chain Mail Pants";
				item[4098+i][1] = "";
				item[4098+i][2] = item_brassChainMailPants;
				item[4098+i][4] = 1 + i;
			
				item[4104+i][0] = "Aluminum Chain Mail Pants";
				item[4104+i][1] = "";
				item[4104+i][2] = item_aluminumChainMailPants;
				item[4104+i][4] = 1 + i;
			
				item[4110+i][0] = "Iron Chain Mail Pants";
				item[4110+i][1] = "";
				item[4110+i][2] = item_ironChainMailPants;
				item[4110+i][4] = 1 + i;
			
				item[4116+i][0] = "Titanium Chain Mail Pants";
				item[4116+i][1] = "";
				item[4116+i][2] = item_titaniumChainMailPants;
				item[4116+i][4] = 1 + i;
		
				item[4122+i][0] = "Platinum Chain Mail Pants";
				item[4122+i][1] = "";
				item[4122+i][2] = item_platinumChainMailPants;
				item[4122+i][4] = 1 + i;
			
				item[4128+i][0] = "Tungsten Chain Mail Pants";
				item[4128+i][1] = "";
				item[4128+i][2] = item_tungstenChainMailPants;
				item[4128+i][4] = 1 + i;
			
			}
			
			item[4134][0] = "Soulscape Chain Mail Pants";
			item[4134][1] = "";
			item[4134][2] = item_soulscapeChainMailPants;
			item[4134][4] = 6;
			
		}
		
		{ //Chain Mail Hood
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[4135+i][0] = "Lead Chain Mail Hood";
				item[4135+i][1] = "";
				item[4135+i][2] = item_leadChainMailHood;
				item[4135+i][4] = i + 1;
			
				item[4141+i][0] = "Tin Chain Mail Hood";
				item[4141+i][1] = "";
				item[4141+i][2] = item_tinChainMailHood;
				item[4141+i][4] = i + 1;
			
				item[4147+i][0] = "Gold Chain Mail Hood";
				item[4147+i][1] = "";
				item[4147+i][2] = item_goldChainMailHood;
				item[4147+i][4] = 1 + i;
			
				item[4153+i][0] = "Silver Chain Mail Hood";
				item[4153+i][1] = "";
				item[4153+i][2] = item_silverChainMailHood;
				item[4153+i][4] = 1 + i;
			
				item[4159+i][0] = "Copper Chain Mail Hood";
				item[4159+i][1] = "";
				item[4159+i][2] = item_copperChainMailHood;
				item[4159+i][4] = 1 + i;
			
				item[4165+i][0] = "Brass Chain Mail Hood";
				item[4165+i][1] = "";
				item[4165+i][2] = item_brassChainMailHood;
				item[4165+i][4] = 1 + i;
			
				item[4171+i][0] = "Aluminum Chain Mail Hood";
				item[4171+i][1] = "";
				item[4171+i][2] = item_aluminumChainMailHood;
				item[4171+i][4] = 1 + i;
			
				item[4177+i][0] = "Iron Chain Mail Hood";
				item[4177+i][1] = "";
				item[4177+i][2] = item_ironChainMailHood;
				item[4177+i][4] = 1 + i;
			
				item[4183+i][0] = "Titanium Chain Mail Hood";
				item[4183+i][1] = "";
				item[4183+i][2] = item_titaniumChainMailHood;
				item[4183+i][4] = 1 + i;
		
				item[4189+i][0] = "Platinum Chain Mail Hood";
				item[4189+i][1] = "";
				item[4189+i][2] = item_platinumChainMailHood;
				item[4189+i][4] = 1 + i;
			
				item[4195+i][0] = "Tungsten Chain Mail Hood";
				item[4195+i][1] = "";
				item[4195+i][2] = item_tungstenChainMailHood;
				item[4195+i][4] = 1 + i;
			
			}
			
			item[4201][0] = "Soulscape Chain Mail Hood";
			item[4201][1] = "";
			item[4201][2] = item_soulscapeChainMailHood;
			item[4201][4] = 6;
			
		}
		
		{ //Chain Mail Gloves
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[4202+i][0] = "Lead Chain Mail Gloves";
				item[4202+i][1] = "";
				item[4202+i][2] = item_leadChainMailGloves;
				item[4202+i][4] = i + 1;
			
				item[4208+i][0] = "Tin Chain Mail Gloves";
				item[4208+i][1] = "";
				item[4208+i][2] = item_tinChainMailGloves;
				item[4208+i][4] = i + 1;
			
				item[4214+i][0] = "Gold Chain Mail Gloves";
				item[4214+i][1] = "";
				item[4214+i][2] = item_goldChainMailGloves;
				item[4214+i][4] = 1 + i;
			
				item[4220+i][0] = "Silver Chain Mail Gloves";
				item[4220+i][1] = "";
				item[4220+i][2] = item_silverChainMailGloves;
				item[4220+i][4] = 1 + i;
			
				item[4226+i][0] = "Copper Chain Mail Gloves";
				item[4226+i][1] = "";
				item[4226+i][2] = item_copperChainMailGloves;
				item[4226+i][4] = 1 + i;
			
				item[4232+i][0] = "Brass Chain Mail Gloves";
				item[4232+i][1] = "";
				item[4232+i][2] = item_brassChainMailGloves;
				item[4232+i][4] = 1 + i;
			
				item[4238+i][0] = "Aluminum Chain Mail Gloves";
				item[4238+i][1] = "";
				item[4238+i][2] = item_aluminumChainMailGloves;
				item[4238+i][4] = 1 + i;
			
				item[4244+i][0] = "Iron Chain Mail Gloves";
				item[4244+i][1] = "";
				item[4244+i][2] = item_ironChainMailGloves;
				item[4244+i][4] = 1 + i;
			
				item[4250+i][0] = "Titanium Chain Mail Gloves";
				item[4250+i][1] = "";
				item[4250+i][2] = item_titaniumChainMailGloves;
				item[4250+i][4] = 1 + i;
		
				item[4256+i][0] = "Platinum Chain Mail Gloves";
				item[4256+i][1] = "";
				item[4256+i][2] = item_platinumChainMailGloves;
				item[4256+i][4] = 1 + i;
			
				item[4262+i][0] = "Tungsten Chain Mail Gloves";
				item[4262+i][1] = "";
				item[4262+i][2] = item_tungstenChainMailGloves;
				item[4262+i][4] = 1 + i;
			
			}
			
			item[4268][0] = "Soulscape Chain Mail Gloves";
			item[4268][1] = "";
			item[4268][2] = item_soulscapeChainMailGloves;
			item[4268][4] = 6;
			
		}
		
		{ //Kettle Helmet
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[4269+i][0] = "Lead Kettle Helmet";
				item[4269+i][1] = "";
				item[4269+i][2] = item_leadKettleHelmet;
				item[4269+i][4] = i + 1;
			
				item[4275+i][0] = "Tin Kettle Helmet";
				item[4275+i][1] = "";
				item[4275+i][2] = item_tinKettleHelmet;
				item[4275+i][4] = i + 1;
			
				item[4281+i][0] = "Gold Kettle Helmet";
				item[4281+i][1] = "";
				item[4281+i][2] = item_goldKettleHelmet;
				item[4281+i][4] = 1 + i;
			
				item[4287+i][0] = "Silver Kettle Helmet";
				item[4287+i][1] = "";
				item[4287+i][2] = item_silverKettleHelmet;
				item[4287+i][4] = 1 + i;
			
				item[4293+i][0] = "Copper Kettle Helmet";
				item[4293+i][1] = "";
				item[4293+i][2] = item_copperKettleHelmet;
				item[4293+i][4] = 1 + i;
			
				item[4299+i][0] = "Brass Kettle Helmet";
				item[4299+i][1] = "";
				item[4299+i][2] = item_brassKettleHelmet;
				item[4299+i][4] = 1 + i;
			
				item[4305+i][0] = "Aluminum Kettle Helmet";
				item[4305+i][1] = "";
				item[4305+i][2] = item_aluminumKettleHelmet;
				item[4305+i][4] = 1 + i;
			
				item[4311+i][0] = "Iron Kettle Helmet";
				item[4311+i][1] = "";
				item[4311+i][2] = item_ironKettleHelmet;
				item[4311+i][4] = 1 + i;
			
				item[4317+i][0] = "Titanium Kettle Helmet";
				item[4317+i][1] = "";
				item[4317+i][2] = item_titaniumKettleHelmet;
				item[4317+i][4] = 1 + i;
		
				item[4323+i][0] = "Platinum Kettle Helmet";
				item[4323+i][1] = "";
				item[4323+i][2] = item_platinumKettleHelmet;
				item[4323+i][4] = 1 + i;
			
				item[4329+i][0] = "Tungsten Kettle Helmet";
				item[4329+i][1] = "";
				item[4329+i][2] = item_tungstenKettleHelmet;
				item[4329+i][4] = 1 + i;
			
			}
			
			item[4335][0] = "Soulscape Kettle Helmet";
			item[4335][1] = "";
			item[4335][2] = item_soulscapeKettleHelmet;
			item[4335][4] = 6;
			
		}
		
		{ //Breastplate
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[4336+i][0] = "Lead Breastplate";
				item[4336+i][1] = "";
				item[4336+i][2] = item_leadBreastplate;
				item[4336+i][4] = i + 1;
			
				item[4342+i][0] = "Tin Breastplate";
				item[4342+i][1] = "";
				item[4342+i][2] = item_tinBreastplate;
				item[4342+i][4] = i + 1;
			
				item[4348+i][0] = "Gold Breastplate";
				item[4348+i][1] = "";
				item[4348+i][2] = item_goldBreastplate;
				item[4348+i][4] = 1 + i;
			
				item[4354+i][0] = "Silver Breastplate";
				item[4354+i][1] = "";
				item[4354+i][2] = item_silverBreastplate;
				item[4354+i][4] = 1 + i;
			
				item[4360+i][0] = "Copper Breastplate";
				item[4360+i][1] = "";
				item[4360+i][2] = item_copperBreastplate;
				item[4360+i][4] = 1 + i;
			
				item[4366+i][0] = "Brass Breastplate";
				item[4366+i][1] = "";
				item[4366+i][2] = item_brassBreastplate;
				item[4366+i][4] = 1 + i;
			
				item[4372+i][0] = "Aluminum Breastplate";
				item[4372+i][1] = "";
				item[4372+i][2] = item_aluminumBreastplate;
				item[4372+i][4] = 1 + i;
			
				item[4378+i][0] = "Iron Breastplate";
				item[4378+i][1] = "";
				item[4378+i][2] = item_ironBreastplate;
				item[4378+i][4] = 1 + i;
			
				item[4384+i][0] = "Titanium Breastplate";
				item[4384+i][1] = "";
				item[4384+i][2] = item_titaniumBreastplate;
				item[4384+i][4] = 1 + i;
		
				item[4390+i][0] = "Platinum Breastplate";
				item[4390+i][1] = "";
				item[4390+i][2] = item_platinumBreastplate;
				item[4390+i][4] = 1 + i;
			
				item[4396+i][0] = "Tungsten Breastplate";
				item[4396+i][1] = "";
				item[4396+i][2] = item_tungstenBreastplate;
				item[4396+i][4] = 1 + i;
			
			}
			
			item[4402][0] = "Soulscape Breastplate";
			item[4402][1] = "";
			item[4402][2] = item_soulscapeBreastplate;
			item[4402][4] = 6;
			
		}
		
		{ //Greaves
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[4403+i][0] = "Lead Greaves";
				item[4403+i][1] = "";
				item[4403+i][2] = item_leadGreaves;
				item[4403+i][4] = i + 1;
			
				item[4409+i][0] = "Tin Greaves";
				item[4409+i][1] = "";
				item[4409+i][2] = item_tinGreaves;
				item[4409+i][4] = i + 1;
			
				item[4415+i][0] = "Gold Greaves";
				item[4415+i][1] = "";
				item[4415+i][2] = item_goldGreaves;
				item[4415+i][4] = 1 + i;
			
				item[4421+i][0] = "Silver Greaves";
				item[4421+i][1] = "";
				item[4421+i][2] = item_silverGreaves;
				item[4421+i][4] = 1 + i;
			
				item[4427+i][0] = "Copper Greaves";
				item[4427+i][1] = "";
				item[4427+i][2] = item_copperGreaves;
				item[4427+i][4] = 1 + i;
			
				item[4433+i][0] = "Brass Greaves";
				item[4433+i][1] = "";
				item[4433+i][2] = item_brassGreaves;
				item[4433+i][4] = 1 + i;
			
				item[4439+i][0] = "Aluminum Greaves";
				item[4439+i][1] = "";
				item[4439+i][2] = item_aluminumGreaves;
				item[4439+i][4] = 1 + i;
			
				item[4445+i][0] = "Iron Greaves";
				item[4445+i][1] = "";
				item[4445+i][2] = item_ironGreaves;
				item[4445+i][4] = 1 + i;
			
				item[4451+i][0] = "Titanium Greaves";
				item[4451+i][1] = "";
				item[4451+i][2] = item_titaniumGreaves;
				item[4451+i][4] = 1 + i;
		
				item[4457+i][0] = "Platinum Greaves";
				item[4457+i][1] = "";
				item[4457+i][2] = item_platinumGreaves;
				item[4457+i][4] = 1 + i;
			
				item[4463+i][0] = "Tungsten Greaves";
				item[4463+i][1] = "";
				item[4463+i][2] = item_tungstenGreaves;
				item[4463+i][4] = 1 + i;
			
			}
			
			item[4469][0] = "Soulscape Greaves";
			item[4469][1] = "";
			item[4469][2] = item_soulscapeGreaves;
			item[4469][4] = 6;
			
		}
		
		{ //Gauntlet
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[4470+i][0] = "Lead Gauntlet";
				item[4470+i][1] = "";
				item[4470+i][2] = item_leadGauntlet;
				item[4470+i][4] = i + 1;
			
				item[4476+i][0] = "Tin Gauntlet";
				item[4476+i][1] = "";
				item[4476+i][2] = item_tinGauntlet;
				item[4476+i][4] = i + 1;
			
				item[4482+i][0] = "Gold Gauntlet";
				item[4482+i][1] = "";
				item[4482+i][2] = item_goldGauntlet;
				item[4482+i][4] = 1 + i;
			
				item[4488+i][0] = "Silver Gauntlet";
				item[4488+i][1] = "";
				item[4488+i][2] = item_silverGauntlet;
				item[4488+i][4] = 1 + i;
			
				item[4494+i][0] = "Copper Gauntlet";
				item[4494+i][1] = "";
				item[4494+i][2] = item_copperGauntlet;
				item[4494+i][4] = 1 + i;
			
				item[4500+i][0] = "Brass Gauntlet";
				item[4500+i][1] = "";
				item[4500+i][2] = item_brassGauntlet;
				item[4500+i][4] = 1 + i;
			
				item[4506+i][0] = "Aluminum Gauntlet";
				item[4506+i][1] = "";
				item[4506+i][2] = item_aluminumGauntlet;
				item[4506+i][4] = 1 + i;
			
				item[4512+i][0] = "Iron Gauntlet";
				item[4512+i][1] = "";
				item[4512+i][2] = item_ironGauntlet;
				item[4512+i][4] = 1 + i;
			
				item[4518+i][0] = "Titanium Gauntlet";
				item[4518+i][1] = "";
				item[4518+i][2] = item_titaniumGauntlet;
				item[4518+i][4] = 1 + i;
		
				item[4524+i][0] = "Platinum Gauntlet";
				item[4524+i][1] = "";
				item[4524+i][2] = item_platinumGauntlet;
				item[4524+i][4] = 1 + i;
			
				item[4530+i][0] = "Tungsten Gauntlet";
				item[4530+i][1] = "";
				item[4530+i][2] = item_tungstenGauntlet;
				item[4530+i][4] = 1 + i;
			
			}
			
			item[4536][0] = "Soulscape Gauntlet";
			item[4536][1] = "";
			item[4536][2] = item_soulscapeGauntlet;
			item[4536][4] = 6;
			
		}
		
		{ //Leg Plates
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[4537+i][0] = "Lead Leg Plates";
				item[4537+i][1] = "";
				item[4537+i][2] = item_leadLegPlates;
				item[4537+i][4] = i + 1;
			
				item[4543+i][0] = "Tin Leg Plates";
				item[4543+i][1] = "";
				item[4543+i][2] = item_tinLegPlates;
				item[4543+i][4] = i + 1;
			
				item[4549+i][0] = "Gold Leg Plates";
				item[4549+i][1] = "";
				item[4549+i][2] = item_goldLegPlates;
				item[4549+i][4] = 1 + i;
			
				item[4555+i][0] = "Silver Leg Plates";
				item[4555+i][1] = "";
				item[4555+i][2] = item_silverLegPlates;
				item[4555+i][4] = 1 + i;
			
				item[4561+i][0] = "Copper Leg Plates";
				item[4561+i][1] = "";
				item[4561+i][2] = item_copperLegPlates;
				item[4561+i][4] = 1 + i;
			
				item[4567+i][0] = "Brass Leg Plates";
				item[4567+i][1] = "";
				item[4567+i][2] = item_brassLegPlates;
				item[4567+i][4] = 1 + i;
			
				item[4573+i][0] = "Aluminum Leg Plates";
				item[4573+i][1] = "";
				item[4573+i][2] = item_aluminumLegPlates;
				item[4573+i][4] = 1 + i;
			
				item[4579+i][0] = "Iron Leg Plates";
				item[4579+i][1] = "";
				item[4579+i][2] = item_ironLegPlates;
				item[4579+i][4] = 1 + i;
			
				item[4585+i][0] = "Titanium Leg Plates";
				item[4585+i][1] = "";
				item[4585+i][2] = item_titaniumLegPlates;
				item[4585+i][4] = 1 + i;
		
				item[4591+i][0] = "Platinum Leg Plates";
				item[4591+i][1] = "";
				item[4591+i][2] = item_platinumLegPlates;
				item[4591+i][4] = 1 + i;
			
				item[4597+i][0] = "Tungsten Leg Plates";
				item[4597+i][1] = "";
				item[4597+i][2] = item_tungstenLegPlates;
				item[4597+i][4] = 1 + i;
			
			}
			
			item[4603][0] = "Soulscape Leg Plates";
			item[4603][1] = "";
			item[4603][2] = item_soulscapeLegPlates;
			item[4603][4] = 6;
			
		}
		
		{ //Arm Plates
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[4604+i][0] = "Lead Arm Plates";
				item[4604+i][1] = "";
				item[4604+i][2] = item_leadArmPlates;
				item[4604+i][4] = i + 1;
			
				item[4610+i][0] = "Tin Arm Plates";
				item[4610+i][1] = "";
				item[4610+i][2] = item_tinArmPlates;
				item[4610+i][4] = i + 1;
			
				item[4616+i][0] = "Gold Arm Plates";
				item[4616+i][1] = "";
				item[4616+i][2] = item_goldArmPlates;
				item[4616+i][4] = 1 + i;
			
				item[4622+i][0] = "Silver Arm Plates";
				item[4622+i][1] = "";
				item[4622+i][2] = item_silverArmPlates;
				item[4622+i][4] = 1 + i;
			
				item[4628+i][0] = "Copper Arm Plates";
				item[4628+i][1] = "";
				item[4628+i][2] = item_copperArmPlates;
				item[4628+i][4] = 1 + i;
			
				item[4634+i][0] = "Brass Arm Plates";
				item[4634+i][1] = "";
				item[4634+i][2] = item_brassArmPlates;
				item[4634+i][4] = 1 + i;
			
				item[4640+i][0] = "Aluminum Arm Plates";
				item[4640+i][1] = "";
				item[4640+i][2] = item_aluminumArmPlates;
				item[4640+i][4] = 1 + i;
			
				item[4646+i][0] = "Iron Arm Plates";
				item[4646+i][1] = "";
				item[4646+i][2] = item_ironArmPlates;
				item[4646+i][4] = 1 + i;
			
				item[4652+i][0] = "Titanium Arm Plates";
				item[4652+i][1] = "";
				item[4652+i][2] = item_titaniumArmPlates;
				item[4652+i][4] = 1 + i;
		
				item[4658+i][0] = "Platinum Arm Plates";
				item[4658+i][1] = "";
				item[4658+i][2] = item_platinumArmPlates;
				item[4658+i][4] = 1 + i;
			
				item[4664+i][0] = "Tungsten Arm Plates";
				item[4664+i][1] = "";
				item[4664+i][2] = item_tungstenArmPlates;
				item[4664+i][4] = 1 + i;
			
			}
			
			item[4670][0] = "Soulscape Arm Plates";
			item[4670][1] = "";
			item[4670][2] = item_soulscapeArmPlates;
			item[4670][4] = 6;
			
		}
		
		{ //Kite Shield
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[4671+i][0] = "Lead Kite Shield";
				item[4671+i][1] = "";
				item[4671+i][2] = item_leadKiteShield;
				item[4671+i][4] = i + 1;
			
				item[4677+i][0] = "Tin Kite Shield";
				item[4677+i][1] = "";
				item[4677+i][2] = item_tinKiteShield;
				item[4677+i][4] = i + 1;
			
				item[4683+i][0] = "Gold Kite Shield";
				item[4683+i][1] = "";
				item[4683+i][2] = item_goldKiteShield;
				item[4683+i][4] = 1 + i;
			
				item[4689+i][0] = "Silver Kite Shield";
				item[4689+i][1] = "";
				item[4689+i][2] = item_silverKiteShield;
				item[4689+i][4] = 1 + i;
			
				item[4695+i][0] = "Copper Kite Shield";
				item[4695+i][1] = "";
				item[4695+i][2] = item_copperKiteShield;
				item[4695+i][4] = 1 + i;
			
				item[4701+i][0] = "Brass Kite Shield";
				item[4701+i][1] = "";
				item[4701+i][2] = item_brassKiteShield;
				item[4701+i][4] = 1 + i;
			
				item[4707+i][0] = "Aluminum Kite Shield";
				item[4707+i][1] = "";
				item[4707+i][2] = item_aluminumKiteShield;
				item[4707+i][4] = 1 + i;
			
				item[4713+i][0] = "Iron Kite Shield";
				item[4713+i][1] = "";
				item[4713+i][2] = item_ironKiteShield;
				item[4713+i][4] = 1 + i;
			
				item[4719+i][0] = "Titanium Kite Shield";
				item[4719+i][1] = "";
				item[4719+i][2] = item_titaniumKiteShield;
				item[4719+i][4] = 1 + i;
		
				item[4725+i][0] = "Platinum Kite Shield";
				item[4725+i][1] = "";
				item[4725+i][2] = item_platinumKiteShield;
				item[4725+i][4] = 1 + i;
			
				item[4731+i][0] = "Tungsten Kite Shield";
				item[4731+i][1] = "";
				item[4731+i][2] = item_tungstenKiteShield;
				item[4731+i][4] = 1 + i;
			
			}
			
			item[4737][0] = "Soulscape Kite Shield";
			item[4737][1] = "";
			item[4737][2] = item_soulscapeKiteShield;
			item[4737][4] = 6;
			
		}
		
		{ //Buckler Shield
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[4738+i][0] = "Lead Buckler Shield";
				item[4738+i][1] = "";
				item[4738+i][2] = item_leadBucklerShield;
				item[4738+i][4] = i + 1;
			
				item[4744+i][0] = "Tin Buckler Shield";
				item[4744+i][1] = "";
				item[4744+i][2] = item_tinBucklerShield;
				item[4744+i][4] = i + 1;
			
				item[4750+i][0] = "Gold Buckler Shield";
				item[4750+i][1] = "";
				item[4750+i][2] = item_goldBucklerShield;
				item[4750+i][4] = 1 + i;
			
				item[4756+i][0] = "Silver Buckler Shield";
				item[4756+i][1] = "";
				item[4756+i][2] = item_silverBucklerShield;
				item[4756+i][4] = 1 + i;
			
				item[4762+i][0] = "Copper Buckler Shield";
				item[4762+i][1] = "";
				item[4762+i][2] = item_copperBucklerShield;
				item[4762+i][4] = 1 + i;
			
				item[4768+i][0] = "Brass Buckler Shield";
				item[4768+i][1] = "";
				item[4768+i][2] = item_brassBucklerShield;
				item[4768+i][4] = 1 + i;
			
				item[4774+i][0] = "Aluminum Buckler Shield";
				item[4774+i][1] = "";
				item[4774+i][2] = item_aluminumBucklerShield;
				item[4774+i][4] = 1 + i;
			
				item[4780+i][0] = "Iron Buckler Shield";
				item[4780+i][1] = "";
				item[4780+i][2] = item_ironBucklerShield;
				item[4780+i][4] = 1 + i;
			
				item[4786+i][0] = "Titanium Buckler Shield";
				item[4786+i][1] = "";
				item[4786+i][2] = item_titaniumBucklerShield;
				item[4786+i][4] = 1 + i;
		
				item[4792+i][0] = "Platinum Buckler Shield";
				item[4792+i][1] = "";
				item[4792+i][2] = item_platinumBucklerShield;
				item[4792+i][4] = 1 + i;
			
				item[4798+i][0] = "Tungsten Buckler Shield";
				item[4798+i][1] = "";
				item[4798+i][2] = item_tungstenBucklerShield;
				item[4798+i][4] = 1 + i;
			
			}
			
			item[4804][0] = "Soulscape Buckler Shield";
			item[4804][1] = "";
			item[4804][2] = item_soulscapeBucklerShield;
			item[4804][4] = 6;
			
		}
		
		{ //Great Helmet
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[4805+i][0] = "Lead Great Helmet";
				item[4805+i][1] = "";
				item[4805+i][2] = item_leadGreatHelmet;
				item[4805+i][4] = i + 1;
			
				item[4811+i][0] = "Tin Great Helmet";
				item[4811+i][1] = "";
				item[4811+i][2] = item_tinGreatHelmet;
				item[4811+i][4] = i + 1;
			
				item[4817+i][0] = "Gold Great Helmet";
				item[4817+i][1] = "";
				item[4817+i][2] = item_goldGreatHelmet;
				item[4817+i][4] = 1 + i;
			
				item[4823+i][0] = "Silver Great Helmet";
				item[4823+i][1] = "";
				item[4823+i][2] = item_silverGreatHelmet;
				item[4823+i][4] = 1 + i;
			
				item[4829+i][0] = "Copper Great Helmet";
				item[4829+i][1] = "";
				item[4829+i][2] = item_copperGreatHelmet;
				item[4829+i][4] = 1 + i;
			
				item[4835+i][0] = "Brass Great Helmet";
				item[4835+i][1] = "";
				item[4835+i][2] = item_brassGreatHelmet;
				item[4835+i][4] = 1 + i;
			
				item[4841+i][0] = "Aluminum Great Helmet";
				item[4841+i][1] = "";
				item[4841+i][2] = item_aluminumGreatHelmet;
				item[4841+i][4] = 1 + i;
			
				item[4847+i][0] = "Iron Great Helmet";
				item[4847+i][1] = "";
				item[4847+i][2] = item_ironGreatHelmet;
				item[4847+i][4] = 1 + i;
			
				item[4853+i][0] = "Titanium Great Helmet";
				item[4853+i][1] = "";
				item[4853+i][2] = item_titaniumGreatHelmet;
				item[4853+i][4] = 1 + i;
		
				item[4859+i][0] = "Platinum Great Helmet";
				item[4859+i][1] = "";
				item[4859+i][2] = item_platinumGreatHelmet;
				item[4859+i][4] = 1 + i;
			
				item[4865+i][0] = "Tungsten Great Helmet";
				item[4865+i][1] = "";
				item[4865+i][2] = item_tungstenGreatHelmet;
				item[4865+i][4] = 1 + i;
			
			}
			
			item[4871][0] = "Soulscape Great Helmet";
			item[4871][1] = "";
			item[4871][2] = item_soulscapeGreatHelmet;
			item[4871][4] = 6;
			
		}
		
		{ //Crown
			var qualityLevels = 6;
		
			for(i = 0; i < qualityLevels; i++) {
			
				item[4872+i][0] = "Lead Crown";
				item[4872+i][1] = "";
				item[4872+i][2] = item_leadCrown;
				item[4872+i][4] = i + 1;
			
				item[4878+i][0] = "Tin Crown";
				item[4878+i][1] = "";
				item[4878+i][2] = item_tinCrown;
				item[4878+i][4] = i + 1;
			
				item[4884+i][0] = "Gold Crown";
				item[4884+i][1] = "";
				item[4884+i][2] = item_goldCrown;
				item[4884+i][4] = 1 + i;
			
				item[4890+i][0] = "Silver Crown";
				item[4890+i][1] = "";
				item[4890+i][2] = item_silverCrown;
				item[4890+i][4] = 1 + i;
			
				item[4896+i][0] = "Copper Crown";
				item[4896+i][1] = "";
				item[4896+i][2] = item_copperCrown;
				item[4896+i][4] = 1 + i;
			
				item[4902+i][0] = "Brass Crown";
				item[4902+i][1] = "";
				item[4902+i][2] = item_brassCrown;
				item[4902+i][4] = 1 + i;
			
				item[4908+i][0] = "Aluminum Crown";
				item[4908+i][1] = "";
				item[4908+i][2] = item_aluminumCrown;
				item[4908+i][4] = 1 + i;
			
				item[4914+i][0] = "Iron Crown";
				item[4914+i][1] = "";
				item[4914+i][2] = item_ironCrown;
				item[4914+i][4] = 1 + i;
			
				item[4920+i][0] = "Titanium Crown";
				item[4920+i][1] = "";
				item[4920+i][2] = item_titaniumCrown;
				item[4920+i][4] = 1 + i;
		
				item[4926+i][0] = "Platinum Crown";
				item[4926+i][1] = "";
				item[4926+i][2] = item_platinumCrown;
				item[4926+i][4] = 1 + i;
			
				item[4932+i][0] = "Tungsten Crown";
				item[4932+i][1] = "";
				item[4932+i][2] = item_tungstenCrown;
				item[4932+i][4] = 1 + i;
			
			}
			
			item[4938][0] = "Soulscape Crown";
			item[4938][1] = "";
			item[4938][2] = item_soulscapeCrown;
			item[4938][4] = 6;
			
		}
	}
	
	{ //Brewed Potions
		
		item[671][0] = "Feeble Tonic";
		item[671][1] = "";
		item[671][2] = item_feebleTonic;
		item[671][4] = 1;
		
		item[672][0] = "Feeble Speed Brew";
		item[672][1] = "";
		item[672][2] = item_feebleSpeedBrew;
		item[672][4] = 1;
		
		item[673][0] = "Feeble Bleeding Elixir";
		item[673][1] = "";
		item[673][2] = item_feebleBleedingElixir;
		item[673][4] = 1;
		
		item[674][0] = "Feeble Experience Stimulant";
		item[674][1] = "";
		item[674][2] = item_feebleExperienceStimulant;
		item[674][4] = 1;
		
	}
}

{ //Inventory
	
	globalvar mouseItem;
	mouseItem[0] = 0;
	mouseItem[1] = 0;
	
	globalvar inventorySlotSize, inventorySlotX, inventorySlotY, inventoryBuffer, inventorySlotsWide;
	inventorySlotSize = 64;
	inventoryBuffer = 16;
	inventorySlotX = craftingWingWidth + inventoryBuffer;
	inventorySlotY = 64;
	
	inventorySlotsWide = 20;
	
	globalvar inventorySlots, inventory;
	inventorySlots = 200;
	
	for(i = 0; i < inventorySlots; i++) {
		inventory[i][0] = 0; //What's in it
		inventory[i][1] = 0; //How many?
	}
	
	/*Adds All Wood And Ores Into Inventory
	for(i = 269; i < 335; i++) { //_
		inventory[i - 269][0] = i;
		inventory[i - 269][1] = 1;
		
	}
	*/
	
}

{ //Item Description Stats
	
	globalvar itemDescriptionWidth, itemDescriptionHeight;
	itemDescriptionWidth = 256;
	itemDescriptionHeight = 98;
	
}

{ //Real World Time
	
	globalvar year, month, day, hour, minute, second;
	year = current_year;
	month = current_month;
	day = current_day;
	hour = current_hour;
	minute = current_minute;
	second = current_second;
	
	globalvar yearP, monthP, dayP, hourP, minuteP, secondP;
	yearP = current_year;
	monthP = current_month;
	dayP = current_day;
	hourP = current_hour;
	minuteP = current_minute;
	secondP = current_second;
	
	globalvar timeSinceLastPlayed;
	timeSinceLastPlayed = 0;
	
	alarm[1] = 1;
	
}

{ //Smelting
	
	globalvar smeltingInput, smeltingOutput, smeltingFuel;
	smeltingInput[0] = 0;
	smeltingInput[1] = 0;
	
	smeltingOutput[0] = 0;
	smeltingOutput[1] = 0;
	
	smeltingFuel = 0;
	
	globalvar smeltingTime;
	
	smeltingTime[0] = 0;
	smeltingTime[1] = 600;
	
}

{ //Log Planing
	
	globalvar logPlaningInput, logPlaningOutput, logPlaningFuel;
	logPlaningInput[0] = 0;
	logPlaningInput[1] = 0;
	
	logPlaningOutput[0] = 0;
	logPlaningOutput[1] = 0;
	
	logPlaningFuel = 0;
	
	globalvar logPlaningTime;
	
	logPlaningTime[0] = 0;
	logPlaningTime[1] = 600;
	
}

{ //Farming
	
	{ //Setting Up Plots
		globalvar farmingPlot, farmingSectionSize, farmingNumberOfSections, farmingSectionsPerRow, farmingSectionOwned;
	
		farmingNumberOfSections = 25;
		farmingSectionSize = 8;
		farmingSectionsPerRow = 5;
	
		for(section = 0; section < farmingNumberOfSections; section++) { //Establishing All Plots
			
			farmingSectionOwned[section] = false;
			
			for(X = 0; X < farmingSectionSize; X++) {
			
				for(Y = 0; Y < farmingSectionSize; Y++) {
				
					farmingPlot[section][X][Y][0] = 0; //Ground texture for slot
					farmingPlot[section][X][Y][1] = 0; //Plant ID
					farmingPlot[section][X][Y][2] = 0; //Current progress
					farmingPlot[section][X][Y][3] = 0; //How long until slot is fully developed
				
				}
			
			}
		
		}
	}
	
	//_
	farmingSectionOwned[irandom(farmingNumberOfSections)] = true;
	
	{ //Plot Sprite Index
		
		globalvar farmingPlotSprite, farmingPlantSprite;
		
		farmingPlotSprite[0] = spr_farming_texture_grass;
		farmingPlotSprite[1] = spr_farming_texture_tilled;
		farmingPlotSprite[2] = spr_farming_texture_watered;
		
		farmingPlantSprite[0] = spr_empty;
		farmingPlantSprite[537] = spr_farming_plant_grass;
		farmingPlantSprite[538] = spr_farming_plant_grass;
		farmingPlantSprite[539] = spr_farming_plant_grass;
		farmingPlantSprite[540] = spr_farming_plant_grass;
		farmingPlantSprite[541] = spr_farming_plant_grass;
		farmingPlantSprite[542] = spr_farming_plant_grass;
		farmingPlantSprite[543] = spr_farming_plant_grass;
		farmingPlantSprite[544] = spr_farming_plant_grass;
		farmingPlantSprite[545] = spr_farming_plant_grass;
		farmingPlantSprite[546] = spr_farming_plant_grass;
		
		
	}
	
	{ //Seed Information
		
		globalvar farmingSeed;
		
		farmingSeed[537][0] = 10; //Minutes To Grow
		farmingSeed[537][1] = 604; //Produce Dropped
		
		farmingSeed[538][0] = 10; //Minutes To Grow
		farmingSeed[538][1] = 605; //Produce Dropped
		
		farmingSeed[539][0] = 10; //Minutes To Grow
		farmingSeed[539][1] = 606; //Produce Dropped
		
		farmingSeed[540][0] = 10; //Minutes To Grow
		farmingSeed[540][1] = 607; //Produce Dropped
		
		farmingSeed[541][0] = 10; //Minutes To Grow
		farmingSeed[541][1] = 608; //Produce Dropped
		
		farmingSeed[542][0] = 10; //Minutes To Grow
		farmingSeed[542][1] = 609; //Produce Dropped
		
		farmingSeed[543][0] = 10; //Minutes To Grow
		farmingSeed[543][1] = 610; //Produce Dropped
		
		farmingSeed[544][0] = 10; //Minutes To Grow
		farmingSeed[544][1] = 611; //Produce Dropped
		
		farmingSeed[545][0] = 10; //Minutes To Grow
		farmingSeed[545][1] = 612; //Produce Dropped
		
		farmingSeed[546][0] = 10; //Minutes To Grow
		farmingSeed[546][1] = 613; //Produce Dropped
		
		
	}
	
}

{ //Meat Carving
	
	globalvar meatCarvingInput, meatCarvingOutput, meatCarvingFuel;
	
	meatCarvingInput[0] = 0;
	meatCarvingInput[1] = 0;
	
	meatCarvingOutput[0] = 0;
	meatCarvingOutput[1] = 0;
	
	meatCarvingFuel = 0;
	
	globalvar meatCarvingTime;
	meatCarvingTime[0] = 0;
	meatCarvingTime[1] = 600;
	
}

{ //Bladecrafting
	
	globalvar bladeCraftingInput, bladeCraftingOutput, bladeCraftingRequiredAmount, bladeCraftingSelectedItem;
	
	bladeCraftingInput[0] = 0;
	bladeCraftingInput[1] = 0;
	
	bladeCraftingOutput[0] = 0;
	bladeCraftingOutput[1] = 0;
	
	bladeCraftingRequiredAmount = 0;
	
	bladeCraftingSelectedItem = 1;
	
	globalvar bladeCraftingTime;
	bladeCraftingTime[0] = 0;
	bladeCraftingTime[1] = 6000;
	
	{ //Recipe Stuff
		
		/*
		[0] = Name
		[1] = Required Level
		[2] = Required Amount
		*/
		
		
		globalvar bladeCraftingIndex;
		
		bladeCraftingIndex[0][0] = "Carving Knife";
		bladeCraftingIndex[0][1] = 0;
		bladeCraftingIndex[0][2] = 2;
		
		bladeCraftingIndex[1][0] = "Practice Sword";
		bladeCraftingIndex[1][1] = 50;
		bladeCraftingIndex[1][2] = 3;
		
		bladeCraftingIndex[2][0] = "Dagger";
		bladeCraftingIndex[2][1] = 100;
		bladeCraftingIndex[2][2] = 3;
		
		bladeCraftingIndex[3][0] = "Shortsword";
		bladeCraftingIndex[3][1] = 150;
		bladeCraftingIndex[3][2] = 4;
		
		bladeCraftingIndex[4][0] = "Cutlass";
		bladeCraftingIndex[4][1] = 200;
		bladeCraftingIndex[4][2] = 4;
		
		bladeCraftingIndex[5][0] = "Saber";
		bladeCraftingIndex[5][1] = 250;
		bladeCraftingIndex[5][2] = 4;
		
		bladeCraftingIndex[6][0] = "Longsword";
		bladeCraftingIndex[6][1] = 300;
		bladeCraftingIndex[6][2] = 5;
		
		bladeCraftingIndex[7][0] = "Rapier";
		bladeCraftingIndex[7][1] = 350;
		bladeCraftingIndex[7][2] = 4;
		
		bladeCraftingIndex[8][0] = "Backsword";
		bladeCraftingIndex[8][1] = 400;
		bladeCraftingIndex[8][2] = 6;
		
		bladeCraftingIndex[9][0] = "Gladius";
		bladeCraftingIndex[9][1] = 450;
		bladeCraftingIndex[9][2] = 5;
		
		bladeCraftingIndex[10][0] = "Katana";
		bladeCraftingIndex[10][1] = 500;
		bladeCraftingIndex[10][2] = 5;
		
		bladeCraftingIndex[11][0] = "Dirk";
		bladeCraftingIndex[11][1] = 550;
		bladeCraftingIndex[11][2] = 4;
		
		bladeCraftingIndex[12][0] = "Broadsword";
		bladeCraftingIndex[12][1] = 600;
		bladeCraftingIndex[12][2] = 8;
		
		bladeCraftingIndex[13][0] = "Claymore";
		bladeCraftingIndex[13][1] = 650;
		bladeCraftingIndex[13][2] = 10;
		
	}
	
}

{ //Woodworking
	
	globalvar woodworkingInput, woodworkingOutput, woodworkingRequiredAmount, woodworkingSelectedItem;
	
	woodworkingInput[0] = 0;
	woodworkingInput[1] = 0;
	
	woodworkingOutput[0] = 0;
	woodworkingOutput[1] = 0;
	
	woodworkingRequiredAmount = 0;
	
	woodworkingSelectedItem = 1;
	
	globalvar woodworkingTime;
	woodworkingTime[0] = 0;
	woodworkingTime[1] = 6000;
	
	{ //Recipe Stuff
		
		/*
		[0] = Name
		[1] = Required Level
		[2] = Required Amount
		*/
		
		
		globalvar woodworkingIndex;
		
		woodworkingIndex[0][0] = "Arrow";
		woodworkingIndex[0][1] = 0;
		woodworkingIndex[0][2] = 1;
		
		woodworkingIndex[1][0] = "Chair";
		woodworkingIndex[1][1] = 50;
		woodworkingIndex[1][2] = 4;
		
		woodworkingIndex[2][0] = "Short Bow";
		woodworkingIndex[2][1] = 100;
		woodworkingIndex[2][2] = 3;
		
		woodworkingIndex[3][0] = "Table";
		woodworkingIndex[3][1] = 150;
		woodworkingIndex[3][2] = 8;
		
		woodworkingIndex[4][0] = "Long Bow";
		woodworkingIndex[4][1] = 200;
		woodworkingIndex[4][2] = 4;
		
		woodworkingIndex[5][0] = "Bowl";
		woodworkingIndex[5][1] = 250;
		woodworkingIndex[5][2] = 1;
		
		woodworkingIndex[6][0] = "Chest";
		woodworkingIndex[6][1] = 300;
		woodworkingIndex[6][2] = 5;
		
		woodworkingIndex[7][0] = "Lute";
		woodworkingIndex[7][1] = 350;
		woodworkingIndex[7][2] = 3;
		
		woodworkingIndex[8][0] = "Bed";
		woodworkingIndex[8][1] = 400;
		woodworkingIndex[8][2] = 8;
		
		woodworkingIndex[9][0] = "Crossbow";
		woodworkingIndex[9][1] = 450;
		woodworkingIndex[9][2] = 4;
		
		woodworkingIndex[10][0] = "Barrel";
		woodworkingIndex[10][1] = 500;
		woodworkingIndex[10][2] = 6;
		
		woodworkingIndex[11][0] = "Staff";
		woodworkingIndex[11][1] = 550;
		woodworkingIndex[11][2] = 2;
		
		woodworkingIndex[12][0] = "Caravan Cart";
		woodworkingIndex[12][1] = 600;
		woodworkingIndex[12][2] = 20;
		
		woodworkingIndex[13][0] = "Catapult";
		woodworkingIndex[13][1] = 650;
		woodworkingIndex[13][2] = 40;
		
	}
	
}

{ //Armor Crafting
	
	globalvar armorCraftingInput, armorCraftingOutput, armorCraftingRequiredAmount, armorCraftingSelectedItem;
	
	armorCraftingInput[0] = 0;
	armorCraftingInput[1] = 0;
	
	armorCraftingOutput[0] = 0;
	armorCraftingOutput[1] = 0;
	
	armorCraftingRequiredAmount = 0;
	
	armorCraftingSelectedItem = 1;
	
	globalvar armorCraftingTime;
	armorCraftingTime[0] = 0;
	armorCraftingTime[1] = 6000;
	
	{ //Recipe Stuff
		
		/*
		[0] = Name
		[1] = Required Level
		[2] = Required Amount
		*/
		
		
		globalvar armorCraftingIndex;
		
		armorCraftingIndex[0][0] = "Chain Mail Shirt";
		armorCraftingIndex[0][1] = 0;
		armorCraftingIndex[0][2] = 6;
		
		armorCraftingIndex[1][0] = "Chain Mail Pants";
		armorCraftingIndex[1][1] = 50;
		armorCraftingIndex[1][2] = 5;
		
		armorCraftingIndex[2][0] = "Chain Mail Hood";
		armorCraftingIndex[2][1] = 100;
		armorCraftingIndex[2][2] = 5;
		
		armorCraftingIndex[3][0] = "Chain Mail Gloves";
		armorCraftingIndex[3][1] = 150;
		armorCraftingIndex[3][2] = 3;
		
		armorCraftingIndex[4][0] = "Kettle Helmet";
		armorCraftingIndex[4][1] = 200;
		armorCraftingIndex[4][2] = 5;
		
		armorCraftingIndex[5][0] = "Breastplate";
		armorCraftingIndex[5][1] = 250;
		armorCraftingIndex[5][2] = 10;
		
		armorCraftingIndex[6][0] = "Greaves";
		armorCraftingIndex[6][1] = 300;
		armorCraftingIndex[6][2] = 6;
		
		armorCraftingIndex[7][0] = "Gauntlet";
		armorCraftingIndex[7][1] = 350;
		armorCraftingIndex[7][2] = 6;
		
		armorCraftingIndex[8][0] = "Leg Plates";
		armorCraftingIndex[8][1] = 400;
		armorCraftingIndex[8][2] = 8;
		
		armorCraftingIndex[9][0] = "Arm Plates";
		armorCraftingIndex[9][1] = 450;
		armorCraftingIndex[9][2] = 8;
		
		armorCraftingIndex[10][0] = "Kite Shield";
		armorCraftingIndex[10][1] = 500;
		armorCraftingIndex[10][2] = 15;
		
		armorCraftingIndex[11][0] = "Buckler Shield";
		armorCraftingIndex[11][1] = 550;
		armorCraftingIndex[11][2] = 25;
		
		armorCraftingIndex[12][0] = "Great Helmet";
		armorCraftingIndex[12][1] = 600;
		armorCraftingIndex[12][2] = 10;
		
		armorCraftingIndex[13][0] = "Crown";
		armorCraftingIndex[13][1] = 650;
		armorCraftingIndex[13][2] = 15;
		
	}
	
}

{ //Brewing
	
	globalvar brewingInputOne, brewingInputTwo, brewingInputThree, brewingOutput, brewingTime, brewingSelectedItem;
	
	brewingSelectedItem = 0;
	
	brewingInputOne[0] = 0;
	brewingInputOne[1] = 0;
	
	brewingInputTwo[0] = 612;
	brewingInputTwo[1] = 0;
	
	brewingInputThree[0] = 0;
	brewingInputThree[1] = 0;
	
	brewingOutput[0] = 671;
	brewingOutput[1] = 0;
	
	brewingTime[0] = 0;
	brewingTime[1] = 600;
	
	globalvar brewingEnough;
	brewingEnough[1] = false;
	brewingEnough[2] = false;
	brewingEnough[3] = false;
	
	{ //Recipe
		
		globalvar brewingRecipe;
		
		/*
		
		First number is the recipe index
		
		[0] Name
		[1] Required Level
		[2] "i1,i2,i3/amount1,amount2,amount3"
		[3] Output
		
		Must have an entry in all 3 positions. Ex. 0,132,0/0,3,0
		
		*/
		
		brewingRecipe[0][0] = "Feeble Tonic";
		brewingRecipe[0][1] = 1;
		brewingRecipe[0][2] = "0,202,0/0,1,0/";
		brewingRecipe[0][3] = 671;
		
		brewingRecipe[1][0] = "Feeble Speed Brew";
		brewingRecipe[1][1] = 1;
		brewingRecipe[1][2] = "204,204,205/3,1,2/";
		brewingRecipe[1][3] = 671;
		
		brewingRecipe[2][0] = "Feeble Bleeding Elixir";
		brewingRecipe[2][1] = 1;
		brewingRecipe[2][2] = "205,204,205/3,1,2/";
		brewingRecipe[2][3] = 671;
		
		brewingRecipe[3][0] = "Feeble Experience Stimulant";
		brewingRecipe[3][1] = 1;
		brewingRecipe[3][2] = "206,204,205/3,1,2/";
		brewingRecipe[3][3] = 671;
	}
	
}

{ //Cooking
	
	
	
}

{ //Optimization
	
	globalvar maxItemObjects;
	maxItemObjects = 50;

}

{ //Completion Bar
	
	globalvar completionBarW, completionBarH;
	completionBarH = 32;
	completionBarW = 200;
	
}

alarm[2] = 1;
instance_create_depth(x,y,depth,draw);
























