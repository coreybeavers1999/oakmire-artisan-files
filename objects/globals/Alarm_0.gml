/// @description XP Stats


//Updating Required XP Collecting
for(i = 3; i < 3 + numberOfCollectingSkills; i++) {
	//Leveling Up
	if(xp[i] >= requiredXP[i]) { level[i] += 1; }
	
	//Setting XPs
	requiredXP[i] = level[i] + (power(level[i],3));
	prevRequired[i] = (level[i] - 1) + (power(level[i] - 1,3));
}

//Updating Required XP Processing
for(i = 8; i < 8 + numberOfProcessingSkills; i++) {
	//Leveling Up
	if(xp[i] >= requiredXP[i]) { level[i] += 1; }
	
	//Setting XPs
	requiredXP[i] = level[i] + (power(level[i],3));
	prevRequired[i] = (level[i] - 1) + (power(level[i] - 1,3));
}

//Updating Required XP Crafting
for(i = 13; i < 13 + numberOfCraftingSkills; i++) {
	//Leveling Up
	if(xp[i] >= requiredXP[i]) { level[i] += 1; }
	
	//Setting XPs
	requiredXP[i] = level[i] + (power(level[i],3));
	prevRequired[i] = (level[i] - 1) + (power(level[i] - 1,3));
}




//Updating Crafting Wing Stats
cw[3][0] = "Treecutting (" + string(level[3]) + " / 999)";
cw[4][0] = "Mining (" + string(level[4]) + " / 999)";
cw[5][0] = "Fishing (" + string(level[5]) + " / 999)";
cw[6][0] = "Foraging (" + string(level[6]) + " / 999)";

cw[8][0] = "Smelting (" + string(level[8]) + " / 999)";
cw[9][0] = "Log Planing (" + string(level[9]) + " / 999)";
cw[10][0] = "Meat Carving (" + string(level[10]) + " / 999)";
cw[11][0] = "Farming (" + string(level[11]) + " / 999)";

cw[13][0] = "Blade Crafting (" + string(level[13]) + " / 999)";
cw[14][0] = "Woodworking (" + string(level[14]) + " / 999)";
cw[15][0] = "Armor Crafting (" + string(level[15]) + " / 999)";
cw[16][0] = "Brewing (" + string(level[16]) + " / 999)";
cw[17][0] = "Cooking (" + string(level[17]) + " / 999)";
cw[18][0] = "Jewelry Making (" + string(level[18]) + " / 999)";

alarm[0] = 1;