/// @description Log Back In Progress

//Time Since Last Played
timeSinceLastPlayed = date_second_span(date_create_datetime(yearP,monthP,dayP,hourP,minuteP,secondP),date_current_datetime());

timeSinceLastPlayed *= 60;

//Calculate How Many Resources To Add
if(doingCollectingJob[0] == 1) && (timeSinceLastPlayed >= job[doingCollectingJob[1]][doingCollectingJob[2]][4]) { //Has been off long enough for at least 1 run through
	
	var amountOfResources = floor(timeSinceLastPlayed / job[doingCollectingJob[1]][doingCollectingJob[2]][4]);
	show_debug_message("Amount Of Resources = " + string(amountOfResources));
	
	
	for(number = 0; number < amountOfResources; number++) {

		completeJob(doingCollectingJob[1],doingCollectingJob[2]);
		
	}
	
}