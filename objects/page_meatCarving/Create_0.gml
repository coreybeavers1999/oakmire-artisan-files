clickBoxXOffset = 350;
clickBoxYOffset = 175;
clickBoxSize = 64;

osScale = 4;
osX = (screenW + craftingWingWidth) / 2;
osY = screenH - (32 * osScale) - 64;

osSpacing = 48;
osPopHeight = 64;

procHeight = 16;
procBuffer = 32;
procX1 = craftingWingWidth + procBuffer;
procX2 = screenW - procBuffer;
procY1 = procBuffer/2;
procY2 = procY1 + procHeight;




/*
procX1 = osX - clickBoxXOffset;
procX2 = osX + clickBoxXOffset;
procY1 = osY - clickBoxYOffset;
procY2 = procY1 - procHeight;
*/