var drawDescription = false;
var itemDescription;
itemDescription[0] = 0;
itemDescription[1] = 0;
var descriptionX = mouse_x;
var descriptionY = mouse_y;

{ //Drawing Saw In Midde
	
	var sawOn = false;
	if(doingProcessingJob[0] == true && doingProcessingJob[1] == 10) sawOn = true;
	
	var sawX, sawY;
	sawX = (screenW + craftingWingWidth) / 2;
	sawY = screenH / 3;
	
	draw_sprite(spr_meatCarving_knife,sawOn,sawX,sawY);
	
}

{ //Clicking Forge
	
	var fcX1, fcX2, fcY1, fcY2, collisionSafeEdgesX, collisionSafeEdgesY;
	collisionSafeEdgesX = 48;
	collisionSafeEdgesY = 128;
	fcX1 = sawX - (sprite_get_width(spr_meatCarving_knife) / 2) + collisionSafeEdgesX;
	fcY1 = sawY - (sprite_get_height(spr_meatCarving_knife) / 2) + collisionSafeEdgesY;
	fcX2 = sawX + (sprite_get_width(spr_meatCarving_knife) / 2) - collisionSafeEdgesX;
	fcY2 = sawY + (sprite_get_height(spr_meatCarving_knife) / 2) - collisionSafeEdgesY;
	
	if mouseOver(fcX1,fcY1,fcX2,fcY2) && (mouse_check_button_pressed(mb_left)) {
		
		if(doingProcessingJob[0] == false) { //Not Doing Anything
			
			doingProcessingJob[0] = true;
			doingProcessingJob[1] = 10;
		
		}
		else
		{
			
			if(doingProcessingJob[1] == 10) { //Turning On And Off
				
				doingProcessingJob[0] = false;
				
			} else { //Switching From Another Job
				
				doingProcessingJob[0] = 1;
				doingProcessingJob[1] = 10;
				
			}
			
		}
		
	}
	
}

{ //Drawing Input Slot
	
	var iX1, iX2, iY1, iY2;
	iX1 = sawX - clickBoxXOffset;
	iX2 = iX1 + clickBoxSize;
	iY1 = sawY - (clickBoxSize / 2);
	iY2 = sawY + (clickBoxSize / 2);
	
	{ //Drawing Background
		draw_set_color(globals.inventorySlotBackgroundColor);
		draw_set_alpha(1);
		draw_rectangle(iX1,iY1,iX2,iY2,false);
		draw_set_color(c_white);
		draw_rectangle(iX1,iY1,iX2+1,iY2+1,true);
	}
	
	{ //Drawing Item Inside
		
		if(meatCarvingInput[0] != 0) {
		
			draw_sprite_ext(item[meatCarvingInput[0]][2],0,iX1+1,iY1+1,2,2,0,c_white,1);
			
			var tqOffset = 2; //text quantity offset
			
			draw_set_font(fontSmall);
			draw_set_valign(fa_bottom);
			draw_set_halign(fa_right);
			
			draw_text(iX2-tqOffset, iY2-tqOffset+5, string(meatCarvingInput[1]));
			
			
		}
		
	}
	
	{ //Mouse Over
		
		if mouseOver(iX1,iY1,iX2,iY2) { 
			drawDescription = true;
			itemDescription[0] = meatCarvingInput[0];
			itemDescription[1] = meatCarvingInput[1];
			
			//Click To Take Out
			if(mouse_check_button_pressed(mb_left)) { 
				meatCarvingInput[0] = 0;
				meatCarvingInput[1] = 0; 
			}
		}
		
	}
}

{ //Drawing Output Slot
	
	var oX1, oX2, oY1, oY2;
	oX1 = sawX + clickBoxXOffset - clickBoxSize;
	oX2 = oX1 + clickBoxSize;
	oY1 = sawY - (clickBoxSize / 2);
	oY2 = sawY + (clickBoxSize / 2);
	
	{ //Drawing Background
		draw_set_color(globals.inventorySlotBackgroundColor);
		draw_set_alpha(1);
		draw_rectangle(oX1,oY1,oX2,oY2,false);
		draw_set_color(c_white);
		draw_rectangle(oX1,oY1,oX2+1,oY2+1,true);
	}
	
	{ //Drawing Item Inside
		
		if(meatCarvingOutput[0] != 0) {
			
			var alpha = 1;
			if(meatCarvingOutput[1] == 0) alpha = .5;
			
			draw_sprite_ext(item[meatCarvingOutput[0]][2],0,oX1+1,oY1+1,2,2,0,c_white,alpha);
			
			var tqOffset = 2; //text quantity offset
			
			draw_set_font(fontSmall);
			draw_set_valign(fa_bottom);
			draw_set_halign(fa_right);
			
			if(meatCarvingOutput[1] > 0) draw_text(oX2-tqOffset, oY2-tqOffset+5, string(meatCarvingOutput[1]));
			
			
		}
		
		if(meatCarvingOutput[1] == 0 && meatCarvingInput[0] == 0) {
			
			meatCarvingOutput[0] = 0;
			
		}
		
	}
	
	{ //Mouse Over
		
		if mouseOver(oX1,oY1,oX2,oY2) { 
			drawDescription = true;
			itemDescription[0] = meatCarvingOutput[0];
			itemDescription[1] = meatCarvingOutput[1];
			
			{ //Clicking
				
				if(mouse_check_button_pressed(mb_left)) {
					inventoryAddItem(meatCarvingOutput[0],meatCarvingOutput[1]);
					meatCarvingOutput[1] = 0;
				}
			
			}
		}
		
	}

}

{ //Drawing Fuel
	
	var fX1, fX2, fY1, fY2;
	fX1 = sawX - (clickBoxSize/2);
	fX2 = fX1 + clickBoxSize;
	fY1 = sawY + clickBoxYOffset;
	fY2 = fY1 + clickBoxSize;
	
	{ //Drawing Background
		draw_set_color(globals.inventorySlotBackgroundColor);
		draw_set_alpha(1);
		draw_rectangle(fX1,fY1,fX2,fY2,false);
		draw_set_color(c_white);
		draw_rectangle(fX1,fY1,fX2+1,fY2+1,true);
	}
	
	{ //Drawing Item Inside
		
		if(meatCarvingFuel != 0) {
		
			draw_sprite_ext(item[10001][2],0,fX1+1,fY1+1,2,2,0,c_white,1);
			
			var tqOffset = 2; //text quantity offset
			
			draw_set_font(fontSmall);
			draw_set_valign(fa_bottom);
			draw_set_halign(fa_right);
			
			draw_text(fX2-tqOffset, fY2-tqOffset+5, string(meatCarvingFuel));
			
			
		}
		else 
		{
		
			draw_sprite_ext(item[10001][2],0,fX1+1,fY1+1,2,2,0,c_white,.5);
			
		}
		
	}
	
	{ //Mouse Over
		
		if mouseOver(fX1,fY1,fX2,fY2) {
			drawDescription = true;
			itemDescription[0] = 10001;
			itemDescription[1] = meatCarvingFuel;
		}
		
	}
	
}

{ //Drawing Ore Selection
	
	{ //How many ores?
		var ore;
		var someOre = false;
		var numberOfOre = 0;
		
		for(i = 0; i < inventorySlots; i++;) { //Counting Ores
			if(inventory[i][0] >= 135 && inventory[i][0] <= 201) {
				
				var alreadyHave = false;
				
				for(check = 0; check < numberOfOre; check++) {
					
					if(ore[check][0] == inventory[i][0]) { //Already Have
						
						ore[check][1] += inventory[i][1];
						alreadyHave = true;
						
					}
					
				}
				
				if(alreadyHave == false) {
					ore[numberOfOre][0] = inventory[i][0];
					ore[numberOfOre][1] = inventory[i][1];
					someOre = true;
					numberOfOre += 1;
				}
				
			}
		}
		
		if someOre { var amountOfOre = array_length(ore); }
	}
	
	{ //Drawing Selection
		
		if someOre {
			for(i = 0; i < amountOfOre; i++) {
				
				var tX1,tY1,yPop,itemNumber;
				tX1 = osX + ((i - (numberOfOre/2)) * osSpacing)- ((32 * osScale) / 2);
				tY1 = osY;
				yPop = 0;
				itemNumber = ore[i][0];
				
				{ //Mouse Over
					
					var mouseX2,mouseY2;
					mouseY2 = tY1 + 32 * osScale;
					mouseX2 = tX1 + osSpacing;
					
					if(i == amountOfOre - 1) mouseX2 = tX1 + (32 * osScale);
					
					if mouseOver(tX1,tY1,mouseX2,mouseY2) { 
						yPop = osPopHeight; 
						
						drawDescription = true;
						itemDescription[0] = ore[i][0];
						itemDescription[1] = ore[i][1];
						descriptionX = tX1 - (itemDescriptionWidth / 2) + (16 * osScale);
						descriptionY = tY1 - yPop - 4;
						
						{ //Clicking
							
							if(mouse_check_button_pressed(mb_left)) {
								
								meatCarvingInput[0] = ore[i][0];
								meatCarvingInput[1] = ore[i][1];
								
								//Switching Output
								if(meatCarvingOutput[0] != meatCarvingInput[0] + 201) {
									inventoryAddItem(meatCarvingOutput[0],meatCarvingOutput[1]);
									
									meatCarvingOutput[0] = meatCarvingInput[0] + 268;
									meatCarvingOutput[1] = 0;
								}
								
							}
							
						}
						
					}
				}
				
				draw_sprite_ext(item[itemNumber][2],0,tX1,tY1 - yPop,osScale,osScale,0,c_white,1);
				//draw_sprite_ext(item_oreOutline,0,tX1,tY1 - yPop,osScale,osScale,0,qualityColor[item[itemNumber][4]],.75);
			
			}
		}
	}
}

{ //Drawing Completion Bar
	
	draw_healthbar(procX1,procY1,procX2,procY2,(meatCarvingTime[0] / meatCarvingTime[1]) * 100, globals.jobIconCompBackgroundColor,globals.jobIconCompMinColor,globals.jobIconCompMaxColor,0,true,false);
	
}

{ //Drawing Item Description
	
	if(drawDescription == true) && (itemDescription[0] != 0) drawItemDescriptionIDPosition(itemDescription[0],itemDescription[1],descriptionX,descriptionY);
	
}