plotSize = 64;
scale = plotSize / 32;

xOffset = craftingWingWidth;

mouseHolding = 0; //Empty

depth = 100;

mouseTriggerSpace = 32;
moveSpeed = 15;

maxWidth = (-plotSize * farmingSectionSize * farmingSectionsPerRow) + screenW + xOffset;
maxHeight = (-plotSize * farmingSectionSize * floor(farmingNumberOfSections / farmingSectionsPerRow)) + screenH;



//Buttons
buttonScale = 2;

hoeX = craftingWingWidth;
hoeY = (32 * buttonScale);

wateringCanX = hoeX;
wateringCanY = hoeY + (32 * buttonScale);


//Seed Select
showingSeeds = true;