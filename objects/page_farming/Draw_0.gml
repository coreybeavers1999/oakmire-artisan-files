{ //Drawing Plots
	
	
	for(section = 0; section < farmingNumberOfSections; section++) {
	
		if(farmingSectionOwned[section] == true) {
	
			for(X = 0; X < farmingSectionSize; X++) { //Own It
			
				for(Y = 0; Y < farmingSectionSize; Y++) {
					var sprite, xPos, yPos, rowNumber, plantSprite, plantImage; 
					sprite = farmingPlotSprite[farmingPlot[section][X][Y][0]];
					plantSprite = farmingPlantSprite[farmingPlot[section][X][Y][1]];
					plantImage = floor((farmingPlot[section][X][Y][2] / farmingPlot[section][X][Y][3]) * 3);
				
					rowNumber = floor(section / farmingSectionsPerRow);
					xPos = x + xOffset + plotSize * X + (plotSize * farmingSectionSize * (section - (floor(rowNumber) * farmingSectionsPerRow)));
					yPos = y + plotSize * Y + (plotSize * farmingSectionSize * rowNumber);
				
					draw_sprite_ext(sprite,0,xPos,yPos,scale,scale,0,c_white,1);
					draw_sprite_ext(plantSprite,plantImage,xPos,yPos,scale,scale,0,c_white,1);
				
					{ //Mouse Over
					
						if mouseOver(xPos,yPos,xPos + plotSize,yPos + plotSize) {
							draw_sprite_ext(spr_farming_texture_mouseOver,0,xPos,yPos,scale,scale,0,c_white,1);
							
							{ //Clicking
								if mouse_check_button_pressed(mb_left) {
									
									clickingPlot(section,X,Y);
									
								}
							}
					
						}
					
					}
			
				}
		
			}
		}
		
		else { //Dont Own It
			
			var x1, x2, y1, y2, rowNumber, color;
			color = c_black;
			rowNumber = floor(section / farmingSectionsPerRow);
			x1 = x + xOffset + (plotSize * farmingSectionSize * (section - (floor(rowNumber) * farmingSectionsPerRow)));
			x2 = x1 + (plotSize * farmingSectionSize);
			y1 = y + (plotSize * farmingSectionSize * rowNumber);
			y2 = y1 + (plotSize * farmingSectionSize);
			
			draw_set_color(c_white);
			draw_set_alpha(1);
			draw_set_halign(fa_center);
			draw_set_valign(fa_middle);
							
			draw_text(x1 + ((x2 - x1) / 2), y1 + ((y2 - y1) / 2),"Locked");
			
			{ //Buying new land
				
				if mouseOver(x1,y1,x2,y2) {
					
					color = c_dkgray;
					
					if(mouse_check_button_pressed(mb_left)) {
						
						farmingSectionOwned[section] = true;
						
					}
					
				}
				
			}
			
			draw_set_alpha(.5);
			draw_set_color(color);
			draw_rectangle(x1,y1,x2,y2,false);
			
		}
	}
	
}

{ //Moving Around The Map
	
	var movingX, movingY;
	movingX = 0;
	movingY = 0;
	
	if(mouse_x <= mouseTriggerSpace) { movingX = -1; } else if(mouse_x >= screenW - mouseTriggerSpace) { movingX = 1; }
	if(mouse_y <= mouseTriggerSpace) { movingY = -1; } else if(mouse_y >= screenH - mouseTriggerSpace) { movingY = 1; }
	
	x -= movingX * moveSpeed;
	y -= movingY * moveSpeed;
	
	if(x > 0) x = 0;
	if(y > 0) y = 0;

	

	if(x < maxWidth) x = maxWidth;
	if(y < maxHeight) y = maxHeight;
	
}

{ //Buttons On Screen
	
	{ //Hoe
		
		if mouseOver(hoeX,hoeY,hoeX + (32 * buttonScale),hoeY + (32 * buttonScale)) {

				if(mouse_check_button_pressed(mb_left)) {
					
					if(mouseHolding == 1) { mouseHolding = 0; } else { mouseHolding = 1; }
					
				}
			
		}
		
		var hoeActive = false;
		if(mouseHolding == 1) hoeActive = true; 
		
		draw_sprite_ext(spr_farming_hoeButton,hoeActive,hoeX,hoeY,buttonScale,buttonScale,0,c_white,1);
		
	}
		
	{ //Watering Can
		
		if mouseOver(wateringCanX,wateringCanY,wateringCanX + (32 * buttonScale),wateringCanY + (32 * buttonScale)) {
			
				if(mouse_check_button_pressed(mb_left)) {
					
					if(mouseHolding == 2) { mouseHolding = 0; } else { mouseHolding = 2; }
					
				}
			
		}
		
		var wateringCanActive = false;
		if(mouseHolding == 2) wateringCanActive = true; 
		
		draw_sprite_ext(spr_farming_wateringCan,wateringCanActive,wateringCanX,wateringCanY,buttonScale,buttonScale,0,c_white,1);
		
	}
	
}

{ //Drawing Seeds
	
	var checking = drawItemSelection(537,603);
		
	if(mouse_check_button_pressed(mb_left) && checking != 0) mouseHolding = checking;
	
}

function clickingPlot(section,X,Y) {
	
	{ //Harvesting Plants
		
		if(farmingPlot[section][X][Y][2] == farmingPlot[section][X][Y][3] && farmingPlot[section][X][Y][1] != 0) {
			
				inventoryAddItem(farmingSeed[farmingPlot[section][X][Y][1]][1],1);
				inventoryAddItem(farmingPlot[section][X][Y][1],1);
				
				farmingPlot[section][X][Y][0] = 1;
				farmingPlot[section][X][Y][1] = 0;
				farmingPlot[section][X][Y][2] = 0;
				farmingPlot[section][X][Y][3] = 0;
				
				xp[11] += 1;
			
		}
		
	}
	
	{ //Using Tools
		if(mouseHolding == 1 && farmingPlot[section][X][Y][1] == 0) farmingPlot[section][X][Y][0] = 1;
		
		if(mouseHolding == 2 && farmingPlot[section][X][Y][0] == 1 && farmingPlot[section][X][Y][1] == 0) farmingPlot[section][X][Y][0] = 2;
		
	}
	
	{ //Planting
		
		if farmingPlot[section][X][Y][0] == 2 && farmingPlot[section][X][Y][1] == 0 && (mouseHolding >= 537 && mouseHolding <= 603) && itemInInventory(mouseHolding,1) { //Planting and holding seed
		
			farmingPlot[section][X][Y][1] = mouseHolding;
			farmingPlot[section][X][Y][2] = 0;
			farmingPlot[section][X][Y][3] = farmingSeed[mouseHolding][0];
			removeFromInventory(mouseHolding,1);
			
			if !itemInInventory(mouseHolding,1) mouseHolding = 0;
			
		}
		
	}
	
}