procHeight = 16;
procBuffer = 32;
procX1 = craftingWingWidth + procBuffer;
procX2 = screenW - procBuffer;
procY1 = procBuffer/2;
procY2 = procY1 + procHeight;

slotScale = 2;
slotSize = 32 * slotScale;


//Crafting List
craftingListWidth = 200;
craftingListHeight = 32;

craftingListX1 = screenW - craftingListWidth - procBuffer;
craftingListY1 = procY2 + (procBuffer / 2);

craftingListSpacing = 8;

craftingListBackgroundAlpha = .5;
craftingListBackgroundColor = c_black;
craftingListTextColor = c_white;
craftingListBackgroundColorMouse = c_gray;
craftingListColorActive = c_green;

craftingListTextYBuffer = craftingListHeight / 2;
craftingListTextXBuffer = 4;


//Activate Button
activateX1 = procX1;
activateX2 = activateX1 + craftingListWidth;
activateY1 = craftingListY1;
activateY2 = activateY1 + craftingListHeight;

activeMidX = activateX1 + (craftingListWidth / 2);
activeMidY = activateY1 + (craftingListHeight / 2);

//Input|Output
inputOutputDistance = 400;
inputOutputY = (screenH / 2) - 150;
inputOutputCenter = (screenW / 2) + (craftingWingWidth / 2) - ((screenW - craftingListX1) / 2);

inputSlotX1 = inputOutputCenter - (inputOutputDistance / 2) - (slotSize / 2);
inputSlotY1 = inputOutputY;
inputSlotX2 = inputSlotX1 + slotSize;
inputSlotY2 = inputSlotY1 + slotSize;

outputSlotX1 = inputOutputCenter + (inputOutputDistance / 2) - (slotSize / 2);
outputSlotY1 = inputOutputY;
outputSlotX2 = outputSlotX1 + slotSize;
outputSlotY2 = outputSlotY1 + slotSize;

requiredBarX = inputOutputCenter;
requiredBarY = inputOutputY + (slotSize / 2);
