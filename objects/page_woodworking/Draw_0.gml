var drawDescription = false;
var drawDescriptionItem = 0;
var drawDescriptionAmount = 0;


{ //Draw Input
	
	{ //Drawing Background
		
		if(woodworkingInput[0] != 0) {
			
			draw_set_color(qualityColor[item[woodworkingInput[0]][4]]);
			
		} else {
			draw_set_color(globals.inventorySlotBackgroundColor);
		}
		
		draw_set_alpha(globals.qualityBackgroundAlpha);
		draw_rectangle(inputSlotX1,inputSlotY1,inputSlotX2,inputSlotY2,false);
		draw_set_color(globals.defaultTextColor);
		draw_rectangle(inputSlotX1,inputSlotY1,inputSlotX2-1,inputSlotY2-1,true);
		
	}
	
	{ //Draw Item
		
		if(woodworkingInput[1] > 0 && woodworkingInput[0] != 0) {
			draw_sprite_ext(item[woodworkingInput[0]][2],0,inputSlotX1,inputSlotY1,slotScale,slotScale,0,c_white,1);
			
			var tqOffset = 2; //text quantity offset
				
			draw_set_font(fontSmall);
			draw_set_valign(fa_bottom);
			draw_set_halign(fa_right);
			draw_set_color(globals.defaultTextColor);
			
			draw_text(inputSlotX2-tqOffset, inputSlotY2-tqOffset+5, string(woodworkingInput[1]));
		
		} else {
			
			woodworkingInput[0] = 0;
			woodworkingInput[1] = 0;
			
		}
		
	}
	
	{ //Draw Amount Needed
		
		draw_set_halign(fa_center);
		draw_set_valign(fa_middle);
		draw_text(requiredBarX,requiredBarY,string(woodworkingIndex[woodworkingSelectedItem][2]) + " Planks Required");
		
	}
	
}

{ //Draw Output
	
	{ //Determine Output
		
		if(woodworkingInput[0] != 0) { 
			
			woodworkingOutput[0] = (woodworkingInput[0] + 2665) + (67 * woodworkingSelectedItem);
			
		} else if(woodworkingOutput[1] == 0) { woodworkingOutput[0] = 0; }
		
	}
	
	{ //Drawing Background
		
		if(woodworkingOutput[0] != 0) {
			
			draw_set_color(qualityColor[item[woodworkingOutput[0]][4]]);
			
		} else {
			draw_set_color(globals.inventorySlotBackgroundColor);
		}
		
		draw_set_alpha(globals.qualityBackgroundAlpha);
		draw_rectangle(outputSlotX1,outputSlotY1,outputSlotX2,outputSlotY2,false);
		draw_set_color(globals.defaultBoxOutlineColor);	
		draw_rectangle(outputSlotX1,outputSlotY1,outputSlotX2-1,outputSlotY2-1,true);
		
	}
	
	{ //Draw Item
		
		var tqOffset = 2; //text quantity offset
		var itemAlpha = 1;
		
		if(woodworkingOutput[0] != 0 && woodworkingOutput[1] != 0) {
			draw_set_color(globals.defaultTextColor);
			draw_set_font(fontSmall);
			draw_set_valign(fa_bottom);
			draw_set_halign(fa_right);
				
			draw_text(outputSlotX2-tqOffset, outputSlotY2-tqOffset+5, string(woodworkingOutput[1]));
		}
		else
		{
			itemAlpha = .5;
		}
		
		draw_sprite_ext(item[woodworkingOutput[0]][2],0,outputSlotX1,outputSlotY1,slotScale,slotScale,0,c_white,itemAlpha);
	}
	
	{ //Clicking
		
		if mouseOver(outputSlotX1,outputSlotY1,outputSlotX2,outputSlotY2) && mouse_check_button_pressed(mb_left) {
			
			inventoryAddItem(woodworkingOutput[0],woodworkingOutput[1]);
			woodworkingOutput[0] = 0;
			woodworkingOutput[1] = 0;
			
		}
		
	}
	
}

{ //Drawing Completion Bar
	
	draw_set_alpha(1);
	draw_healthbar(procX1,procY1,procX2,procY2,(woodworkingTime[0] / woodworkingTime[1]) * 100, globals.jobIconCompBackgroundColor,globals.jobIconCompMinColor,globals.jobIconCompMaxColor,0,true,false);
	
}

{ //Drawing Activate Button
	
	if(doingCraftingJob[0] == false or doingCraftingJob[1] != 14) { 
		draw_set_color(craftingListBackgroundColor);
	} else {
		draw_set_color(globals.defaultBoxActiveColor);
	}
	
	draw_set_alpha(craftingListBackgroundAlpha);
	draw_rectangle(activateX1,activateY1,activateX2,activateY2,false);
	draw_set_alpha(1);
	draw_set_color(globals.defaultTextColor);
	draw_set_halign(fa_middle);
	draw_set_valign(fa_center);
	draw_text(activeMidX,activeMidY,"Activate Skill");
	
	if mouseOver(activateX1,activateY1,activateX2,activateY2) && mouse_check_button_pressed(mb_left) {
	
		if(doingCraftingJob[1] == 14 && doingCraftingJob[0] == true) {
			
			doingCraftingJob[0] = false;
			
		} else {
			
			doingCraftingJob[0] = true;
			doingCraftingJob[1] = 14;
			
		}
		
	}
	
}

{ //Item Selection
	
	var selectedBar = drawItemSelection(336,402);
	
	if(mouse_check_button_pressed(mb_left) && selectedBar != 0) {
		
		woodworkingTime[0] = 0;
		
		if(woodworkingOutput[1] != 0) { //Deposit current output
			
			inventoryAddItem(woodworkingOutput[0],woodworkingOutput[1]);
			woodworkingOutput[0] = 0;
			woodworkingOutput[1] = 0;
			
		}
		
		
		if(selectedBar == woodworkingInput[0]) {
			
			woodworkingInput[0] = 0;
			
		} else {
			
			woodworkingInput[0] = selectedBar;
			
		}
		
	}
	
}
	
{ //Draw Item Description
	
	if mouseOver(inputSlotX1,inputSlotY1,inputSlotX1 + slotSize,inputSlotY1 + slotSize) {
		
		drawDescription = true;
		drawDescriptionItem = woodworkingInput[0];
		drawDescriptionAmount = woodworkingInput[1];
		
	}
	
	if mouseOver(outputSlotX1,outputSlotY1,outputSlotX1 + slotSize,outputSlotY1 + slotSize) {
		
		drawDescription = true;
		drawDescriptionItem = woodworkingOutput[0];
		drawDescriptionAmount = woodworkingOutput[1];
		
	}
	
	if(drawDescription && drawDescriptionItem != 0) drawItemDescriptionID(drawDescriptionItem,drawDescriptionAmount);
	
}
	
{ //Draw Crafting List
	
	var numberOfCrafting = array_length(woodworkingIndex);
	
	for(selectedSpot = 0; selectedSpot < numberOfCrafting; selectedSpot++) {
		
		var mouseIsOver = false;
		var craftingItemLocked = false;
		
		if(level[14] < woodworkingIndex[selectedSpot][1]) { craftingItemLocked = true; }
		
		//Background
		var backgroundX1,backgroundX2,backgroundY1,backgroundY2;
		backgroundX1 = craftingListX1;
		backgroundY1 = craftingListY1 + (selectedSpot * (craftingListHeight + craftingListSpacing));
		backgroundX2 = backgroundX1 + craftingListWidth;
		backgroundY2 = backgroundY1 + craftingListHeight;
		
		draw_set_color(craftingListBackgroundColor);
		draw_set_alpha(craftingListBackgroundAlpha);
		
		//Selected so draw background selected color
		if(woodworkingSelectedItem == selectedSpot) draw_set_color(craftingListColorActive);
		
		//Mouse Over
		if mouseOver(backgroundX1,backgroundY1,backgroundX2,backgroundY2) { 
			mouseIsOver = true;
			draw_set_color(craftingListBackgroundColorMouse);
			
			
			
			if mouse_check_button_pressed(mb_left) && craftingItemLocked == false {
				
				woodworkingSelectedItem = selectedSpot;
				
				if(woodworkingOutput[1] > 0) inventoryAddItem(woodworkingOutput[0],woodworkingOutput[1]);
				
				woodworkingOutput[0] = 0;
				woodworkingOutput[1] = 0;
				woodworkingInput[0] = 0;
				woodworkingInput[0] = 0;
				
				woodworkingTime[0] = 0;
				
			}
		}

		draw_rectangle(backgroundX1,backgroundY1,backgroundX2,backgroundY2,false);
		
		
		
		draw_set_alpha(1);
		
		//Text
		draw_set_halign(fa_left);
		draw_set_valign(fa_middle);
		draw_set_color(craftingListTextColor);
		
		var textX, textY;
		textX = backgroundX1 + craftingListTextXBuffer;
		textY = backgroundY1 + craftingListTextYBuffer;
		
		if craftingItemLocked {
			draw_text(textX,textY,"Unlock At Level " + string(woodworkingIndex[selectedSpot][1]));
		} else {
			draw_text(textX,textY,woodworkingIndex[selectedSpot][0]);
		}
		
	}
	
}