itemID = 0;
amount = 0;

image_xscale = 2;
image_yscale = 2;

depth = -instance_number(object);

x = screenW - 64;
y = screenH + 64;

moveToStart = true;
moveToTop = false;

moveToStartTimeMax = 30;
moveToStartTime = moveToStartTimeMax;

pauseTime = 30;

moveToTopTimeMax = 60;
moveToTopTime = moveToTopTimeMax;

Speed = 0;
moveToStartSpeed = 10;
moveToTopSpeed = 40;

waitLimit = (instance_number(object) - 1) * 30;

if(room == Inventory) instance_destroy();

/*
startingPos = screenH - 155;
upAmount = 0;

lifeTimeMax = 90;
lifeTime = lifeTimeMax;

textXOffset = 20;
textYOffset = 32 * 1;

positionAmount = 0;
positionAmountOffset = 56;

moveUpSpeed = 1;

y = startingPos;
*/