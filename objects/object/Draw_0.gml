sprite_index = item[itemID][2];
image_index = 0;

if(waitLimit <= 0) {

	if moveToStart {
	
		Speed = (moveToStartTime / moveToStartTimeMax) * moveToStartSpeed;
	
		if(moveToStartTime > 0) { moveToStartTime -= 1; } else { moveToStart = false; }
	
	}

	if !moveToStart && !moveToTop {
	
		Speed = 1;
	
		if(pauseTime > 0) { pauseTime -= 1; } else { moveToTop = true; }
	
	}

	if moveToTop {
	
		Speed = ((moveToTopTimeMax - moveToTopTime) / moveToTopTimeMax) * moveToTopSpeed;
	
		if(moveToTopTime > 0) { moveToTopTime -= 1; } else { instance_destroy(); }
	
	}

	y -= Speed;

}
else
{
	
	waitLimit -= 1;
	
	if(room == Inventory) instance_destroy();
	
}

if(instance_count > maxItemObjects) instance_destroy();


/*
lifeTime -= 1;

var percent = (lifeTime / lifeTimeMax);
if(percent > .3) { percent = 1; }

moveUpSpeed = (lifeTimeMax - lifeTime) * (1 - percent);
y -= moveUpSpeed;

if(place_meeting(x,y,object)) positionAmount += positionAmountOffset;

//y = startingPos + (upAmount * percent) - positionAmount;


image_alpha = percent;

//Draw Amount Added
draw_set_color(c_white);
draw_set_alpha(percent);
draw_set_font(fontMedium);
draw_set_valign(fa_middle);
draw_set_halign(fa_left);
draw_text(x - textXOffset, y + textYOffset, "+" + string(amount));


if(lifeTime <= 0) instance_destroy();
*/
draw_self();