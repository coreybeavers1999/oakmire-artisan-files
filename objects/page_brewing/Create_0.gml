procHeight = 16;
procBuffer = 32;
procX1 = craftingWingWidth + procBuffer;
procX2 = screenW - procBuffer;
procY1 = procBuffer/2;
procY2 = procY1 + procHeight;

slotScale = 2;
slotSize = 32 * slotScale;

//Crafting List
craftingListWidth = 300;
craftingListHeight = 32;

craftingListX1 = screenW - craftingListWidth - procBuffer;
craftingListY1 = procY2 + (procBuffer / 2);

craftingListSpacing = 8;

craftingListBackgroundAlpha = .5;
craftingListBackgroundColor = c_black;
craftingListTextColor = c_white;
craftingListBackgroundColorMouse = c_gray;
craftingListColorActive = c_green;

craftingListTextYBuffer = craftingListHeight / 2;
craftingListTextXBuffer = 4;


//Activate Button
activateX1 = procX1;
activateX2 = activateX1 + completionBarW;
activateY1 = craftingListY1;
activateY2 = activateY1 + completionBarH;

activeMidX = activateX1 + (completionBarW / 2);
activeMidY = activateY1 + (completionBarH / 2);


//Output/Input
centerX = (screenW / 2) + (craftingWingWidth / 2) - ((screenW - craftingListX1) / 2);
inputSlotSeperation = 150;
inputSlotDrop = 64;
outputSlotDrop = 256;
requiredAmountOffset = 16;

//Middle Slot
iS2X1 = centerX - (slotSize / 2);
iS2Y1 = screenH/2 - 256;
iS2X2 = iS2X1 + slotSize;
iS2Y2 = iS2Y1 + slotSize;
it2X = iS2X1 + (slotSize / 2);
it2Y = iS2Y2 + requiredAmountOffset;


//Left Slot
iS1X1 = centerX - (slotSize / 2) - inputSlotSeperation;
iS1Y1 = iS2Y1 + inputSlotDrop;
iS1X2 = iS1X1 + slotSize;
iS1Y2 = iS1Y1 + slotSize;
it1X = iS1X2 + requiredAmountOffset;
it1Y = iS1Y1 + (slotSize / 2);

//Right Slot
iS3X1 = centerX - (slotSize / 2) + inputSlotSeperation;
iS3Y1 = iS2Y1 + inputSlotDrop;
iS3X2 = iS3X1 + slotSize;
iS3Y2 = iS3Y1 + slotSize;
it3X = iS3X1 - requiredAmountOffset;
it3Y = iS3Y1 + (slotSize / 2);

//Output
osX1 = iS2X1;
osY1 = iS2Y1 + outputSlotDrop;
osX2 = iS2X2;
osY2 = osY1 + slotSize;
textBuffer = 4;