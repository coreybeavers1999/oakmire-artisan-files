function setFontInputAmount() {
	
	draw_set_font(fontTiny);
	draw_set_color(globals.defaultTextColor);
	draw_set_alpha(1);
	draw_set_halign(fa_middle);
	draw_set_valign(fa_center);
	
}

function updateInputs() {
	
	{ //Entering In Input ID's
		
		var currentLetter = 0;
		var inputOne, inputTwo, inputThree;
		inputOne[0] = "";
		inputOne[1] = "";
		inputTwo[0] = "";
		inputTwo[1] = "";
		inputThree[0] = "";
		inputThree[1] = "";
		
		for(lc1 = currentLetter; lc1 < 100; lc1++) { //Input One
			if(string_char_at(brewingRecipe[brewingSelectedItem][2],lc1+1) == ",") { //Moving To Next
				inputOne[0] = int64(inputOne[0]);
				currentLetter = lc1+1;
				break;
			} else {
				inputOne[0] += string_char_at(brewingRecipe[brewingSelectedItem][2],lc1+1);
			}
		}
		
		for(lc2 = currentLetter; lc2 < 100; lc2++) { //Input Two
			if(string_char_at(brewingRecipe[brewingSelectedItem][2],lc2+1) == ",") { //Moving To Next
				inputTwo[0] = int64(inputTwo[0]);
				currentLetter = lc2+1;
				break;
			} else {
				inputTwo[0] += string_char_at(brewingRecipe[brewingSelectedItem][2],lc2+1);
			}
		}
		
		for(lc3 = currentLetter; lc3 < 100; lc3++) { //Input Three
			if(string_char_at(brewingRecipe[brewingSelectedItem][2],lc3+1) == "/") { //Moving To Next
				inputThree[0] = int64(inputThree[0]);
				currentLetter = lc3+1;
				break;
			} else {
				inputThree[0] += string_char_at(brewingRecipe[brewingSelectedItem][2],lc3+1);
			}
		}
		
		for(lc = currentLetter; lc < 100; lc++) { //Input Amount One
			if(string_char_at(brewingRecipe[brewingSelectedItem][2],lc+1) == ",") { //Moving To Next
				inputOne[1] = int64(inputOne[1]);
				currentLetter = lc+1;
				break;
			} else {
				inputOne[1] += string_char_at(brewingRecipe[brewingSelectedItem][2],lc+1);
			}
		}
		
		for(lc = currentLetter; lc < 100; lc++) { //Input Amount Two
			if(string_char_at(brewingRecipe[brewingSelectedItem][2],lc+1) == ",") { //Moving To Next
				inputTwo[1] = int64(inputTwo[1]);
				currentLetter = lc+1;
				break;
			} else {
				inputTwo[1] += string_char_at(brewingRecipe[brewingSelectedItem][2],lc+1);
			}
		}
		
		for(lc = currentLetter; lc < 100; lc++) { //Input Amount Three
			if(string_char_at(brewingRecipe[brewingSelectedItem][2],lc+1) == "/") { //Moving To Next
				inputThree[1] = int64(inputThree[1]);
				currentLetter = lc+1;
				break;
			} else {
				inputThree[1] += string_char_at(brewingRecipe[brewingSelectedItem][2],lc+1);
			}
		}
		
		brewingInputOne[0] = inputOne[0];
		brewingInputOne[1] = inputOne[1];
		
		brewingInputTwo[0] = inputTwo[0];
		brewingInputTwo[1] = inputTwo[1];
		
		brewingInputThree[0] = inputThree[0];
		brewingInputThree[1] = inputThree[1];
		
		show_debug_message("/////////////////////");
		
		show_debug_message(brewingInputOne[0]);
		show_debug_message(brewingInputOne[1]);
		
		show_debug_message(brewingInputTwo[0]);
		show_debug_message(brewingInputTwo[1]);
		
		show_debug_message(brewingInputThree[0]);
		show_debug_message(brewingInputThree[1]);
		
		show_debug_message("/////////////////////");
		
	}
	
}

var drawDescription = false;
var drawDescriptionItem = 0;

{ //Input
	
	{ //Center
		drawSlot(iS2X1,iS2Y1,globals.inventorySlotBackgroundColor);
		
		//Draw Item
		if(brewingInputTwo[0] != 0) {
		
			var inputTwoAlpha = 1;
			if(!brewingEnough[2]) inputTwoAlpha = .5;
			
			draw_sprite_ext(item[brewingInputTwo[0]][2],0,iS2X1,iS2Y1,slotScale,slotScale,0,c_white,inputTwoAlpha);
		}
		
		if(brewingInputTwo[1] != 0) {
			setFontInputAmount();
			draw_text(it2X,it2Y,string(brewingInputTwo[1]));
			draw_set_font(fontSmall);
		}
		
		//Mouse Over
		if mouseOver(iS2X1,iS2Y1,iS2X2,iS2Y2) && (brewingInputTwo[0] != 0) {
			
			drawDescription = true;
			drawDescriptionItem = brewingInputTwo[0];
			
		}
		
	}
	
	{ //Left
		drawSlot(iS1X1,iS1Y1,globals.inventorySlotBackgroundColor);
		
		//Draw Item
		if(brewingInputOne[0] != 0) {
		
			var inputOneAlpha = 1;
			if(!brewingEnough[1]) inputOneAlpha = .5;
			
			draw_sprite_ext(item[brewingInputOne[0]][2],0,iS1X1,iS1Y1,slotScale,slotScale,0,c_white,inputOneAlpha);
		}
		
		if(brewingInputOne[1] != 0) {
			setFontInputAmount();
			draw_text(it1X,it1Y,string(brewingInputOne[1]));
			draw_set_font(fontSmall);
		}
		
		//Mouse Over
		if mouseOver(iS1X1,iS1Y1,iS1X2,iS1Y2) && (brewingInputOne[0] != 0) {
			
			drawDescription = true;
			drawDescriptionItem = brewingInputOne[0];
			
		}
		
	}
	
	{ //Right
		drawSlot(iS3X1,iS3Y1,globals.inventorySlotBackgroundColor);
		
		//Draw Item
		if(brewingInputThree[0] != 0) {
		
			var inputThreeAlpha = 1;
			if(!brewingEnough[3]) inputThreeAlpha = .5;
			
			draw_sprite_ext(item[brewingInputThree[0]][2],0,iS3X1,iS3Y1,slotScale,slotScale,0,c_white,inputThreeAlpha);
		}
		
		if(brewingInputThree[1] != 0) {
			setFontInputAmount();
			draw_text(it3X,it3Y,string(brewingInputThree[1]));
			draw_set_font(fontSmall);
		}
		
		//Mouse Over
		if mouseOver(iS3X1,iS3Y1,iS3X2,iS3Y2) && (brewingInputThree[0] != 0) {
			
			drawDescription = true;
			drawDescriptionItem = brewingInputThree[0];
			
		}
		
	}
	
}

{ //Output
	drawSlot(osX1,osY1,globals.inventorySlotBackgroundColor);
	
	//Draw Item
	if(brewingOutput[0] != 0) {
		
		var outputAlpha = 1;
		if(brewingOutput[1] == 0) outputAlpha = .5;
		
		draw_sprite_ext(item[brewingOutput[0]][2],0,osX1,osY1,slotScale,slotScale,0,c_white,outputAlpha);
		
		if(brewingOutput[1] > 1) { //Draw Amount
			
			draw_set_color(globals.defaultTextColor);
			draw_set_font(fontTiny);
			draw_set_alpha(1);
			draw_set_halign(fa_right);
			draw_set_valign(fa_bottom);
			draw_text(osX2 - textBuffer, osY2 - textBuffer,string(brewingOutput[1]));
			draw_set_font(fontSmall);
		}
	}
	
	//Mouse Over
	if mouseOver(osX1,osY1,osX2,osY2) && (brewingOutput[0] != 0) {
		
		drawItemDescriptionID(brewingOutput[0],brewingOutput[1]);
		
		if mouse_check_button_pressed(mb_left) {
			
			inventoryAddItem(brewingOutput[0],brewingOutput[1]);
			brewingOutput[1] = 0;
			
		}
		
	}
	
}

{ //Draw Crafting List
	
	var numberOfCrafting = array_length(brewingRecipe);
	
	for(selectedSpot = 0; selectedSpot < numberOfCrafting; selectedSpot++) {
		
		var mouseIsOver = false;
		var craftingItemLocked = false;
		
		if(level[16] < brewingRecipe[selectedSpot][1]) { craftingItemLocked = true; }
		
		//Background
		var backgroundX1,backgroundX2,backgroundY1,backgroundY2;
		backgroundX1 = craftingListX1;
		backgroundY1 = craftingListY1 + (selectedSpot * (craftingListHeight + craftingListSpacing));
		backgroundX2 = backgroundX1 + craftingListWidth;
		backgroundY2 = backgroundY1 + craftingListHeight;
		
		draw_set_color(craftingListBackgroundColor);
		draw_set_alpha(craftingListBackgroundAlpha);
		
		//Selected so draw background selected color
		if(brewingSelectedItem == selectedSpot) draw_set_color(craftingListColorActive);
		
		//Mouse Over
		if mouseOver(backgroundX1,backgroundY1,backgroundX2,backgroundY2) { 
			mouseIsOver = true;
			draw_set_color(craftingListBackgroundColorMouse);
			
			
			
			if mouse_check_button_pressed(mb_left) && craftingItemLocked == false {
				
				brewingSelectedItem = selectedSpot;
				
				if(brewingOutput[1] > 0) inventoryAddItem(brewingOutput[0],brewingOutput[1]);
				
				brewingOutput[0] = brewingRecipe[brewingSelectedItem][3];
				brewingOutput[1] = 0;
				updateInputs();
				
				brewingTime[0] = 0;
				
			}
		}

		draw_rectangle(backgroundX1,backgroundY1,backgroundX2,backgroundY2,false);
		
		
		
		draw_set_alpha(1);
		
		//Text
		draw_set_halign(fa_left);
		draw_set_valign(fa_middle);
		draw_set_color(craftingListTextColor);
		
		var textX, textY;
		textX = backgroundX1 + craftingListTextXBuffer;
		textY = backgroundY1 + craftingListTextYBuffer;
		
		if craftingItemLocked {
			draw_text(textX,textY,"Unlock At Level " + string(brewingRecipe[selectedSpot][1]));
		} else {
			draw_text(textX,textY,brewingRecipe[selectedSpot][0]);
		}
		
	}
	
}

{ //Drawing Completion Bar
	
	draw_set_alpha(1);
	draw_healthbar(procX1,procY1,procX2,procY2,(brewingTime[0] / brewingTime[1]) * 100, globals.jobIconCompBackgroundColor,globals.jobIconCompMinColor,globals.jobIconCompMaxColor,0,true,false);
	
}

{ //Drawing Activate Button
	
	if(doingCraftingJob[0] == false or doingCraftingJob[1] != 16) { 
		draw_set_color(craftingListBackgroundColor);
	} else {
		draw_set_color(globals.defaultBoxActiveColor);
	}
	
	draw_set_alpha(craftingListBackgroundAlpha);
	draw_rectangle(activateX1,activateY1,activateX2,activateY2,false);
	draw_set_alpha(1);
	draw_set_color(globals.defaultTextColor);
	draw_set_halign(fa_middle);
	draw_set_valign(fa_center);
	draw_text(activeMidX,activeMidY,"Activate Skill");
	
	if mouseOver(activateX1,activateY1,activateX2,activateY2) && mouse_check_button_pressed(mb_left) {
	
		if(doingCraftingJob[1] == 16 && doingCraftingJob[0] == true) {
			
			doingCraftingJob[0] = false;
			
		} else {
			
			doingCraftingJob[0] = true;
			doingCraftingJob[1] = 16;
			
		}
		
	}
	
}

{ //Draw Item Description

	if(drawDescription == true) drawItemDescriptionID(drawDescriptionItem,countItem(drawDescriptionItem));

}