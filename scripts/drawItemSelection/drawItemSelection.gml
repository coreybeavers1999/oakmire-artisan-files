function drawItemSelection(smallestID,largestID) {
	
	var drawDescription = false;
	var itemDescription;
	itemDescription[0] = 0;
	itemDescription[1] = 0;
	var descriptionX = mouse_x;
	var descriptionY = mouse_y;
	var returningValue = 0;
	
	var osScale, osX, osY, osSpacing, osPopHeight;
	osScale = 4;
	osX = (screenW + craftingWingWidth) / 2;
	osY = screenH - (32 * osScale) - 64;

	osSpacing = 32;
	osPopHeight = 64;
	
	{ //Drawing Ore Selection
	
	{ //How many ores?
		var ore;
		var someOre = false;
		var numberOfOre = 0;
		
		for(i = 0; i < inventorySlots; i++;) { //Counting Ores
			if(inventory[i][0] >= smallestID && inventory[i][0] <= largestID) {
				
				var alreadyHave = false;
				
				for(check = 0; check < numberOfOre; check++) {
					
					if(ore[check][0] == inventory[i][0]) { //Already Have
						
						ore[check][1] += inventory[i][1];
						alreadyHave = true;
						
					}
					
				}
				
				if(alreadyHave == false) {
					ore[numberOfOre][0] = inventory[i][0];
					ore[numberOfOre][1] = inventory[i][1];
					someOre = true;
					numberOfOre += 1;
				}
				
			}
		}
		
		if someOre { var amountOfOre = array_length(ore); }
	}
	
	{ //Drawing Selection
		
		if someOre {
			for(i = 0; i < amountOfOre; i++) {
				
				var tX1,tY1,yPop,itemNumber;
				tX1 = osX + ((i - (numberOfOre/2)) * osSpacing)- ((32 * osScale) / 2);
				tY1 = osY;
				yPop = 0;
				itemNumber = ore[i][0];
				
				{ //Mouse Over
					
					var mouseX2,mouseY2;
					mouseY2 = tY1 + 32 * osScale;
					mouseX2 = tX1 + osSpacing;
					
					if(i == amountOfOre - 1) mouseX2 = tX1 + (32 * osScale);
					
					if mouseOver(tX1,tY1,mouseX2,mouseY2) { 
						yPop = osPopHeight; 
						
						drawDescription = true;
						itemDescription[0] = ore[i][0];
						itemDescription[1] = ore[i][1];
						descriptionX = tX1 - (itemDescriptionWidth / 2) + (16 * osScale);
						descriptionY = tY1 - yPop - 4;
						
						{ //Clicking
							
							if(mouse_check_button_pressed(mb_left)) {
								
								returningValue = ore[i][0];
								
							}
							
						}
						
					}
				}
				
					draw_sprite_ext(item[itemNumber][2],0,tX1,tY1 - yPop,osScale,osScale,0,c_white,1);
					
					var color = qualityColor[item[itemNumber][4]];
					draw_sprite_ext(item[itemNumber][2],1,tX1,tY1 - yPop,osScale,osScale,0,color,.75); //Draw Outline
			
				}
			}
		}	
	
	}
	
	{ //Drawing Item Description
	
		if(drawDescription == true) && (itemDescription[0] != 0) drawItemDescriptionIDPosition(itemDescription[0],itemDescription[1],descriptionX,descriptionY);
	
	}

	return(returningValue);
}
