function saveGame(){
	
	ini_open("save.ini");
	
	{ //Saving Levels and Inventory
		for(i = 3; i < 3 + numberOfCollectingSkills; i++) { //Save All Collecting Levels
			ini_write_real("Skill Levels", "xp[" + string(i) + "]", xp[i]);
		}
		
		for(i = 8; i < 8 + numberOfProcessingSkills; i++) { //Save All Processing Levels
			ini_write_real("Skill Levels", "xp[" + string(i) + "]", xp[i]);
		}
	
		ini_write_real("Inventory", "inventorySlots", inventorySlots);
		for(i = 0; i < inventorySlots; i++) { //Saving Everything In Inventory
		
			ini_write_real("Inventory", "inventory[" + string(i) + "][0]", inventory[i][0]);
			ini_write_real("Inventory", "inventory[" + string(i) + "][1]", inventory[i][1]);
		
		}
	}
	
	{ //Save Time
		
		ini_write_real("Time","yearP",year);
		ini_write_real("Time","monthP",month);
		ini_write_real("Time","dayP",day);
		ini_write_real("Time","hourP",hour);
		ini_write_real("Time","minuteP",minute);
		ini_write_real("Time","secondP",second);
		
	}
	
	{ //Saving Job Doing
		
		ini_write_real("Current Job","doingCollectingJob[0]",doingCollectingJob[0]);
		ini_write_real("Current Job","doingCollectingJob[1]",doingCollectingJob[1]);
		ini_write_real("Current Job","doingCollectingJob[2]",doingCollectingJob[2]);
		
	}
	
	
	ini_close();
	
	if(file_exists("save.ini")) show_debug_message("File Saved!");
	
}

function loadGame(){
	
	if file_exists("save.ini") {
		
		ini_open("save.ini");
		
		{ //Loading Levels and Inventory
			for(i = 3; i < 3 + numberOfCollectingSkills; i++) { //Load All Collecting Levels
				xp[i] = ini_read_real("Skill Levels", "xp[" + string(i) + "]", 0);
			}
			
			for(i = 8; i < 8 + numberOfProcessingSkills; i++) { //Load All Processing Levels
				xp[i] = ini_read_real("Skill Levels", "xp[" + string(i) + "]", 0);
			}
		
			inventorySlots = ini_read_real("Inventory", "inventorySlots", 30);
			for(i = 0; i < inventorySlots; i++) { //Loading All Inventory
			
				inventory[i][0] = ini_read_real("Inventory","inventory[" + string(i) + "][0]",0);
				inventory[i][1] = ini_read_real("Inventory","inventory[" + string(i) + "][1]",0);
			
			}
		}
		
		{ //Load Time
		
		yearP = ini_read_real("Time","yearP",current_year);
		monthP = ini_read_real("Time","monthP",current_month);
		dayP = ini_read_real("Time","dayP",current_day);
		hourP = ini_read_real("Time","hourP",current_hour);
		minuteP = ini_read_real("Time","minuteP",current_minute);
		secondP = ini_read_real("Time","secondP",current_second);
		
	}
		
		{ //Loading jobDoing
			
			doingCollectingJob[0] = ini_read_real("Current Job","doingCollectingJob[0]",0);
			doingCollectingJob[1] = ini_read_real("Current Job","doingCollectingJob[1]",0);
			doingCollectingJob[2] = ini_read_real("Current Job","doingCollectingJob[2]",0);
			
		}
		
		show_debug_message("Game Loaded!");
		alarm[11] = 1;
		
		ini_close();
		
	}
	
}

function deleteSave(){
	
	if file_exists("save.ini") { 
		file_delete("save.ini");
		show_debug_message("Save deleted");
	}
	
}