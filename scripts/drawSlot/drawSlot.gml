function drawSlot(X,Y,color){
	
	var X2,Y2;
	X2 = X + 64;
	Y2 = Y + 64;
	
	draw_set_color(color);
	draw_set_alpha(globals.qualityBackgroundAlpha);
	draw_rectangle(X,Y,X2,Y2,false);
	draw_set_alpha(1);
	draw_set_color(globals.defaultTextColor);
	draw_rectangle(X,Y,X2-1,Y2-1,true);
	

}