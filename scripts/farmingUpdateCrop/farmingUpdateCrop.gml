function farmingUpdateCrop(){

	show_debug_message("Updated Crops");
	
	for(section = 0; section < farmingNumberOfSections; section++) {
		
		if(farmingSectionOwned[section] == true) { //Update This Section
			
			for(X = 0; X < farmingSectionSize; X++) {
				
				for(Y = 0; Y < farmingSectionSize; Y++) {
									
					//Updating Plants
					if(farmingPlot[section][X][Y][1] != 0) && (farmingPlot[section][X][Y][2] < farmingPlot[section][X][Y][3]) {
						
						farmingPlot[section][X][Y][2] += 1;
						
					}
					
				}
				
			}
			
		}
		
	}

}