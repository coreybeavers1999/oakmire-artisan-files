function drawItemDescriptionIDPosition(itemID,amount,X,Y){
	
	var ItemID = itemID;
	var Amount = amount;
	
	var x1, x2, y1, y2;
	x1 = X;
	y1 = Y - itemDescriptionHeight;
	x2 = x1 + itemDescriptionWidth;
	y2 = Y;
	
	{ //Background
		
		draw_set_alpha(1);
		draw_set_color(globals.itemDescriptionBackgroundColor);
		draw_rectangle(x1,y1,x2,y2,false);
		draw_set_color(c_white);
		draw_rectangle(x1,y1,x2+1,y2+1,true);
		
	}
	
	{ //Name
		var nameBuffer = 4;
		
		draw_set_color(globals.cwClickableColor);
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		draw_set_font(fontSmall);
		draw_text(x1 + nameBuffer, y1 + nameBuffer, item[ItemID][0] + " (" + string(Amount) + ")");
	
	}
	
	{ //Quality Level
		
		var qualityYOffset = 28;
		draw_set_color(globals.cwNonClickableColor);
		draw_text(x1 + nameBuffer, y1 + nameBuffer + qualityYOffset, qualityLevel[item[ItemID][4]]);
		
	}
	
	{ //Base Value
		
		var valueYOffset = qualityYOffset + 28;
		draw_set_color(globals.cwNonClickableColor);
		draw_text(x1 + nameBuffer, y1 + nameBuffer + valueYOffset,"Value: " + string(item[ItemID][3] * Amount) + " Gold");
		
		
	}
	
}