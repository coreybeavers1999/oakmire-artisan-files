function drawJob(SkillID,JobID,X,Y){
	
	var iconBuffer, iconScale, textXBuffer, textYBuffer;
	iconScale = 3;
	iconBuffer = (jobIconHeight - (32 * iconScale)) / 2;
	textXBuffer = iconBuffer * 2;//4;
	textYBuffer = 4;
	
	var mouseIsOver = false;
	
	{ //Draw Background
		if(!mouseOver(X,Y,X+jobIconWidth,Y+jobIconHeight)) { //Set different color if the mouse is over
			draw_set_color(globals.jobIconBackgroundColor);
		} else {
			draw_set_color(globals.jobIconMouseOverColor);
			mouseIsOver = true;
		}
	
		draw_set_alpha(.75);
		draw_rectangle(X,Y,X + jobIconWidth, Y + jobIconHeight,false);
	
		draw_set_color(c_white);
		draw_rectangle(X,Y,X + jobIconWidth + 1, Y + jobIconHeight + 1,true);
	}
	
	{ //Drawing Face
		if(level[SkillID] >= job[SkillID][JobID][5]) { //Is high enough level
			//Draw Icon
			draw_sprite_ext(job[SkillID][JobID][1],0,X + iconBuffer, Y + iconBuffer,iconScale,iconScale,0,c_white,1);
	
			//Draw Job Name
			var nameX, nameY;
			nameX = X + (32 * iconScale) + textXBuffer;
			nameY = Y + textYBuffer;
	
			draw_set_valign(fa_top);
			draw_set_halign(fa_left);
			draw_set_font(fontMedium);
			draw_set_color(globals.cwClickableColor);
			draw_text(nameX, nameY, job[SkillID][JobID][0]);
	
			//Draw XP Bonus
			var xpX, xpY, xpYBuffer;
			xpYBuffer = 32;
			xpX = nameX;
			xpY = nameY + xpYBuffer;
	
			draw_set_font(fontSmall);
			draw_set_color(globals.cwNonClickableColor);
			draw_text(xpX, xpY, "+" + string(job[SkillID][JobID][2]) + " experience");
			
			//Draw Reward
			var rewardX, rewardY, rewardYBuffer;
			rewardYBuffer = xpYBuffer;
			rewardX = xpX;
			rewardY = xpY + rewardYBuffer;
			draw_text(rewardX,rewardY,"(+" + string(job[SkillID][JobID][7]) + " " + item[job[SkillID][JobID][6]][0] + ")");
		
			//Draw Completion Bar
			if(doingCollectingJob[0] == true && doingCollectingJob[1] == SkillID && doingCollectingJob[2] == JobID) { //Job Selected
			
				var x1, x2, y1, y2;
				x1 = X + jobIconCompX;
				y1 = Y + jobIconCompY;
				x2 = x1 + jobIconCompWidth;
				y2 = y1 + jobIconCompHeight;
			
				draw_healthbar(x1,y1,x2,y2,(job[SkillID][JobID][3] / job[SkillID][JobID][4]) * 100,globals.jobIconCompBackgroundColor,globals.jobIconCompMinColor,globals.jobIconCompMaxColor,0,true,false);
			
			}
		
		}
		else
		{ //Isn't high enough level
		
			var needLevelX, needLevelY;
			needLevelX = X + (jobIconWidth / 2);
			needLevelY = Y + (jobIconHeight / 2);
		
			//Draw Text
			draw_set_color(globals.cwClickableColor);
			draw_set_valign(fa_middle);
			draw_set_halign(fa_center);
			draw_text(needLevelX, needLevelY, "Unlock at level " + string(job[SkillID][JobID][5]));
		
		
		}
	}
		
		
	//Selecting Job
	if(mouseIsOver == true) && (mouse_check_button_pressed(mb_left)) && (level[SkillID] >= job[SkillID][JobID][5]) {
		
		if(doingCollectingJob[0] == true) && (doingCollectingJob[1] == SkillID) && (doingCollectingJob[2] == JobID) { //Stops doing job if already selected
			doingCollectingJob[0] = false; 
			doingCollectingJob[1] = 0;
			doingCollectingJob[2] = 0;
		}
		else
		{ //Equips Job
			
			doingCollectingJob[0] = true;
			doingCollectingJob[1] = SkillID;
			doingCollectingJob[2] = JobID;
			
		}
		
	}
}