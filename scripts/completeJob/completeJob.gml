function completeJob(SkillID, JobID){
	
	job[SkillID][JobID][3] = 0;
	
	if(SkillID == 3 or SkillID == 4) { //Treecutting And Mining
		
		xp[SkillID] += job[SkillID][JobID][2];
		
		if(job[SkillID][JobID][6] != 0) { //Dropping Default Item
			inventoryAddItem(job[SkillID][JobID][6],job[SkillID][JobID][7]);
		}
	
		if(job[SkillID][JobID][8] != 0) { //If this job has a chance to drop item
		
			var chance = irandom_range(1,100);
		
			if(chance <= job[SkillID][JobID][9]) { inventoryAddItem(job[SkillID][JobID][8],1); }
		
		}
	
		if(SkillID == 4) { //Mining Drops Coal Chunks
			
			inventoryAddItem(10000,1);
		
		}
	
	}
	
	if(SkillID == 5) { //Fishing
		
		{ //Breaking Down String Into Information
			var fishIDs,fishChances,fishXPs;
		
			var fishOptionCount = 1;
		
			var whileLoop = true;
			var letterCount = 0;
			while(whileLoop == true) { //Figuring how many fish options there are
			
				if (string_char_at(job[5][doingCollectingJob[2]][2],letterCount) == ",") {  fishOptionCount += 1; } else
				if (string_char_at(job[5][doingCollectingJob[2]][2],letterCount) == "/") { whileLoop = false; break; }
			
				letterCount += 1;
		
			}
		
			letterCount = 1;
			for(IDCould = 0; IDCould < fishOptionCount; IDCould += 1;) { //Getting ID's for each potential fish
			
				var IDString = "";
				while(true) { //Cycles through each letter and writes the number to IDString and stops once it hits a , or /
					var currentLetter = string_char_at(job[5][doingCollectingJob[2]][2],letterCount);				
					if(currentLetter != "," && currentLetter != "/") { IDString += currentLetter; } else { letterCount += 1; break; }
			
					letterCount += 1;
				}
			
				//Enter the IDString into database
				fishIDs[IDCould] = int64(IDString);
			
			}
		
			for(IDCould = 0; IDCould < fishOptionCount; IDCould += 1;) { //Getting Chance for each potential fish
			
				var IDString = "";
				while(true) { //Cycles through each letter and writes the number to IDString and stops once it hits a , or /
					var currentLetter = string_char_at(job[5][doingCollectingJob[2]][2],letterCount);				
					if(currentLetter != "," && currentLetter != "/") { IDString += currentLetter; } else { letterCount += 1; break; }
			
					letterCount += 1;
				}
			
				//Enter the IDString into database
				fishChances[IDCould] = int64(IDString);
			
			}
		
			for(IDCould = 0; IDCould < fishOptionCount; IDCould += 1;) { //Getting XP for each potential fish
			
				var IDString = "";
				while(true) { //Cycles through each letter and writes the number to IDString and stops once it hits a , or /
					var currentLetter = string_char_at(job[5][doingCollectingJob[2]][2],letterCount);				
					if(currentLetter != "," && currentLetter != "/") { IDString += currentLetter; } else { letterCount += 1; break; }
			
					letterCount += 1;
				}
			
				//Enter the IDString into database
				fishXPs[IDCould] = int64(IDString);
			
			}
		}
		
		{ //Giving Item and XP
			
			var rando = irandom_range(1,100);
			
			for(runTimes = fishOptionCount-1; runTimes >= 0; runTimes--) {
				
				if(rando <= fishChances[runTimes]) { inventoryAddItem(int64(fishIDs[runTimes]),1); xp[5] += fishXPs[runTimes]; exit; }
				
			}
			
		}
		
	} else
	
	if(SkillID == 6) { //Foraging
		
		{ //Breaking Down String Into Information
			var fishIDs,fishChances,fishXPs;
		
			var fishOptionCount = 1;
		
			var whileLoop = true;
			var letterCount = 0;
			while(whileLoop == true) { //Figuring how many fish options there are
			
				if (string_char_at(job[6][doingCollectingJob[2]][2],letterCount) == ",") {  fishOptionCount += 1; } else
				if (string_char_at(job[6][doingCollectingJob[2]][2],letterCount) == "/") { whileLoop = false; break; }
			
				letterCount += 1;
		
			}
		
			letterCount = 1;
			for(IDCould = 0; IDCould < fishOptionCount; IDCould += 1;) { //Getting ID's for each potential fish
			
				var IDString = "";
				while(true) { //Cycles through each letter and writes the number to IDString and stops once it hits a , or /
					var currentLetter = string_char_at(job[6][doingCollectingJob[2]][2],letterCount);				
					if(currentLetter != "," && currentLetter != "/") { IDString += currentLetter; } else { letterCount += 1; break; }
			
					letterCount += 1;
				}
			
				//Enter the IDString into database
				fishIDs[IDCould] = int64(IDString);
			
			}
		
			for(IDCould = 0; IDCould < fishOptionCount; IDCould += 1;) { //Getting Chance for each potential fish
			
				var IDString = "";
				while(true) { //Cycles through each letter and writes the number to IDString and stops once it hits a , or /
					var currentLetter = string_char_at(job[6][doingCollectingJob[2]][2],letterCount);	
					if(currentLetter != "," && currentLetter != "/") { IDString += currentLetter; } else { letterCount += 1; break; }
			
					letterCount += 1;
				}
			
				//Enter the IDString into database
				fishChances[IDCould] = int64(IDString);
			
			}
			
			for(IDCould = 0; IDCould < fishOptionCount; IDCould += 1;) { //Getting XP for each potential fish
				var IDString = "";
				while(true) { //Cycles through each letter and writes the number to IDString and stops once it hits a , or /
					var currentLetter = string_char_at(job[6][doingCollectingJob[2]][2],letterCount);
					
					if(currentLetter != "," && currentLetter != "/") { IDString += currentLetter; } else { letterCount += 1; break; }
					
					letterCount += 1;
				}
			
				//Enter the IDString into database
				fishXPs[IDCould] = int64(IDString);
			
			}
		}
		
		{ //Giving Item and XP
			
			var rando = irandom_range(1,100);
			
			for(runTimes = fishOptionCount-1; runTimes >= 0; runTimes--) {
				
				if(rando <= fishChances[runTimes]) { inventoryAddItem(int64(fishIDs[runTimes]),1); xp[6] += fishXPs[runTimes]; exit; }
				
			}
			
		}
		
	}
	
}