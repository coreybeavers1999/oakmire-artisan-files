{
  "hinting": 0,
  "glyphOperations": 0,
  "interpreter": 0,
  "pointRounding": 0,
  "fontName": "Advent Pro",
  "styleName": "Regular",
  "size": 12.0,
  "bold": false,
  "italic": false,
  "charset": 0,
  "AntiAlias": 1,
  "first": 0,
  "last": 0,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\nDefault character: ▯ (9647)",
  "includeTTF": false,
  "TTFName": "",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "ascenderOffset": 0,
  "glyphs": {
    "32": {"x":2,"y":2,"w":4,"h":19,"character":32,"shift":4,"offset":0,},
    "33": {"x":70,"y":44,"w":2,"h":19,"character":33,"shift":4,"offset":1,},
    "34": {"x":63,"y":44,"w":5,"h":19,"character":34,"shift":7,"offset":1,},
    "35": {"x":51,"y":44,"w":10,"h":19,"character":35,"shift":10,"offset":0,},
    "36": {"x":40,"y":44,"w":9,"h":19,"character":36,"shift":9,"offset":0,},
    "37": {"x":27,"y":44,"w":11,"h":19,"character":37,"shift":11,"offset":0,},
    "38": {"x":16,"y":44,"w":9,"h":19,"character":38,"shift":9,"offset":0,},
    "39": {"x":12,"y":44,"w":2,"h":19,"character":39,"shift":3,"offset":0,},
    "40": {"x":7,"y":44,"w":3,"h":19,"character":40,"shift":5,"offset":1,},
    "41": {"x":2,"y":44,"w":3,"h":19,"character":41,"shift":5,"offset":1,},
    "42": {"x":74,"y":44,"w":6,"h":19,"character":42,"shift":6,"offset":0,},
    "43": {"x":243,"y":23,"w":6,"h":19,"character":43,"shift":6,"offset":0,},
    "44": {"x":230,"y":23,"w":2,"h":19,"character":44,"shift":3,"offset":0,},
    "45": {"x":222,"y":23,"w":6,"h":19,"character":45,"shift":7,"offset":0,},
    "46": {"x":218,"y":23,"w":2,"h":19,"character":46,"shift":2,"offset":0,},
    "47": {"x":209,"y":23,"w":7,"h":19,"character":47,"shift":7,"offset":0,},
    "48": {"x":200,"y":23,"w":7,"h":19,"character":48,"shift":9,"offset":1,},
    "49": {"x":195,"y":23,"w":3,"h":19,"character":49,"shift":3,"offset":0,},
    "50": {"x":187,"y":23,"w":6,"h":19,"character":50,"shift":6,"offset":0,},
    "51": {"x":178,"y":23,"w":7,"h":19,"character":51,"shift":7,"offset":0,},
    "52": {"x":168,"y":23,"w":8,"h":19,"character":52,"shift":8,"offset":0,},
    "53": {"x":234,"y":23,"w":7,"h":19,"character":53,"shift":7,"offset":0,},
    "54": {"x":82,"y":44,"w":8,"h":19,"character":54,"shift":8,"offset":0,},
    "55": {"x":92,"y":44,"w":7,"h":19,"character":55,"shift":7,"offset":0,},
    "56": {"x":101,"y":44,"w":8,"h":19,"character":56,"shift":8,"offset":0,},
    "57": {"x":27,"y":65,"w":8,"h":19,"character":57,"shift":8,"offset":0,},
    "58": {"x":23,"y":65,"w":2,"h":19,"character":58,"shift":2,"offset":0,},
    "59": {"x":18,"y":65,"w":3,"h":19,"character":59,"shift":2,"offset":0,},
    "60": {"x":10,"y":65,"w":6,"h":19,"character":60,"shift":6,"offset":0,},
    "61": {"x":2,"y":65,"w":6,"h":19,"character":61,"shift":6,"offset":0,},
    "62": {"x":245,"y":44,"w":6,"h":19,"character":62,"shift":6,"offset":0,},
    "63": {"x":236,"y":44,"w":7,"h":19,"character":63,"shift":6,"offset":0,},
    "64": {"x":223,"y":44,"w":11,"h":19,"character":64,"shift":11,"offset":0,},
    "65": {"x":212,"y":44,"w":9,"h":19,"character":65,"shift":8,"offset":0,},
    "66": {"x":202,"y":44,"w":8,"h":19,"character":66,"shift":9,"offset":1,},
    "67": {"x":194,"y":44,"w":6,"h":19,"character":67,"shift":7,"offset":1,},
    "68": {"x":185,"y":44,"w":7,"h":19,"character":68,"shift":9,"offset":1,},
    "69": {"x":177,"y":44,"w":6,"h":19,"character":69,"shift":7,"offset":1,},
    "70": {"x":169,"y":44,"w":6,"h":19,"character":70,"shift":7,"offset":1,},
    "71": {"x":160,"y":44,"w":7,"h":19,"character":71,"shift":9,"offset":1,},
    "72": {"x":150,"y":44,"w":8,"h":19,"character":72,"shift":9,"offset":1,},
    "73": {"x":145,"y":44,"w":3,"h":19,"character":73,"shift":4,"offset":1,},
    "74": {"x":140,"y":44,"w":3,"h":19,"character":74,"shift":3,"offset":0,},
    "75": {"x":131,"y":44,"w":7,"h":19,"character":75,"shift":8,"offset":1,},
    "76": {"x":122,"y":44,"w":7,"h":19,"character":76,"shift":7,"offset":1,},
    "77": {"x":111,"y":44,"w":9,"h":19,"character":77,"shift":11,"offset":1,},
    "78": {"x":157,"y":23,"w":9,"h":19,"character":78,"shift":10,"offset":1,},
    "79": {"x":148,"y":23,"w":7,"h":19,"character":79,"shift":9,"offset":1,},
    "80": {"x":138,"y":23,"w":8,"h":19,"character":80,"shift":9,"offset":1,},
    "81": {"x":193,"y":2,"w":7,"h":19,"character":81,"shift":9,"offset":1,},
    "82": {"x":178,"y":2,"w":8,"h":19,"character":82,"shift":9,"offset":1,},
    "83": {"x":167,"y":2,"w":9,"h":19,"character":83,"shift":9,"offset":0,},
    "84": {"x":158,"y":2,"w":7,"h":19,"character":84,"shift":6,"offset":0,},
    "85": {"x":149,"y":2,"w":7,"h":19,"character":85,"shift":9,"offset":1,},
    "86": {"x":139,"y":2,"w":8,"h":19,"character":86,"shift":9,"offset":1,},
    "87": {"x":125,"y":2,"w":12,"h":19,"character":87,"shift":13,"offset":1,},
    "88": {"x":114,"y":2,"w":9,"h":19,"character":88,"shift":10,"offset":1,},
    "89": {"x":103,"y":2,"w":9,"h":19,"character":89,"shift":9,"offset":0,},
    "90": {"x":92,"y":2,"w":9,"h":19,"character":90,"shift":9,"offset":0,},
    "91": {"x":188,"y":2,"w":3,"h":19,"character":91,"shift":5,"offset":1,},
    "92": {"x":83,"y":2,"w":7,"h":19,"character":92,"shift":8,"offset":1,},
    "93": {"x":72,"y":2,"w":3,"h":19,"character":93,"shift":5,"offset":1,},
    "94": {"x":65,"y":2,"w":5,"h":19,"character":94,"shift":6,"offset":1,},
    "95": {"x":57,"y":2,"w":6,"h":19,"character":95,"shift":6,"offset":1,},
    "96": {"x":52,"y":2,"w":3,"h":19,"character":96,"shift":4,"offset":0,},
    "97": {"x":43,"y":2,"w":7,"h":19,"character":97,"shift":7,"offset":0,},
    "98": {"x":34,"y":2,"w":7,"h":19,"character":98,"shift":8,"offset":1,},
    "99": {"x":26,"y":2,"w":6,"h":19,"character":99,"shift":6,"offset":0,},
    "100": {"x":17,"y":2,"w":7,"h":19,"character":100,"shift":8,"offset":1,},
    "101": {"x":8,"y":2,"w":7,"h":19,"character":101,"shift":7,"offset":0,},
    "102": {"x":77,"y":2,"w":4,"h":19,"character":102,"shift":5,"offset":1,},
    "103": {"x":202,"y":2,"w":7,"h":19,"character":103,"shift":7,"offset":0,},
    "104": {"x":42,"y":23,"w":6,"h":19,"character":104,"shift":8,"offset":1,},
    "105": {"x":211,"y":2,"w":2,"h":19,"character":105,"shift":4,"offset":1,},
    "106": {"x":123,"y":23,"w":4,"h":19,"character":106,"shift":3,"offset":-1,},
    "107": {"x":115,"y":23,"w":6,"h":19,"character":107,"shift":7,"offset":1,},
    "108": {"x":111,"y":23,"w":2,"h":19,"character":108,"shift":3,"offset":1,},
    "109": {"x":98,"y":23,"w":11,"h":19,"character":109,"shift":12,"offset":1,},
    "110": {"x":90,"y":23,"w":6,"h":19,"character":110,"shift":8,"offset":1,},
    "111": {"x":81,"y":23,"w":7,"h":19,"character":111,"shift":8,"offset":1,},
    "112": {"x":72,"y":23,"w":7,"h":19,"character":112,"shift":8,"offset":1,},
    "113": {"x":63,"y":23,"w":7,"h":19,"character":113,"shift":7,"offset":0,},
    "114": {"x":56,"y":23,"w":5,"h":19,"character":114,"shift":5,"offset":0,},
    "115": {"x":129,"y":23,"w":7,"h":19,"character":115,"shift":7,"offset":0,},
    "116": {"x":50,"y":23,"w":4,"h":19,"character":116,"shift":4,"offset":0,},
    "117": {"x":33,"y":23,"w":7,"h":19,"character":117,"shift":7,"offset":0,},
    "118": {"x":24,"y":23,"w":7,"h":19,"character":118,"shift":7,"offset":0,},
    "119": {"x":11,"y":23,"w":11,"h":19,"character":119,"shift":11,"offset":0,},
    "120": {"x":2,"y":23,"w":7,"h":19,"character":120,"shift":8,"offset":1,},
    "121": {"x":240,"y":2,"w":8,"h":19,"character":121,"shift":8,"offset":0,},
    "122": {"x":231,"y":2,"w":7,"h":19,"character":122,"shift":8,"offset":1,},
    "123": {"x":225,"y":2,"w":4,"h":19,"character":123,"shift":5,"offset":1,},
    "124": {"x":221,"y":2,"w":2,"h":19,"character":124,"shift":3,"offset":1,},
    "125": {"x":215,"y":2,"w":4,"h":19,"character":125,"shift":5,"offset":1,},
    "126": {"x":37,"y":65,"w":7,"h":19,"character":126,"shift":8,"offset":1,},
    "9647": {"x":46,"y":65,"w":10,"h":19,"character":9647,"shift":16,"offset":3,},
  },
  "kerningPairs": [],
  "ranges": [
    {"lower":32,"upper":127,},
    {"lower":9647,"upper":9647,},
  ],
  "regenerateBitmap": false,
  "canGenerateBitmap": true,
  "maintainGms1Font": false,
  "parent": {
    "name": "Fonts",
    "path": "folders/Fonts.yy",
  },
  "resourceVersion": "1.0",
  "name": "fontTiny",
  "tags": [],
  "resourceType": "GMFont",
}