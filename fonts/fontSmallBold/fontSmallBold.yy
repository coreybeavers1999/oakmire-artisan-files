{
  "hinting": 0,
  "glyphOperations": 0,
  "interpreter": 0,
  "pointRounding": 0,
  "fontName": "Bitcrusher Cd Rg",
  "styleName": "Condensed Bold",
  "size": 20.0,
  "bold": true,
  "italic": false,
  "charset": 0,
  "AntiAlias": 1,
  "first": 0,
  "last": 0,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\nDefault character: ▯ (9647)",
  "includeTTF": false,
  "TTFName": "",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "ascenderOffset": 0,
  "glyphs": {
    "32": {"x":2,"y":2,"w":3,"h":32,"character":32,"shift":3,"offset":0,},
    "33": {"x":200,"y":70,"w":4,"h":32,"character":33,"shift":4,"offset":0,},
    "34": {"x":191,"y":70,"w":7,"h":32,"character":34,"shift":7,"offset":0,},
    "35": {"x":179,"y":70,"w":10,"h":32,"character":35,"shift":10,"offset":0,},
    "36": {"x":168,"y":70,"w":9,"h":32,"character":36,"shift":10,"offset":0,},
    "37": {"x":154,"y":70,"w":12,"h":32,"character":37,"shift":12,"offset":0,},
    "38": {"x":141,"y":70,"w":11,"h":32,"character":38,"shift":11,"offset":0,},
    "39": {"x":136,"y":70,"w":3,"h":32,"character":39,"shift":3,"offset":0,},
    "40": {"x":129,"y":70,"w":5,"h":32,"character":40,"shift":6,"offset":1,},
    "41": {"x":121,"y":70,"w":6,"h":32,"character":41,"shift":6,"offset":0,},
    "42": {"x":206,"y":70,"w":10,"h":32,"character":42,"shift":10,"offset":0,},
    "43": {"x":107,"y":70,"w":12,"h":32,"character":43,"shift":12,"offset":0,},
    "44": {"x":90,"y":70,"w":4,"h":32,"character":44,"shift":4,"offset":0,},
    "45": {"x":81,"y":70,"w":7,"h":32,"character":45,"shift":6,"offset":0,},
    "46": {"x":75,"y":70,"w":4,"h":32,"character":46,"shift":4,"offset":0,},
    "47": {"x":65,"y":70,"w":8,"h":32,"character":47,"shift":8,"offset":0,},
    "48": {"x":54,"y":70,"w":9,"h":32,"character":48,"shift":10,"offset":0,},
    "49": {"x":46,"y":70,"w":6,"h":32,"character":49,"shift":6,"offset":0,},
    "50": {"x":35,"y":70,"w":9,"h":32,"character":50,"shift":9,"offset":0,},
    "51": {"x":25,"y":70,"w":8,"h":32,"character":51,"shift":8,"offset":0,},
    "52": {"x":14,"y":70,"w":9,"h":32,"character":52,"shift":9,"offset":0,},
    "53": {"x":96,"y":70,"w":9,"h":32,"character":53,"shift":9,"offset":0,},
    "54": {"x":218,"y":70,"w":9,"h":32,"character":54,"shift":9,"offset":0,},
    "55": {"x":229,"y":70,"w":8,"h":32,"character":55,"shift":8,"offset":0,},
    "56": {"x":239,"y":70,"w":9,"h":32,"character":56,"shift":9,"offset":0,},
    "57": {"x":217,"y":104,"w":9,"h":32,"character":57,"shift":9,"offset":0,},
    "58": {"x":211,"y":104,"w":4,"h":32,"character":58,"shift":4,"offset":0,},
    "59": {"x":205,"y":104,"w":4,"h":32,"character":59,"shift":4,"offset":0,},
    "60": {"x":191,"y":104,"w":12,"h":32,"character":60,"shift":12,"offset":0,},
    "61": {"x":177,"y":104,"w":12,"h":32,"character":61,"shift":12,"offset":0,},
    "62": {"x":163,"y":104,"w":12,"h":32,"character":62,"shift":12,"offset":0,},
    "63": {"x":152,"y":104,"w":9,"h":32,"character":63,"shift":9,"offset":0,},
    "64": {"x":138,"y":104,"w":12,"h":32,"character":64,"shift":12,"offset":0,},
    "65": {"x":126,"y":104,"w":10,"h":32,"character":65,"shift":10,"offset":0,},
    "66": {"x":115,"y":104,"w":9,"h":32,"character":66,"shift":10,"offset":0,},
    "67": {"x":105,"y":104,"w":8,"h":32,"character":67,"shift":8,"offset":0,},
    "68": {"x":94,"y":104,"w":9,"h":32,"character":68,"shift":10,"offset":0,},
    "69": {"x":84,"y":104,"w":8,"h":32,"character":69,"shift":8,"offset":0,},
    "70": {"x":75,"y":104,"w":7,"h":32,"character":70,"shift":7,"offset":0,},
    "71": {"x":64,"y":104,"w":9,"h":32,"character":71,"shift":9,"offset":0,},
    "72": {"x":53,"y":104,"w":9,"h":32,"character":72,"shift":9,"offset":0,},
    "73": {"x":47,"y":104,"w":4,"h":32,"character":73,"shift":4,"offset":0,},
    "74": {"x":38,"y":104,"w":7,"h":32,"character":74,"shift":7,"offset":0,},
    "75": {"x":26,"y":104,"w":10,"h":32,"character":75,"shift":9,"offset":0,},
    "76": {"x":17,"y":104,"w":7,"h":32,"character":76,"shift":7,"offset":0,},
    "77": {"x":2,"y":104,"w":13,"h":32,"character":77,"shift":14,"offset":0,},
    "78": {"x":2,"y":70,"w":10,"h":32,"character":78,"shift":10,"offset":0,},
    "79": {"x":244,"y":36,"w":9,"h":32,"character":79,"shift":10,"offset":0,},
    "80": {"x":233,"y":36,"w":9,"h":32,"character":80,"shift":9,"offset":0,},
    "81": {"x":236,"y":2,"w":9,"h":32,"character":81,"shift":10,"offset":0,},
    "82": {"x":217,"y":2,"w":10,"h":32,"character":82,"shift":10,"offset":0,},
    "83": {"x":206,"y":2,"w":9,"h":32,"character":83,"shift":9,"offset":0,},
    "84": {"x":195,"y":2,"w":9,"h":32,"character":84,"shift":8,"offset":0,},
    "85": {"x":184,"y":2,"w":9,"h":32,"character":85,"shift":10,"offset":0,},
    "86": {"x":172,"y":2,"w":10,"h":32,"character":86,"shift":10,"offset":0,},
    "87": {"x":157,"y":2,"w":13,"h":32,"character":87,"shift":14,"offset":0,},
    "88": {"x":145,"y":2,"w":10,"h":32,"character":88,"shift":9,"offset":0,},
    "89": {"x":134,"y":2,"w":9,"h":32,"character":89,"shift":10,"offset":0,},
    "90": {"x":123,"y":2,"w":9,"h":32,"character":90,"shift":9,"offset":0,},
    "91": {"x":229,"y":2,"w":5,"h":32,"character":91,"shift":6,"offset":1,},
    "92": {"x":113,"y":2,"w":8,"h":32,"character":92,"shift":8,"offset":0,},
    "93": {"x":96,"y":2,"w":6,"h":32,"character":93,"shift":6,"offset":0,},
    "94": {"x":82,"y":2,"w":12,"h":32,"character":94,"shift":12,"offset":0,},
    "95": {"x":68,"y":2,"w":12,"h":32,"character":95,"shift":10,"offset":-1,},
    "96": {"x":60,"y":2,"w":6,"h":32,"character":96,"shift":7,"offset":0,},
    "97": {"x":49,"y":2,"w":9,"h":32,"character":97,"shift":9,"offset":0,},
    "98": {"x":38,"y":2,"w":9,"h":32,"character":98,"shift":9,"offset":0,},
    "99": {"x":29,"y":2,"w":7,"h":32,"character":99,"shift":7,"offset":0,},
    "100": {"x":18,"y":2,"w":9,"h":32,"character":100,"shift":9,"offset":0,},
    "101": {"x":7,"y":2,"w":9,"h":32,"character":101,"shift":9,"offset":0,},
    "102": {"x":104,"y":2,"w":7,"h":32,"character":102,"shift":7,"offset":0,},
    "103": {"x":2,"y":36,"w":9,"h":32,"character":103,"shift":9,"offset":0,},
    "104": {"x":111,"y":36,"w":9,"h":32,"character":104,"shift":9,"offset":0,},
    "105": {"x":13,"y":36,"w":4,"h":32,"character":105,"shift":4,"offset":0,},
    "106": {"x":215,"y":36,"w":6,"h":32,"character":106,"shift":4,"offset":-2,},
    "107": {"x":204,"y":36,"w":9,"h":32,"character":107,"shift":9,"offset":0,},
    "108": {"x":198,"y":36,"w":4,"h":32,"character":108,"shift":4,"offset":0,},
    "109": {"x":183,"y":36,"w":13,"h":32,"character":109,"shift":13,"offset":0,},
    "110": {"x":172,"y":36,"w":9,"h":32,"character":110,"shift":9,"offset":0,},
    "111": {"x":161,"y":36,"w":9,"h":32,"character":111,"shift":9,"offset":0,},
    "112": {"x":150,"y":36,"w":9,"h":32,"character":112,"shift":9,"offset":0,},
    "113": {"x":139,"y":36,"w":9,"h":32,"character":113,"shift":9,"offset":0,},
    "114": {"x":131,"y":36,"w":6,"h":32,"character":114,"shift":6,"offset":0,},
    "115": {"x":223,"y":36,"w":8,"h":32,"character":115,"shift":8,"offset":0,},
    "116": {"x":122,"y":36,"w":7,"h":32,"character":116,"shift":7,"offset":0,},
    "117": {"x":100,"y":36,"w":9,"h":32,"character":117,"shift":9,"offset":0,},
    "118": {"x":89,"y":36,"w":9,"h":32,"character":118,"shift":9,"offset":0,},
    "119": {"x":74,"y":36,"w":13,"h":32,"character":119,"shift":13,"offset":0,},
    "120": {"x":63,"y":36,"w":9,"h":32,"character":120,"shift":9,"offset":0,},
    "121": {"x":52,"y":36,"w":9,"h":32,"character":121,"shift":9,"offset":0,},
    "122": {"x":41,"y":36,"w":9,"h":32,"character":122,"shift":9,"offset":0,},
    "123": {"x":33,"y":36,"w":6,"h":32,"character":123,"shift":6,"offset":0,},
    "124": {"x":28,"y":36,"w":3,"h":32,"character":124,"shift":5,"offset":1,},
    "125": {"x":19,"y":36,"w":7,"h":32,"character":125,"shift":6,"offset":0,},
    "126": {"x":228,"y":104,"w":12,"h":32,"character":126,"shift":12,"offset":0,},
    "9647": {"x":2,"y":138,"w":16,"h":32,"character":9647,"shift":26,"offset":5,},
  },
  "kerningPairs": [
    {"first":38,"second":45,"amount":-1,},
    {"first":38,"second":84,"amount":-1,},
    {"first":38,"second":173,"amount":-1,},
    {"first":38,"second":932,"amount":-1,},
    {"first":38,"second":1058,"amount":-1,},
    {"first":38,"second":8211,"amount":-1,},
    {"first":38,"second":8212,"amount":-1,},
    {"first":39,"second":44,"amount":-1,},
    {"first":39,"second":46,"amount":-1,},
    {"first":39,"second":47,"amount":-2,},
    {"first":39,"second":65,"amount":-1,},
    {"first":39,"second":74,"amount":-2,},
    {"first":39,"second":192,"amount":-1,},
    {"first":39,"second":193,"amount":-1,},
    {"first":39,"second":194,"amount":-1,},
    {"first":39,"second":195,"amount":-1,},
    {"first":39,"second":196,"amount":-1,},
    {"first":39,"second":197,"amount":-1,},
    {"first":39,"second":198,"amount":-1,},
    {"first":39,"second":256,"amount":-1,},
    {"first":39,"second":913,"amount":-1,},
    {"first":39,"second":1032,"amount":-2,},
    {"first":39,"second":1040,"amount":-1,},
    {"first":39,"second":8218,"amount":-1,},
    {"first":39,"second":8222,"amount":-1,},
    {"first":39,"second":8230,"amount":-1,},
    {"first":40,"second":106,"amount":1,},
    {"first":40,"second":1112,"amount":1,},
    {"first":44,"second":39,"amount":-1,},
    {"first":44,"second":45,"amount":-1,},
    {"first":44,"second":49,"amount":-1,},
    {"first":44,"second":52,"amount":-2,},
    {"first":44,"second":84,"amount":-2,},
    {"first":44,"second":86,"amount":-1,},
    {"first":44,"second":89,"amount":-2,},
    {"first":44,"second":102,"amount":-1,},
    {"first":44,"second":116,"amount":-1,},
    {"first":44,"second":118,"amount":-1,},
    {"first":44,"second":173,"amount":-1,},
    {"first":44,"second":376,"amount":-2,},
    {"first":44,"second":932,"amount":-2,},
    {"first":44,"second":933,"amount":-2,},
    {"first":44,"second":939,"amount":-2,},
    {"first":44,"second":957,"amount":-1,},
    {"first":44,"second":960,"amount":-1,},
    {"first":44,"second":1058,"amount":-2,},
    {"first":44,"second":1198,"amount":-2,},
    {"first":44,"second":1216,"amount":-1,},
    {"first":44,"second":8211,"amount":-1,},
    {"first":44,"second":8212,"amount":-1,},
    {"first":44,"second":8216,"amount":-1,},
    {"first":44,"second":8217,"amount":-1,},
    {"first":44,"second":8220,"amount":-1,},
    {"first":44,"second":8221,"amount":-1,},
    {"first":45,"second":44,"amount":-1,},
    {"first":45,"second":46,"amount":-1,},
    {"first":45,"second":47,"amount":-2,},
    {"first":45,"second":49,"amount":-1,},
    {"first":45,"second":50,"amount":-1,},
    {"first":45,"second":53,"amount":-1,},
    {"first":45,"second":55,"amount":-2,},
    {"first":45,"second":74,"amount":-2,},
    {"first":45,"second":83,"amount":-1,},
    {"first":45,"second":84,"amount":-2,},
    {"first":45,"second":86,"amount":-1,},
    {"first":45,"second":88,"amount":-1,},
    {"first":45,"second":90,"amount":-2,},
    {"first":45,"second":102,"amount":-1,},
    {"first":45,"second":116,"amount":-1,},
    {"first":45,"second":120,"amount":-1,},
    {"first":45,"second":122,"amount":-1,},
    {"first":45,"second":918,"amount":-2,},
    {"first":45,"second":932,"amount":-2,},
    {"first":45,"second":935,"amount":-1,},
    {"first":45,"second":960,"amount":-1,},
    {"first":45,"second":1029,"amount":-1,},
    {"first":45,"second":1032,"amount":-2,},
    {"first":45,"second":1058,"amount":-2,},
    {"first":45,"second":1061,"amount":-1,},
    {"first":45,"second":1093,"amount":-1,},
    {"first":45,"second":1216,"amount":-1,},
    {"first":45,"second":8218,"amount":-1,},
    {"first":45,"second":8222,"amount":-1,},
    {"first":45,"second":8230,"amount":-1,},
    {"first":46,"second":39,"amount":-1,},
    {"first":46,"second":45,"amount":-1,},
    {"first":46,"second":49,"amount":-1,},
    {"first":46,"second":52,"amount":-2,},
    {"first":46,"second":84,"amount":-2,},
    {"first":46,"second":86,"amount":-1,},
    {"first":46,"second":89,"amount":-2,},
    {"first":46,"second":102,"amount":-1,},
    {"first":46,"second":116,"amount":-1,},
    {"first":46,"second":118,"amount":-1,},
    {"first":46,"second":173,"amount":-1,},
    {"first":46,"second":376,"amount":-2,},
    {"first":46,"second":932,"amount":-2,},
    {"first":46,"second":933,"amount":-2,},
    {"first":46,"second":939,"amount":-2,},
    {"first":46,"second":957,"amount":-1,},
    {"first":46,"second":960,"amount":-1,},
    {"first":46,"second":1058,"amount":-2,},
    {"first":46,"second":1198,"amount":-2,},
    {"first":46,"second":1216,"amount":-1,},
    {"first":46,"second":8211,"amount":-1,},
    {"first":46,"second":8212,"amount":-1,},
    {"first":46,"second":8216,"amount":-1,},
    {"first":46,"second":8217,"amount":-1,},
    {"first":46,"second":8220,"amount":-1,},
    {"first":46,"second":8221,"amount":-1,},
    {"first":47,"second":38,"amount":-1,},
    {"first":47,"second":45,"amount":-2,},
    {"first":47,"second":65,"amount":-1,},
    {"first":47,"second":74,"amount":-2,},
    {"first":47,"second":97,"amount":-1,},
    {"first":47,"second":99,"amount":-1,},
    {"first":47,"second":100,"amount":-1,},
    {"first":47,"second":101,"amount":-1,},
    {"first":47,"second":102,"amount":-1,},
    {"first":47,"second":103,"amount":-1,},
    {"first":47,"second":109,"amount":-1,},
    {"first":47,"second":110,"amount":-1,},
    {"first":47,"second":111,"amount":-1,},
    {"first":47,"second":112,"amount":-1,},
    {"first":47,"second":113,"amount":-1,},
    {"first":47,"second":114,"amount":-1,},
    {"first":47,"second":115,"amount":-2,},
    {"first":47,"second":116,"amount":-1,},
    {"first":47,"second":117,"amount":-1,},
    {"first":47,"second":118,"amount":-1,},
    {"first":47,"second":119,"amount":-1,},
    {"first":47,"second":120,"amount":-1,},
    {"first":47,"second":121,"amount":-1,},
    {"first":47,"second":122,"amount":-1,},
    {"first":47,"second":173,"amount":-2,},
    {"first":47,"second":192,"amount":-1,},
    {"first":47,"second":193,"amount":-1,},
    {"first":47,"second":194,"amount":-1,},
    {"first":47,"second":195,"amount":-1,},
    {"first":47,"second":196,"amount":-1,},
    {"first":47,"second":197,"amount":-1,},
    {"first":47,"second":198,"amount":-1,},
    {"first":47,"second":224,"amount":-1,},
    {"first":47,"second":225,"amount":-1,},
    {"first":47,"second":226,"amount":-1,},
    {"first":47,"second":227,"amount":-1,},
    {"first":47,"second":228,"amount":-1,},
    {"first":47,"second":229,"amount":-1,},
    {"first":47,"second":230,"amount":-1,},
    {"first":47,"second":231,"amount":-1,},
    {"first":47,"second":232,"amount":-1,},
    {"first":47,"second":233,"amount":-1,},
    {"first":47,"second":234,"amount":-1,},
    {"first":47,"second":235,"amount":-1,},
    {"first":47,"second":241,"amount":-1,},
    {"first":47,"second":242,"amount":-1,},
    {"first":47,"second":243,"amount":-1,},
    {"first":47,"second":244,"amount":-1,},
    {"first":47,"second":245,"amount":-1,},
    {"first":47,"second":246,"amount":-1,},
    {"first":47,"second":249,"amount":-1,},
    {"first":47,"second":250,"amount":-1,},
    {"first":47,"second":251,"amount":-1,},
    {"first":47,"second":252,"amount":-1,},
    {"first":47,"second":255,"amount":-1,},
    {"first":47,"second":256,"amount":-1,},
    {"first":47,"second":257,"amount":-1,},
    {"first":47,"second":333,"amount":-1,},
    {"first":47,"second":339,"amount":-1,},
    {"first":47,"second":913,"amount":-1,},
    {"first":47,"second":957,"amount":-1,},
    {"first":47,"second":959,"amount":-1,},
    {"first":47,"second":960,"amount":-1,},
    {"first":47,"second":969,"amount":-1,},
    {"first":47,"second":1032,"amount":-2,},
    {"first":47,"second":1040,"amount":-1,},
    {"first":47,"second":1072,"amount":-1,},
    {"first":47,"second":1077,"amount":-1,},
    {"first":47,"second":1084,"amount":-1,},
    {"first":47,"second":1086,"amount":-1,},
    {"first":47,"second":1088,"amount":-1,},
    {"first":47,"second":1089,"amount":-1,},
    {"first":47,"second":1091,"amount":-1,},
    {"first":47,"second":1093,"amount":-1,},
    {"first":47,"second":1104,"amount":-1,},
    {"first":47,"second":1105,"amount":-1,},
    {"first":47,"second":1109,"amount":-2,},
    {"first":47,"second":8211,"amount":-2,},
    {"first":47,"second":8212,"amount":-2,},
    {"first":50,"second":45,"amount":-1,},
    {"first":50,"second":173,"amount":-1,},
    {"first":50,"second":8211,"amount":-1,},
    {"first":50,"second":8212,"amount":-1,},
    {"first":53,"second":53,"amount":-1,},
    {"first":54,"second":39,"amount":-1,},
    {"first":54,"second":49,"amount":-1,},
    {"first":54,"second":55,"amount":-1,},
    {"first":54,"second":1216,"amount":-1,},
    {"first":54,"second":8216,"amount":-1,},
    {"first":54,"second":8217,"amount":-1,},
    {"first":54,"second":8220,"amount":-1,},
    {"first":54,"second":8221,"amount":-1,},
    {"first":55,"second":44,"amount":-2,},
    {"first":55,"second":45,"amount":-1,},
    {"first":55,"second":46,"amount":-2,},
    {"first":55,"second":50,"amount":-1,},
    {"first":55,"second":173,"amount":-1,},
    {"first":55,"second":8211,"amount":-1,},
    {"first":55,"second":8212,"amount":-1,},
    {"first":55,"second":8218,"amount":-2,},
    {"first":55,"second":8222,"amount":-2,},
    {"first":55,"second":8230,"amount":-2,},
    {"first":64,"second":106,"amount":1,},
    {"first":64,"second":1112,"amount":1,},
    {"first":65,"second":39,"amount":-1,},
    {"first":65,"second":63,"amount":-1,},
    {"first":65,"second":84,"amount":-1,},
    {"first":65,"second":86,"amount":-1,},
    {"first":65,"second":89,"amount":-1,},
    {"first":65,"second":102,"amount":-1,},
    {"first":65,"second":116,"amount":-1,},
    {"first":65,"second":118,"amount":-1,},
    {"first":65,"second":376,"amount":-1,},
    {"first":65,"second":932,"amount":-1,},
    {"first":65,"second":933,"amount":-1,},
    {"first":65,"second":939,"amount":-1,},
    {"first":65,"second":957,"amount":-1,},
    {"first":65,"second":960,"amount":-1,},
    {"first":65,"second":1058,"amount":-1,},
    {"first":65,"second":1198,"amount":-1,},
    {"first":65,"second":8216,"amount":-1,},
    {"first":65,"second":8217,"amount":-1,},
    {"first":65,"second":8220,"amount":-1,},
    {"first":65,"second":8221,"amount":-1,},
    {"first":66,"second":47,"amount":-1,},
    {"first":67,"second":45,"amount":-1,},
    {"first":67,"second":102,"amount":-1,},
    {"first":67,"second":116,"amount":-1,},
    {"first":67,"second":118,"amount":-1,},
    {"first":67,"second":171,"amount":-1,},
    {"first":67,"second":173,"amount":-1,},
    {"first":67,"second":957,"amount":-1,},
    {"first":67,"second":960,"amount":-1,},
    {"first":67,"second":8211,"amount":-1,},
    {"first":67,"second":8212,"amount":-1,},
    {"first":67,"second":8249,"amount":-1,},
    {"first":68,"second":47,"amount":-1,},
    {"first":70,"second":44,"amount":-2,},
    {"first":70,"second":46,"amount":-2,},
    {"first":70,"second":47,"amount":-2,},
    {"first":70,"second":74,"amount":-2,},
    {"first":70,"second":97,"amount":-1,},
    {"first":70,"second":120,"amount":-1,},
    {"first":70,"second":122,"amount":-1,},
    {"first":70,"second":187,"amount":-1,},
    {"first":70,"second":224,"amount":-1,},
    {"first":70,"second":225,"amount":-1,},
    {"first":70,"second":226,"amount":-1,},
    {"first":70,"second":227,"amount":-1,},
    {"first":70,"second":228,"amount":-1,},
    {"first":70,"second":229,"amount":-1,},
    {"first":70,"second":230,"amount":-1,},
    {"first":70,"second":257,"amount":-1,},
    {"first":70,"second":1032,"amount":-2,},
    {"first":70,"second":1072,"amount":-1,},
    {"first":70,"second":1093,"amount":-1,},
    {"first":70,"second":8218,"amount":-2,},
    {"first":70,"second":8222,"amount":-2,},
    {"first":70,"second":8230,"amount":-2,},
    {"first":70,"second":8250,"amount":-1,},
    {"first":71,"second":39,"amount":-1,},
    {"first":71,"second":63,"amount":-1,},
    {"first":71,"second":84,"amount":-2,},
    {"first":71,"second":86,"amount":-1,},
    {"first":71,"second":89,"amount":-1,},
    {"first":71,"second":116,"amount":-1,},
    {"first":71,"second":376,"amount":-1,},
    {"first":71,"second":932,"amount":-2,},
    {"first":71,"second":933,"amount":-1,},
    {"first":71,"second":939,"amount":-1,},
    {"first":71,"second":960,"amount":-1,},
    {"first":71,"second":1058,"amount":-2,},
    {"first":71,"second":1198,"amount":-1,},
    {"first":71,"second":8216,"amount":-1,},
    {"first":71,"second":8217,"amount":-1,},
    {"first":71,"second":8220,"amount":-1,},
    {"first":71,"second":8221,"amount":-1,},
    {"first":74,"second":47,"amount":-1,},
    {"first":75,"second":45,"amount":-1,},
    {"first":75,"second":102,"amount":-1,},
    {"first":75,"second":116,"amount":-1,},
    {"first":75,"second":118,"amount":-1,},
    {"first":75,"second":171,"amount":-1,},
    {"first":75,"second":173,"amount":-1,},
    {"first":75,"second":957,"amount":-1,},
    {"first":75,"second":960,"amount":-1,},
    {"first":75,"second":8211,"amount":-1,},
    {"first":75,"second":8212,"amount":-1,},
    {"first":75,"second":8249,"amount":-1,},
    {"first":76,"second":39,"amount":-1,},
    {"first":76,"second":45,"amount":-2,},
    {"first":76,"second":63,"amount":-1,},
    {"first":76,"second":84,"amount":-2,},
    {"first":76,"second":86,"amount":-1,},
    {"first":76,"second":89,"amount":-2,},
    {"first":76,"second":116,"amount":-1,},
    {"first":76,"second":118,"amount":-1,},
    {"first":76,"second":173,"amount":-2,},
    {"first":76,"second":376,"amount":-2,},
    {"first":76,"second":932,"amount":-2,},
    {"first":76,"second":933,"amount":-2,},
    {"first":76,"second":939,"amount":-2,},
    {"first":76,"second":957,"amount":-1,},
    {"first":76,"second":960,"amount":-1,},
    {"first":76,"second":1058,"amount":-2,},
    {"first":76,"second":1198,"amount":-2,},
    {"first":76,"second":8211,"amount":-2,},
    {"first":76,"second":8212,"amount":-2,},
    {"first":76,"second":8216,"amount":-1,},
    {"first":76,"second":8217,"amount":-1,},
    {"first":76,"second":8220,"amount":-1,},
    {"first":76,"second":8221,"amount":-1,},
    {"first":79,"second":47,"amount":-1,},
    {"first":80,"second":44,"amount":-3,},
    {"first":80,"second":46,"amount":-3,},
    {"first":80,"second":47,"amount":-2,},
    {"first":80,"second":65,"amount":-1,},
    {"first":80,"second":74,"amount":-2,},
    {"first":80,"second":88,"amount":-1,},
    {"first":80,"second":90,"amount":-1,},
    {"first":80,"second":97,"amount":-1,},
    {"first":80,"second":171,"amount":-1,},
    {"first":80,"second":192,"amount":-1,},
    {"first":80,"second":193,"amount":-1,},
    {"first":80,"second":194,"amount":-1,},
    {"first":80,"second":195,"amount":-1,},
    {"first":80,"second":196,"amount":-1,},
    {"first":80,"second":197,"amount":-1,},
    {"first":80,"second":198,"amount":-1,},
    {"first":80,"second":224,"amount":-1,},
    {"first":80,"second":225,"amount":-1,},
    {"first":80,"second":226,"amount":-1,},
    {"first":80,"second":227,"amount":-1,},
    {"first":80,"second":228,"amount":-1,},
    {"first":80,"second":229,"amount":-1,},
    {"first":80,"second":230,"amount":-1,},
    {"first":80,"second":256,"amount":-1,},
    {"first":80,"second":257,"amount":-1,},
    {"first":80,"second":913,"amount":-1,},
    {"first":80,"second":918,"amount":-1,},
    {"first":80,"second":935,"amount":-1,},
    {"first":80,"second":1032,"amount":-2,},
    {"first":80,"second":1040,"amount":-1,},
    {"first":80,"second":1061,"amount":-1,},
    {"first":80,"second":1072,"amount":-1,},
    {"first":80,"second":8218,"amount":-3,},
    {"first":80,"second":8222,"amount":-3,},
    {"first":80,"second":8230,"amount":-3,},
    {"first":80,"second":8249,"amount":-1,},
    {"first":81,"second":47,"amount":-1,},
    {"first":83,"second":45,"amount":-1,},
    {"first":83,"second":63,"amount":-1,},
    {"first":83,"second":102,"amount":-1,},
    {"first":83,"second":115,"amount":-1,},
    {"first":83,"second":116,"amount":-1,},
    {"first":83,"second":118,"amount":-1,},
    {"first":83,"second":173,"amount":-1,},
    {"first":83,"second":957,"amount":-1,},
    {"first":83,"second":960,"amount":-1,},
    {"first":83,"second":1109,"amount":-1,},
    {"first":83,"second":8211,"amount":-1,},
    {"first":83,"second":8212,"amount":-1,},
    {"first":84,"second":44,"amount":-2,},
    {"first":84,"second":45,"amount":-2,},
    {"first":84,"second":46,"amount":-2,},
    {"first":84,"second":47,"amount":-2,},
    {"first":84,"second":65,"amount":-1,},
    {"first":84,"second":74,"amount":-2,},
    {"first":84,"second":97,"amount":-1,},
    {"first":84,"second":99,"amount":-1,},
    {"first":84,"second":100,"amount":-1,},
    {"first":84,"second":101,"amount":-1,},
    {"first":84,"second":102,"amount":-1,},
    {"first":84,"second":103,"amount":-1,},
    {"first":84,"second":109,"amount":-1,},
    {"first":84,"second":110,"amount":-1,},
    {"first":84,"second":111,"amount":-1,},
    {"first":84,"second":112,"amount":-1,},
    {"first":84,"second":113,"amount":-1,},
    {"first":84,"second":114,"amount":-1,},
    {"first":84,"second":115,"amount":-1,},
    {"first":84,"second":116,"amount":-1,},
    {"first":84,"second":117,"amount":-1,},
    {"first":84,"second":118,"amount":-1,},
    {"first":84,"second":119,"amount":-1,},
    {"first":84,"second":120,"amount":-1,},
    {"first":84,"second":121,"amount":-1,},
    {"first":84,"second":122,"amount":-1,},
    {"first":84,"second":171,"amount":-2,},
    {"first":84,"second":173,"amount":-2,},
    {"first":84,"second":187,"amount":-2,},
    {"first":84,"second":192,"amount":-1,},
    {"first":84,"second":193,"amount":-1,},
    {"first":84,"second":194,"amount":-1,},
    {"first":84,"second":195,"amount":-1,},
    {"first":84,"second":196,"amount":-1,},
    {"first":84,"second":197,"amount":-1,},
    {"first":84,"second":198,"amount":-1,},
    {"first":84,"second":224,"amount":-1,},
    {"first":84,"second":225,"amount":-1,},
    {"first":84,"second":226,"amount":-1,},
    {"first":84,"second":227,"amount":-1,},
    {"first":84,"second":228,"amount":-1,},
    {"first":84,"second":229,"amount":-1,},
    {"first":84,"second":230,"amount":-1,},
    {"first":84,"second":231,"amount":-1,},
    {"first":84,"second":232,"amount":-1,},
    {"first":84,"second":233,"amount":-1,},
    {"first":84,"second":234,"amount":-1,},
    {"first":84,"second":235,"amount":-1,},
    {"first":84,"second":241,"amount":-1,},
    {"first":84,"second":242,"amount":-1,},
    {"first":84,"second":243,"amount":-1,},
    {"first":84,"second":244,"amount":-1,},
    {"first":84,"second":245,"amount":-1,},
    {"first":84,"second":246,"amount":-1,},
    {"first":84,"second":249,"amount":-1,},
    {"first":84,"second":250,"amount":-1,},
    {"first":84,"second":251,"amount":-1,},
    {"first":84,"second":252,"amount":-1,},
    {"first":84,"second":255,"amount":-1,},
    {"first":84,"second":256,"amount":-1,},
    {"first":84,"second":257,"amount":-1,},
    {"first":84,"second":305,"amount":-1,},
    {"first":84,"second":333,"amount":-1,},
    {"first":84,"second":339,"amount":-1,},
    {"first":84,"second":913,"amount":-1,},
    {"first":84,"second":953,"amount":-1,},
    {"first":84,"second":957,"amount":-1,},
    {"first":84,"second":959,"amount":-1,},
    {"first":84,"second":960,"amount":-1,},
    {"first":84,"second":969,"amount":-1,},
    {"first":84,"second":1032,"amount":-2,},
    {"first":84,"second":1040,"amount":-1,},
    {"first":84,"second":1072,"amount":-1,},
    {"first":84,"second":1077,"amount":-1,},
    {"first":84,"second":1084,"amount":-1,},
    {"first":84,"second":1086,"amount":-1,},
    {"first":84,"second":1088,"amount":-1,},
    {"first":84,"second":1089,"amount":-1,},
    {"first":84,"second":1091,"amount":-1,},
    {"first":84,"second":1093,"amount":-1,},
    {"first":84,"second":1104,"amount":-1,},
    {"first":84,"second":1105,"amount":-1,},
    {"first":84,"second":1109,"amount":-1,},
    {"first":84,"second":8211,"amount":-2,},
    {"first":84,"second":8212,"amount":-2,},
    {"first":84,"second":8218,"amount":-2,},
    {"first":84,"second":8222,"amount":-2,},
    {"first":84,"second":8230,"amount":-2,},
    {"first":84,"second":8249,"amount":-2,},
    {"first":84,"second":8250,"amount":-2,},
    {"first":85,"second":47,"amount":-1,},
    {"first":86,"second":44,"amount":-1,},
    {"first":86,"second":45,"amount":-1,},
    {"first":86,"second":46,"amount":-1,},
    {"first":86,"second":47,"amount":-2,},
    {"first":86,"second":65,"amount":-1,},
    {"first":86,"second":74,"amount":-1,},
    {"first":86,"second":97,"amount":-1,},
    {"first":86,"second":99,"amount":-1,},
    {"first":86,"second":100,"amount":-1,},
    {"first":86,"second":101,"amount":-1,},
    {"first":86,"second":103,"amount":-1,},
    {"first":86,"second":111,"amount":-1,},
    {"first":86,"second":113,"amount":-1,},
    {"first":86,"second":122,"amount":-1,},
    {"first":86,"second":171,"amount":-1,},
    {"first":86,"second":173,"amount":-1,},
    {"first":86,"second":192,"amount":-1,},
    {"first":86,"second":193,"amount":-1,},
    {"first":86,"second":194,"amount":-1,},
    {"first":86,"second":195,"amount":-1,},
    {"first":86,"second":196,"amount":-1,},
    {"first":86,"second":197,"amount":-1,},
    {"first":86,"second":198,"amount":-1,},
    {"first":86,"second":224,"amount":-1,},
    {"first":86,"second":225,"amount":-1,},
    {"first":86,"second":226,"amount":-1,},
    {"first":86,"second":227,"amount":-1,},
    {"first":86,"second":228,"amount":-1,},
    {"first":86,"second":229,"amount":-1,},
    {"first":86,"second":230,"amount":-1,},
    {"first":86,"second":231,"amount":-1,},
    {"first":86,"second":232,"amount":-1,},
    {"first":86,"second":233,"amount":-1,},
    {"first":86,"second":234,"amount":-1,},
    {"first":86,"second":235,"amount":-1,},
    {"first":86,"second":242,"amount":-1,},
    {"first":86,"second":243,"amount":-1,},
    {"first":86,"second":244,"amount":-1,},
    {"first":86,"second":245,"amount":-1,},
    {"first":86,"second":246,"amount":-1,},
    {"first":86,"second":256,"amount":-1,},
    {"first":86,"second":257,"amount":-1,},
    {"first":86,"second":333,"amount":-1,},
    {"first":86,"second":339,"amount":-1,},
    {"first":86,"second":913,"amount":-1,},
    {"first":86,"second":959,"amount":-1,},
    {"first":86,"second":1032,"amount":-1,},
    {"first":86,"second":1040,"amount":-1,},
    {"first":86,"second":1072,"amount":-1,},
    {"first":86,"second":1077,"amount":-1,},
    {"first":86,"second":1086,"amount":-1,},
    {"first":86,"second":1089,"amount":-1,},
    {"first":86,"second":1104,"amount":-1,},
    {"first":86,"second":1105,"amount":-1,},
    {"first":86,"second":8211,"amount":-1,},
    {"first":86,"second":8212,"amount":-1,},
    {"first":86,"second":8218,"amount":-1,},
    {"first":86,"second":8222,"amount":-1,},
    {"first":86,"second":8230,"amount":-1,},
    {"first":86,"second":8249,"amount":-1,},
    {"first":87,"second":47,"amount":-1,},
    {"first":88,"second":45,"amount":-1,},
    {"first":88,"second":102,"amount":-1,},
    {"first":88,"second":116,"amount":-1,},
    {"first":88,"second":118,"amount":-1,},
    {"first":88,"second":171,"amount":-1,},
    {"first":88,"second":173,"amount":-1,},
    {"first":88,"second":957,"amount":-1,},
    {"first":88,"second":960,"amount":-1,},
    {"first":88,"second":8211,"amount":-1,},
    {"first":88,"second":8212,"amount":-1,},
    {"first":88,"second":8249,"amount":-1,},
    {"first":89,"second":44,"amount":-2,},
    {"first":89,"second":46,"amount":-2,},
    {"first":89,"second":47,"amount":-2,},
    {"first":89,"second":65,"amount":-1,},
    {"first":89,"second":74,"amount":-2,},
    {"first":89,"second":97,"amount":-1,},
    {"first":89,"second":192,"amount":-1,},
    {"first":89,"second":193,"amount":-1,},
    {"first":89,"second":194,"amount":-1,},
    {"first":89,"second":195,"amount":-1,},
    {"first":89,"second":196,"amount":-1,},
    {"first":89,"second":197,"amount":-1,},
    {"first":89,"second":198,"amount":-1,},
    {"first":89,"second":224,"amount":-1,},
    {"first":89,"second":225,"amount":-1,},
    {"first":89,"second":226,"amount":-1,},
    {"first":89,"second":227,"amount":-1,},
    {"first":89,"second":228,"amount":-1,},
    {"first":89,"second":229,"amount":-1,},
    {"first":89,"second":230,"amount":-1,},
    {"first":89,"second":256,"amount":-1,},
    {"first":89,"second":257,"amount":-1,},
    {"first":89,"second":913,"amount":-1,},
    {"first":89,"second":1032,"amount":-2,},
    {"first":89,"second":1040,"amount":-1,},
    {"first":89,"second":1072,"amount":-1,},
    {"first":89,"second":8218,"amount":-2,},
    {"first":89,"second":8222,"amount":-2,},
    {"first":89,"second":8230,"amount":-2,},
    {"first":90,"second":45,"amount":-2,},
    {"first":90,"second":171,"amount":-1,},
    {"first":90,"second":173,"amount":-2,},
    {"first":90,"second":8211,"amount":-2,},
    {"first":90,"second":8212,"amount":-2,},
    {"first":90,"second":8249,"amount":-1,},
    {"first":91,"second":106,"amount":1,},
    {"first":91,"second":1112,"amount":1,},
    {"first":97,"second":63,"amount":-1,},
    {"first":97,"second":106,"amount":1,},
    {"first":97,"second":1112,"amount":1,},
    {"first":98,"second":63,"amount":-1,},
    {"first":99,"second":45,"amount":-1,},
    {"first":99,"second":63,"amount":-1,},
    {"first":99,"second":171,"amount":-1,},
    {"first":99,"second":173,"amount":-1,},
    {"first":99,"second":8211,"amount":-1,},
    {"first":99,"second":8212,"amount":-1,},
    {"first":99,"second":8249,"amount":-1,},
    {"first":101,"second":44,"amount":-1,},
    {"first":101,"second":46,"amount":-1,},
    {"first":101,"second":63,"amount":-1,},
    {"first":101,"second":120,"amount":-1,},
    {"first":101,"second":1093,"amount":-1,},
    {"first":101,"second":8218,"amount":-1,},
    {"first":101,"second":8222,"amount":-1,},
    {"first":101,"second":8230,"amount":-1,},
    {"first":102,"second":44,"amount":-1,},
    {"first":102,"second":45,"amount":-1,},
    {"first":102,"second":46,"amount":-1,},
    {"first":102,"second":47,"amount":-1,},
    {"first":102,"second":173,"amount":-1,},
    {"first":102,"second":8211,"amount":-1,},
    {"first":102,"second":8212,"amount":-1,},
    {"first":102,"second":8218,"amount":-1,},
    {"first":102,"second":8222,"amount":-1,},
    {"first":102,"second":8230,"amount":-1,},
    {"first":103,"second":63,"amount":-1,},
    {"first":103,"second":106,"amount":1,},
    {"first":103,"second":1112,"amount":1,},
    {"first":104,"second":63,"amount":-1,},
    {"first":104,"second":106,"amount":1,},
    {"first":104,"second":1112,"amount":1,},
    {"first":106,"second":106,"amount":1,},
    {"first":106,"second":1112,"amount":1,},
    {"first":107,"second":45,"amount":-1,},
    {"first":107,"second":63,"amount":-1,},
    {"first":107,"second":171,"amount":-1,},
    {"first":107,"second":173,"amount":-1,},
    {"first":107,"second":8211,"amount":-1,},
    {"first":107,"second":8212,"amount":-1,},
    {"first":107,"second":8249,"amount":-1,},
    {"first":109,"second":63,"amount":-1,},
    {"first":109,"second":106,"amount":1,},
    {"first":109,"second":1112,"amount":1,},
    {"first":110,"second":63,"amount":-1,},
    {"first":110,"second":106,"amount":1,},
    {"first":110,"second":1112,"amount":1,},
    {"first":111,"second":63,"amount":-1,},
    {"first":112,"second":63,"amount":-1,},
    {"first":113,"second":63,"amount":-1,},
    {"first":113,"second":106,"amount":1,},
    {"first":113,"second":1112,"amount":1,},
    {"first":114,"second":44,"amount":-1,},
    {"first":114,"second":45,"amount":-1,},
    {"first":114,"second":46,"amount":-1,},
    {"first":114,"second":47,"amount":-2,},
    {"first":114,"second":63,"amount":-1,},
    {"first":114,"second":171,"amount":-1,},
    {"first":114,"second":173,"amount":-1,},
    {"first":114,"second":8211,"amount":-1,},
    {"first":114,"second":8212,"amount":-1,},
    {"first":114,"second":8218,"amount":-1,},
    {"first":114,"second":8222,"amount":-1,},
    {"first":114,"second":8230,"amount":-1,},
    {"first":114,"second":8249,"amount":-1,},
    {"first":115,"second":45,"amount":-1,},
    {"first":115,"second":47,"amount":-1,},
    {"first":115,"second":63,"amount":-1,},
    {"first":115,"second":115,"amount":-1,},
    {"first":115,"second":116,"amount":-1,},
    {"first":115,"second":173,"amount":-1,},
    {"first":115,"second":960,"amount":-1,},
    {"first":115,"second":1109,"amount":-1,},
    {"first":115,"second":8211,"amount":-1,},
    {"first":115,"second":8212,"amount":-1,},
    {"first":116,"second":44,"amount":-1,},
    {"first":116,"second":45,"amount":-1,},
    {"first":116,"second":46,"amount":-1,},
    {"first":116,"second":47,"amount":-1,},
    {"first":116,"second":63,"amount":-1,},
    {"first":116,"second":171,"amount":-1,},
    {"first":116,"second":173,"amount":-1,},
    {"first":116,"second":8211,"amount":-1,},
    {"first":116,"second":8212,"amount":-1,},
    {"first":116,"second":8218,"amount":-1,},
    {"first":116,"second":8222,"amount":-1,},
    {"first":116,"second":8230,"amount":-1,},
    {"first":116,"second":8249,"amount":-1,},
    {"first":118,"second":44,"amount":-1,},
    {"first":118,"second":46,"amount":-1,},
    {"first":118,"second":47,"amount":-1,},
    {"first":118,"second":63,"amount":-1,},
    {"first":118,"second":8218,"amount":-1,},
    {"first":118,"second":8222,"amount":-1,},
    {"first":118,"second":8230,"amount":-1,},
    {"first":120,"second":45,"amount":-1,},
    {"first":120,"second":63,"amount":-1,},
    {"first":120,"second":171,"amount":-1,},
    {"first":120,"second":173,"amount":-1,},
    {"first":120,"second":8211,"amount":-1,},
    {"first":120,"second":8212,"amount":-1,},
    {"first":120,"second":8249,"amount":-1,},
    {"first":121,"second":106,"amount":1,},
    {"first":121,"second":1112,"amount":1,},
    {"first":122,"second":45,"amount":-1,},
    {"first":122,"second":63,"amount":-1,},
    {"first":122,"second":171,"amount":-1,},
    {"first":122,"second":173,"amount":-1,},
    {"first":122,"second":8211,"amount":-1,},
    {"first":122,"second":8212,"amount":-1,},
    {"first":122,"second":8249,"amount":-1,},
    {"first":123,"second":106,"amount":1,},
    {"first":123,"second":1112,"amount":1,},
  ],
  "ranges": [
    {"lower":32,"upper":127,},
    {"lower":9647,"upper":9647,},
  ],
  "regenerateBitmap": false,
  "canGenerateBitmap": true,
  "maintainGms1Font": false,
  "parent": {
    "name": "Fonts",
    "path": "folders/Fonts.yy",
  },
  "resourceVersion": "1.0",
  "name": "fontSmallBold",
  "tags": [],
  "resourceType": "GMFont",
}